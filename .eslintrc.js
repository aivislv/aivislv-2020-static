module.exports = {
  root: true,
  plugins: [
    "import",
    "react",
    "flowtype",
  ],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:import/recommended",
    "plugin:flowtype/recommended",
  ],
  settings: {
    react: {
      version: "detect",
    },
    "import/resolver": {
      webpack: {
        // Because webpack config is async, need to copy/paste the relevant parts.
        config: {
          resolve: {
            extensions: [ ".js", ".jsx" ],
            alias: {
            }
          }
        }
      }
    },
    "import/external-module-folders": [
      "node_modules",
    ]
  },
  env: {
    "browser": true,
    "node": true,
    "es2017": true,
  },
  globals: {
    "dataLayer": "readonly"
  },
  rules: {
    // "class-name-casing": ["error", { allowUnderscorePrefix: true }],  // TODO: use "naming-conventions".
    "no-control-regex": "off",
    "react/display-name": "off",
    "react/jsx-indent": ["error", 2, { checkAttributes: true, indentLogicalExpressions: true }],
    "react/jsx-indent-props": ["error", 2],
    "react/no-unescaped-entities": ["error", { "forbid": [">", "}"] }],
    "react/prop-types": ["error", { skipUndeclared: true }],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error",

    // Organize import section.
    "sort-imports": ["error", { "ignoreDeclarationSort": true, "ignoreMemberSort": false }],  // only order within line.
    "no-multiple-empty-lines": ["error", { "max": 2, "maxEOF": 1, "maxBOF": 0 }],
    "import/no-webpack-loader-syntax": "error",
    "import/no-named-as-default": "off",  // zustand examples show `import create from 'zustand'`.
    "import/no-self-import": "error",
    // "import/no-cycle": "error",  // TODO: solve redux cycles.
    "import/no-useless-path-segments": "error",
    // "import/no-extraneous-dependencies": "error",  // TODO: fix "crypto" and "util".
    // "import/no-relative-parent-imports": "warn",
    // "import/no-unused-modules": ["warn", {"unusedExports": true, "missingExports": true}],
    "import/first": "error",
    "import/newline-after-import": ["error", { "count": 2 }], "import/order": ["error", {
      "newlines-between": "always",
      "groups": [ "builtin", "external", "internal", "parent", "unknown", "sibling"],
      "pathGroups": [
        { "pattern": "react", "group": "external", "position": "before" },
      ],
      "pathGroupsExcludedImportTypes": ["builtin"],
      "alphabetize": {order: "asc", caseInsensitive: true},
    }],
    "no-restricted-imports": ["error", {
      "paths": [{
        "name": "react-redux",
        "importNames": ["useSelector", "useDispatch"],
        "message": "Please import from \"@coinmetro/app/redux/*\" instead."
      }]
    }],
  },
  "overrides": [
    {
      "files": ["*.ts", "*.tsx"],
      "rules": {
        "keyword-spacing": "error",
        "quotes": ["error", "single"],
        "semi": "error",
        "arrow-spacing": "error",
        "object-curly-spacing": ["error", "always"],
        "comma-dangle": ["error", "always-multiline"],
        "react/prop-types": "off",
        "react/jsx-curly-brace-presence": ["error", { props: "never", children: "never" }],
        "react/jsx-tag-spacing": ["error", {
          closingSlash: "never",
          beforeSelfClosing: "always",
          afterOpening: "never",
          beforeClosing: "never"
        }],
        "react/jsx-wrap-multilines": ["error", {
          declaration: "parens-new-line",
          assignment: "parens-new-line",
          return: "parens-new-line",
          arrow: "parens-new-line",
          condition: "parens-new-line",
          logical: "parens-new-line",
          prop: "parens-new-line"
        }],
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "function-declaration",
            unnamedComponents: "arrow-function"
          },
        ],
        "react/jsx-first-prop-new-line": ["error", "multiline-multiprop"],
        "react/jsx-max-props-per-line": ["error", { "when": "multiline" }],
        "react/jsx-closing-bracket-location": ["error", "line-aligned"],  // default 'tag-aligned' conflicts with "@typescript-eslint/indent".
        "react/forbid-dom-props": ["warn", { "forbid": ["style"] }],
        "react/forbid-component-props": ["warn", { "forbid": ["style"] }],
      },
    },
    {
      "files": ["*.stories.tsx"],
      "rules": {
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "function-declaration",
            unnamedComponents: "arrow-function"
          },
        ],
      },
    },
  ],
};
