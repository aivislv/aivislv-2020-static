const dev = process.env.NODE_ENV !== "production";
const path = require( "path" );
const FriendlyErrorsWebpackPlugin = require( "friendly-errors-webpack-plugin" );
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PostcssModulesValues = require('postcss-modules-values');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const Autoprefixer = require('autoprefixer');
const SentryPlugin = require("@sentry/webpack-plugin");
const {uid} = require('uid');

const plugins = [
  new FriendlyErrorsWebpackPlugin(),
  PostcssModulesValues,
  new MiniCssExtractPlugin({
    filename: "[name].css"
  }),
  Autoprefixer,
  new OptimizeCssAssetsPlugin({
    cssProcessor: require('cssnano'),
    cssProcessorPluginOptions: {
      preset: 'default',
    },
    canPrint: true
  }),
  new SentryPlugin({
    release: `${process.env.NODE_ENV}::${new Date().toString()}::${uid()}`,
    include: "../../data/static/",
    dryRun: dev,
    org: "aivislv",
    project: "aivislv-web",
    authToken: "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",
  }),
];

module.exports = {
  mode: dev ? "development" : "production",
  context: path.join( __dirname, "src" ),
  devtool: dev ? "source-map" : "none",
  entry: {
    app: "./index.js",
  },
  resolve: {
    modules: [
      path.resolve( "./src" ),
      "node_modules",
    ],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: dev ? '[name]__[local]___[hash:base64:5]' : '[hash:base64]'
              },
              importLoaders: 1,
              sourceMap: dev,
            }
          },
          'postcss-loader',
        ],
    }
    ],
  },
  output: {
    path: path.resolve( __dirname, "../../data/static/" ),
    filename: "[name].bundle.js",
  },
  optimization: {
    minimize: !dev,
    minimizer: [
      !dev &&
      new TerserPlugin({
        terserOptions: {
          parse: {
            // we want terser to parse ecma 8 code. However, we don't want it
            // to apply any minfication steps that turns valid ecma 5 code
            // into invalid ecma 5 code. This is why the 'compress' and 'output'
            // sections only apply transformations that are ecma 5 safe
            // https://github.com/facebook/create-react-app/pull/4234
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            // Disabled because of an issue with Uglify breaking seemingly valid code:
            // https://github.com/facebook/create-react-app/issues/2376
            // Pending further investigation:
            // https://github.com/mishoo/UglifyJS2/issues/2011
            comparisons: false,
            // Disabled because of an issue with Terser breaking valid code:
            // https://github.com/facebook/create-react-app/issues/5250
            // Pending futher investigation:
            // https://github.com/terser-js/terser/issues/120
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          output: {
            ecma: 5,
            comments: false,
            // Turned on because emoji and regex is not minified properly using default
            // https://github.com/facebook/create-react-app/issues/2488
            ascii_only: true,
          },
        },
        // Use multi-process parallel running to improve the build speed
        // Default number of concurrent runs: os.cpus().length - 1
        parallel: true,
        // Enable file caching
        cache: true,
        sourceMap: false,
      }),
    ].filter(Boolean),
    splitChunks: {
      minSize: 50000,
      cacheGroups: {
        vendors: false,
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'initial',
        },
      },
    },
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  plugins,
};
