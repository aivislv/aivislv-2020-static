import React from 'react';

import { InMemoryCache } from 'apollo-cache-inmemory';
import {ApolloClient} from 'apollo-client';
import {createHttpLink} from 'apollo-link-http'
import {ApolloProvider} from 'react-apollo';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import '@babel/polyfill';
import {config} from './config';
import Router from './init/Router';
import {history, store} from './init/store';
import * as Sentry from "@sentry/react";
import {BrowserTracing} from "@sentry/tracing";


Sentry.init({
    dsn: "https://7e96e1c4216a4dcfbcb4f355bfd682ae@o1308819.ingest.sentry.io/6554477",
    integrations: [new BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
});

const httpLink = createHttpLink({
  uri: config.api
});

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});

const container = document.getElementById('content');

if (container) {
  render(
    <Provider store={store}>
      <ApolloProvider store={store} client={client}>
        <Router history={history} />
      </ApolloProvider>
    </Provider>,
    container,
  );
}
