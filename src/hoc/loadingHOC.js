// @flow

import React from 'react';

import Loading from '../elements/loading/Loading';


export function loadingDecorator(graphql, renderSetings = {ifLoading: false, ifError: false}) {
  return (WrappedComponent) => {
    return class LoadingDecorator extends React.Component {
      render() {
        const {
          children,
          customClass,
          className,
        } = this.props;

        const isLoading = graphql.reduce((result, item) => this.props[item].loading || result, false);
        const useCustomClass = className || customClass;

        if (isLoading) {
          return <Loading className={useCustomClass} isLoading>{renderSetings.ifLoading && children}</Loading>;
        }

        const isError = graphql.reduce((result, item) => (this.props[item].error && this.props[item].error.message) || result, false);

        if (isError) {
          return <Loading className={customClass} isError={isError}>{renderSetings.ifError && children}</Loading>
        }

        return <WrappedComponent {...this.props} />;
      }
    };
  };
}
