// @flow


import React from 'react';

import Footer from '../components/footer/Footer';
import Init from '../components/Init/Init';
import Navigation from '../components/navigation/Navigation';
import { RouteAdv } from '../elements';
import { currentSize, isSize } from '../helpers/CheckSize';
import { connectActions, connectState } from '../init/actions';


const MainLayout = ({ component: Component, part, onResize, onScroll, sizeMedium, sizeLarge, sizeXLarge, section = null, ...rest }) => {
  const checkSize = () => {
    onResize({
      isSizeMedium: isSize(sizeMedium),
      isSizeLarge: isSize(sizeLarge),
      isSizeXLarge: isSize(sizeXLarge),
      sizeCurrent: currentSize(),
    });
  };

  const checkPosition = () => {
    onScroll({
      scrollPosition: window.scrollY,
    });
  };

  checkSize();
  checkPosition();
  window.addEventListener('resize', checkSize);
  window.addEventListener('scroll', checkPosition);

  return (
    <RouteAdv {...rest} render={matchProps => (
      <Init>
        <Navigation part={part}/>
        <Component section={section} {...matchProps} />
        <Footer/>
      </Init>)}
    />
  )
};

const Main = connectActions({
  onResize: ['layouts', 'onResize'],
  onScroll: ['layouts', 'onScroll'],
})(
connectState({
  sizeMedium: ['layouts', 'sizeMedium'],
  sizeLarge: ['layouts', 'sizeLarge'],
  sizeXLarge: ['layouts', 'sizeXLarge'],
})(
  MainLayout
));

export default Main;
