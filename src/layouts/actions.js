import * as C from './actions.names';


export const onResize = (result) => ({
  type: C.SET_WINDOW_ON_RESIZE,
  payload: {
    ...result,
  }
});

export const onScroll = (result) => ({
  type: C.SET_WINDOW_ON_SCROLL,
  payload: {
    ...result,
  }
});
