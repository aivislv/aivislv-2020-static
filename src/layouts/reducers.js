import * as Actions from './actions.names';


const reducer = (state = {
  sizeMedium: 421,
  sizeLarge: 841,
  sizeXLarge: 1201,
  isSizeMedium: false,
  isSizeLarge: false,
  isSizeXLarge: false,
  sizeCurrent: 0,
  scrollPosition: 0,
}, action) => {
  switch (action.type) {
    case Actions.SET_WINDOW_ON_RESIZE: {
      return {
        ...state,
        isSizeLarge: action.payload.isSizeLarge,
        isSizeMedium: action.payload.isSizeMedium,
        isSizeXLarge: action.payload.isSizeXLarge,
        sizeCurrent: action.payload.sizeCurrent,
      };
    }
    case Actions.SET_WINDOW_ON_SCROLL: {
      return {
        ...state,
        scrollPosition: action.payload.scrollPosition,
      };
    }
    default:
      return state;
  }

};

export default reducer;
