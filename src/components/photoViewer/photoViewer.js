// @flow

import React from 'react';

import path from 'path';

import { Link } from '../../elements';
import { SIZE_SPECIAL_ALBUM_WRAP } from '../../elements/constants/size';
import Icon, { Cross, Next, Previous } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { getImageLink, getImageSlug } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { PHOTOS, PHOTOS_SESSIONS } from '../../init/paths';
import { PHOTO_MODE_ALBUM, PHOTO_MODE_SESSION } from '../../views/photos/constants';

import ReactFBLike from 'react-fb-like';

import style from './photoViewer.css';


type Props = {
  photo: number,
  album: number,
  session: number,
  section: string,
  albumData: Object,
  sessionData: Object,
  image: Object,
  push: Function,
  openImageId: number,
  sizeCurrent: number,
  setOpenImageId: Function,
}

type State = {
}

@connectState({
  openMode: ['photos', 'openMode'],
  openImageId: ['photos', 'openImageId'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectActions({
  push: ['router', 'push'],
  setOpenImageId: ['photos', 'setOpenImageId'],
})
@connectQuery({
  query: 'viewPhotos.image',
  name: 'image',
  vars: {
    imageId: (props) => Number(props.photo)
  }
})
@connectQuery({
  query: 'viewPhotos.album',
  name: 'albumData',
  vars: {
    albumId: (props) => props.album
  },
  loading: (props) => props.section !== PHOTO_MODE_ALBUM
})
@connectQuery({
  query: 'viewPhotos.session',
  name: 'sessionData',
  vars: {
    sessionId: (props) => Number(props.session)
  },
  loading: (props) => props.section !== PHOTO_MODE_SESSION
})
class PhotoViewer extends React.PureComponent<Props, State> {
  static defaultProps = {};

  goTo = (data, listId) => {
    const {
      push,
      album,
      session,
      section,
      setOpenImageId,
    } = this.props;

    const slug = getImageSlug(data);
    setOpenImageId(listId);

    if (section === PHOTO_MODE_ALBUM) {
      if (slug) {
        push(path.join(PHOTOS, String(album), String(data.id), slug));
      } else {
        push(`${path.join(PHOTOS, String(album), String(data.id))}/`);
      }
    }
    else {
      if (slug) {
        push(path.join(PHOTOS_SESSIONS, String(session), String(data.id), slug));
      } else {
        push(`${path.join(PHOTOS_SESSIONS, String(session), String(data.id))}/`);
      }
    }
  }

  getOpenImageId = () => {
    const {
      albumData,
      sessionData,
      section,
      photo,
    } = this.props;

    if (section === PHOTO_MODE_ALBUM && !albumData.loading) {
      return albumData.data.album.images.reduce((validKey, photoData, key) => (photoData.id === Number(photo) ? key : validKey), null);
    }

    if (section === PHOTO_MODE_SESSION && !sessionData.loading) {
      return sessionData.data.session.images.reduce((validKey, photoData, key) => (photoData.id === Number(photo) ? key : validKey), null);
    }

    return null;
  }

  goNext = () => {
    const closestImage = this.getCloseImage('next');
    if (closestImage) {
      this.goTo(closestImage.image, closestImage.listId)
    }
  };

  goPrevious = () => {
    const closestImage = this.getCloseImage('previous');
    if (closestImage) {
      this.goTo(closestImage.image, closestImage.listId)
    }
  };

  getCloseImage = (position = 'next') => {
    const {
      albumData,
      sessionData,
      section,
      openImageId,
    } = this.props;

    if ((section === PHOTO_MODE_ALBUM && !albumData.loading) || (section === PHOTO_MODE_SESSION && !sessionData.loading)) {
      const useOpenImageId = openImageId !== null ? openImageId : this.getOpenImageId();

      let useData;
      if (section === PHOTO_MODE_SESSION) {
        useData = sessionData.data.session.images;
      } else {
        useData = albumData.data.album.images;
      }

      if (useOpenImageId !== null) {
        let listId;
        let image;

        if (position === 'next') {
          listId = useData[useOpenImageId + 1] ? useOpenImageId + 1 : 0;
          image = useData[listId]
        } else {
          listId = useData[useOpenImageId - 1] ? useOpenImageId - 1 : (useData.length - 1);
          image = useData[listId];
        }

        return { image, listId };
      }
    }

    return null;
  };

  close = () => {
    const {
      sizeCurrent,
      push,
      album,
      session,
      section,
    } = this.props;

    if (sizeCurrent > SIZE_SPECIAL_ALBUM_WRAP) {
      if (section === PHOTO_MODE_ALBUM) {
        push(path.join(PHOTOS, String(album)));
      } else {
        push(path.join(PHOTOS_SESSIONS, String(session)));
      }
    } else {
      if (section === PHOTO_MODE_ALBUM) {
        push(path.join(PHOTOS));
      } else {
        push(path.join(PHOTOS_SESSIONS));
      }
    }
  };

  render = () => {
    const {
      image,
      sizeCurrent,
    } = this.props;

    let imageSource = '';

    if (!image.loading) {
      imageSource = getImageLink(image.data.image, sizeCurrent);
      setTitle(image.data.image.title);
    }

    let photoAlt;

    if (!image.loading) {
      photoAlt = ((image.data.image.title && image.data.image.persons && `${image.data.image.title}; ${image.data.image.persons}`) || (image.data.image.title && image.data.image.place && `${image.data.image.title}; ${image.data.image.place}`) || image.data.image.title || image.data.image.persons || image.data.image.place).substr(0, 100);
    }

    return (<article className={style.body}>
      <div className={style.close} onClick={this.close}><Icon theme="white"><Cross /></Icon></div>
      <div className={style.next} onClick={this.goNext}><Icon className={style.nextIcon} theme="white"><Next/></Icon></div>
      <div className={style.previous} onClick={this.goPrevious}><Icon className={style.previousIcon} theme="white"><Previous /></Icon></div>
      <img className={style.image} src={imageSource} ref={this.imageCurrent} alt={photoAlt} title={photoAlt} />
      <>{!image.loading &&
        <div className={style.descriptionBlock}>
          {image.data.image.title && <h1 className={style.titleContainer}><Text asSpan className={style.title}>{image.data.image.title}</Text></h1>}
          {image.data.image.place && <Text className={style.subtitle}>Place: {image.data.image.place}</Text>}
          {image.data.image.persons && <Text className={style.subtitle}>Person: {image.data.image.persons}</Text>}
          {image.data.image.session && <Text className={style.subtitle}>Session: <Link to={path.join(PHOTOS_SESSIONS, String(image.data.image.session.id))}>{image.data.image.session.title}</Link></Text>}
          <time dateTime={image.data.image.created}></time>
          <div className={style.like}>
            <ReactFBLike language="en_US" appId="483133628493176" version="v12.0" colorscheme="dark" layout="button" reference={`photo-desktop-${image.data.image.id}`} />
          </div>
        </div>
        }</>
    </article>);
  }
}

export default PhotoViewer;
