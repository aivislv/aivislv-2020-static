// @flow

import React from 'react';


import dayjs from 'dayjs';
import path from 'path';

import { config } from '../../config';
import { Button } from '../../elements';
import Icon, { Bgg } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { calculateRed } from '../../helpers/HexDec';
import { getGameImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import {connectActions, connectQuery, connectState} from '../../init/actions';
import {GAMES_SHOW} from '../../init/paths';
import {GAME_EXPANSIONS_MAX} from '../../views/games/constants';

import style from './gameView.css';
import {createGamePath} from "../../helpers/DynamicPaths";
import {faCircle, faClock} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


type Props = {
  gameData: Object,
  openFilter: string,
  openSort: string,
  openType: string,
  openTags: Array,
  search: string,
  push: Function,
}

@connectActions({
  push: ['router', 'push'],
})
@connectState({
  openFilter: ['games', 'openFilter'],
  openSort: ['games', 'openSort'],
  openType: ['games', 'openType'],
  search: ['games', 'search'],
  openGame: ['games', 'openGame'],
  openTags: ['games', 'openTags'],
})
@connectQuery({
  query: 'viewGames.game',
  name: 'gameData',
  vars: {
    gameId: (props) => Number(props.openGame)
  },
  loading: (props) => !props.openGame,
})
class GameView extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  renderExpansions = (expansion) => {
    return <div key={expansion.id}  className={style.expansion}>
      <div className={expansion.type === 1 ? style.expansionBody : `${style.expansionBody} ${style.wishlist}`}>
        <img className={style.image} src={getGameImageLink(expansion.id)} />
        <a className={style.bggLink} target="_blank" href={`${config.gameBggLink}/${expansion.bggId}/`} rel="noreferrer"><Icon><Bgg /></Icon></a>
        {expansion.type === 2 && expansion.preordered &&
            <div title={message('components.gameView.texts.preordered')} className={style.preordered}><FontAwesomeIcon
                icon={faClock}/></div>}
        {expansion.type === 2 && expansion.wishlistPriority > 0 &&
            <div className={style.wishlistPriority}
                 title={message(`components.gameView.texts.wishlistPriority`)[expansion.wishlistPriority]}>
              <FontAwesomeIcon className={style[`wishlistPriorityIcon-${expansion.wishlistPriority}`]} icon={faCircle}/>
            </div>
        }
      </div>
      <Text type="h2" className={style.expansionTitle}>{expansion.title}</Text>
    </div>
  };

  sortExpansions = (first, second) => {
    const byType = first.type > second.type ? 1 : (first.type < second.type ? -1 : 0);

    if (byType === 0) {
      return first.bggId > second.bggId ? 1 : (first.bggId < second.bggId ? -1 : 0);
    }

    return byType;
  };

  createPath = (tag = false) => {
    const {
      openFilter,
      openSort,
      openType,
      openTags,
      search,
    } = this.props;

    return createGamePath(GAMES_SHOW, {
      type: openType,
      sort: openSort,
      filter: openFilter,
      search: search || '',
      tags: tag ? [tag] : openTags
    })
  }

  render = () => {
    const {
      gameData,
      push,
    } = this.props;

    let game;

    if (!gameData.loading && gameData?.data?.game) {
      game = gameData.data.game;
      setTitle(`Game - ${game.title}`);
    }

    if (!gameData.loading && !gameData.data) {
      push(GAMES_SHOW);
      return <></>;
    }

    const isFullscreen = !gameData.loading && game.expansions && game.expansions.length > GAME_EXPANSIONS_MAX;

    let playerCountString = "";

    if (!gameData.loading) {
      if (gameData.data.game.players !== "") {
        const playerCount = JSON.parse(gameData.data.game.players);

        const pcBest = [];
        const pcOk = [];

        for (let pcData of playerCount) {
          if (pcData['status'] === 1) {
            pcBest.push(pcData['players'])
          } else if (pcData['status'] === 2) {
            pcOk.push(pcData['players'])
          }
        }

        playerCountString = `Best: ${pcBest.join(";")} Ok: ${pcOk.join(";")}`
      }
    }

    return (<div className={isFullscreen ? style.coverAllContainer : style.container}>
      <Button asLink to={this.createPath()}
              className={style.backButton}>{message('common.back')}</Button>
      {!gameData.loading && gameData.data.game && <div className={style.body}>
        <div className={style.imageContainer}>
          <img className={style.image} src={getGameImageLink(game.id)}/>
          <a title={message('common.open').replace('#SOURCE#', message('specs.imdb'))} className={style.bggLink}
             target="_blank" href={`${config.gameBggLink}/${game.bggId}/`} rel="noreferrer"><Icon><Bgg/></Icon></a>
          {game.rating > 0 && <Text title={message('common.rating')} className={style.rating} type="h1"
                                    style={{color: calculateRed(game.rating)}}>{game.rating}</Text>}
          {game.type === 2 && game.preordered &&
              <div title={message('components.gameView.texts.preordered')} className={style.preordered}><FontAwesomeIcon
                  icon={faClock}/></div>}
          {game.type === 2 && game.wishlistPriority > 0 &&
              <div className={style.wishlistPriority}
                   title={message(`components.gameView.texts.wishlistPriority`)[game.wishlistPriority]}>
                <FontAwesomeIcon className={style[`wishlistPriorityIcon-${game.wishlistPriority}`]} icon={faCircle}/>
              </div>
          }
        </div>
        <div className={style.infoContainer}>
          <Text type="h1" className={style.title}>{game.title}</Text>
          {game.lastPlay && <Text type="h2"
                                  className={style.playCount}><span>{message('components.gameView.texts.playCount')}:</span><span>{game.plays}</span></Text>}
          {game.lastPlay && <Text type="h2"
                                  className={style.lastPlay}><span>{message('components.gameView.texts.lastPlay')}:</span><span>{dayjs(game.lastPlay.date).format('YYYY.MM.DD')}</span></Text>}
          <Text type="h2"
                className={style.playerCountTitle}><span>{message('components.gameView.texts.playerCount')}:</span></Text>
          <Text type="h2" className={style.playerCount}><span>{playerCountString}</span></Text>
          <Text type="h2"
                className={style.playLength}><span>{message('components.gameView.texts.playLength')}:</span><span>{game.minPlaytime !== game.maxPlaytime ? `${game.minPlaytime} - ${game.maxPlaytime}` : game.minPlaytime}</span></Text>
        </div>
        <div className={style.tagContainer}>
          <Text type="h1" className={style.tagTitle}>Tags:</Text>
          {game.tags.map((tag) => (
              <Button
                  className={style.tag}
                  style={{backgroundColor: `#${tag.category.color}`}}
                  key={tag.id}
                  to={this.createPath(tag.id)}
                  asLink
            >
              {tag.tag}
            </Button>
          ))}
        </div>
        <div className={style.expansionContainer}>
          {game.expansions && game.expansions.sort(this.sortExpansions).map((expansion) => this.renderExpansions(expansion))}
        </div>
        {!isFullscreen && <Button asLink to={this.createPath()}
                                  className={style.closeButton}>{message('common.close')}</Button>}
      </div>}
    </div>);
  }
}

// gameData?.lastPlay?.date && dayjs(gameData.lastPlay.date).format('YYYY.MM.DD')

export default GameView;
