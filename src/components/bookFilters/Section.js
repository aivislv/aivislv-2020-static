// @flow

import React from 'react';
import {Button} from "../../elements";
import style from "./bookFilters.css";
import Text from "../../elements/text/Text";

export const Section = ({title, list, type, link, current, reset}) => {
    return <div className={style.section}>
        <Text type="h2" className={style.title}>{title}</Text>
        <div className={style.sectionBody}>
            <div key="reset" className={style.button}><Button onClick={() => link(type, reset)}>Reset</Button></div>
            <select className={style.select} value={current}
                    onChange={(selected) => link(type, selected.currentTarget.value)}>
                {
                    list.map((listItem) => {
                        return (<option value={listItem.value} key={listItem.value}
                                        style={{backgroundColor: listItem.isSelected ? `#aaaaaa` : `#${listItem.color}`}}
                                        className={style.button}>{listItem.title}</option>)
                    })
                }
            </select>
        </div>
    </div>
}