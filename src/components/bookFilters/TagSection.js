// @flow

import React from 'react';
import {Button} from "../../elements";
import style from "./bookFilters.css";
import Text from "../../elements/text/Text";

export const TagSection = ({title, list, link}) => {
    return <div className={style.section}>
        <Text type="h2" className={style.title}>{title}</Text>
        <div className={style.tagSectionBody}>
            <div key="reset" className={style.button}><Button onClick={() => link(null)}>Reset</Button></div>
            {list.map((listItem) => {
                return (<div key={listItem.id} className={style.button}><Button
                    style={{backgroundColor: listItem.isSelected ? `#aaaaaa` : `#eeeeee`}}
                    onClick={() => link(listItem.id)}>{listItem.title}</Button></div>)
            })}
        </div>
    </div>
}