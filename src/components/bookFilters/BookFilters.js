// @flow

import React from 'react';
import {connectActions, connectQuery, connectState} from "../../init/actions";

import style from './bookFilters.css';
import {createBookPath} from "../../helpers/DynamicPaths";
import {BOOKS_LIST} from "../../init/paths";
import {Button} from "../../elements";
import {TagSection} from "./TagSection";
import * as Filters from "../../views/books/constants";
import {Section} from "./Section";

const sortOwned = [
    {value: Filters.BOOK_OWNERSHIP_ALL, title: "All"},
    {value: Filters.BOOK_OWNERSHIP_YES, title: "Owned"},
    {value: Filters.BOOK_OWNERSHIP_NO, title: "Not owned"},
]

const sortOrder = [
    {value: Filters.BOOK_SORT_ALPHABET, title: "Alphabet"},
    {value: Filters.BOOK_SORT_WISH_PRIORITY, title: "Wish priority"}
]

@connectActions({
    push: ['router', 'push'],
})
@connectState({
    openTags: ['books', 'openTags'],
    openType: ['books', 'openType'],
    openOwned: ['books', 'openOwned'],
    openSort: ['books', 'openSort'],
    search: ['books', 'search'],
})
@connectQuery({
    query: 'viewBooks.tags',
    name: 'tagsList',
})
class BookFilters extends React.PureComponent {
    static defaultProps = {};
    state = {};

    navigateTo = (type, value) => {
        const {
            openTags,
            openType,
            openOwned,
            openSort,
            search,
            push
        } = this.props;

        const options = {
            tags: openTags,
            type: openType,
            owned: openOwned,
            sort: openSort,
            search
        }

        options[type] = value

        push(createBookPath(BOOKS_LIST, options))
    }

    navigateFilterTo = (value) => {
        const {openTags} = this.props

        if (value === null) {
            this.navigateTo("tags", [])
            return;
        }

        const itemId = openTags.indexOf(value);

        let newArray = [];

        if (itemId > -1) {
            newArray = [...openTags.splice(0, itemId), ...openTags.splice(itemId + 1)];
        } else {
            newArray = [value, ...openTags]
        }

        this.navigateTo("tags", newArray)
    }

    prepTagList = () => {
        const {tagsList, openTags} = this.props

        if (!tagsList.loading && tagsList.data) {
            return tagsList.data.bookTags
                .filter(tag => !tag.promote)
                .sort((a, b) => a.title > b.title ? 1 : -1)
                .map((item) => {
                    return {
                        title: item.title,
                        id: item.id,
                        isSelected: openTags.includes(item.id)
                    }
                })
        }

        return []
    }

    render = () => {
        const {
            closeFilters,
            openOwned,
            openSort,
        } = this.props;

        return (<div className={style.overlay}>
            <div className={style.closeContainer}><Button className={style.close} onClick={closeFilters}>X</Button>
            </div>
            <div className={style.body}>
                <Section title="Owned" reset={Filters.BOOK_OWNERSHIP_ALL} list={sortOwned} current={openOwned}
                         type="owned" link={this.navigateTo}/>
                <Section title="Sort" reset={Filters.BOOK_SORT_ALPHABET} list={sortOrder} current={openSort}
                         type="sort" link={this.navigateTo}/>
                <TagSection title="Tags" list={this.prepTagList()} link={this.navigateFilterTo}/>
            </div>
        </div>)
    }
}

export default BookFilters;
