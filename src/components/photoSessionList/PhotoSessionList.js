// @flow

import React from 'react';

import { Button, Text } from '../../elements';
import { connectQuery } from '../../init/actions';
import { PHOTOS } from '../../init/paths';
import Session from '../session/Session';

import style from './photoSessionList.css';


type Props = {
  sessions: Object,
}

type State = {
}

@connectQuery({
  query: 'viewPhotos.sessions',
  name: 'sessions',
})
class PhotoSessionList extends React.Component<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      sessions,
    } = this.props;

    return (<div className={style.container}>
      <div className={style.center}><Button className={style.sessions} asLink to={PHOTOS}>Themes</Button></div>
      <div className={style.center}><Text type="h1" className={style.title}>Sessions</Text></div>
      <div className={style.body}>
        <div className={style.album}>
          {!sessions.loading && sessions?.data?.sessions?.map((session, id) => (<Session data={session} key={`session-list-${id}`} />))}
        </div>
      </div>
    </div>);
  }
}

export default PhotoSessionList;
