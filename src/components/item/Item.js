// @flow

import React from 'react';


import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons';
import {faBook, faBookReader, faCircle, faClock} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import dayjs from 'dayjs';
import path from 'path';

import Text from '../../elements/text/Text';
import { calculateRed } from '../../helpers/HexDec';
import { connectActions } from '../../init/actions';
import Image from '../image/Image';
import { OutIcon } from '../outIcon/OutIcon';

import style from './item.css';


type Props = {
  itemData: Object,
  processImage?: Function,
  processImageSrc?: string,
  linkOut: string,
  messages: Object,
  itemLink: string,
  service: string,
  linkOutIcon?: Object,
  linkOutFile?: string,
  isGame?: boolean,
  isBook?: bookean,
  outLink: ?string, // config.bookGoodreadsLink + /book/show/
  ratingMax?: number,
  push: Function,
}

@connectActions({
  push: ['router', 'push'],
})
class Item extends React.PureComponent<Props, State> {
  static defaultProps = {
    linkOutIcon: null,
    linkOutFile: null,
    isGame: false,
    isBook: false,
    ratingMax: 10,
    processImageSrc: null,
  };
  state = {
    loader: true,
    is404: false,
  };

  preventPropogation = (e) => {e.stopPropagation()};

  show404 = () => {
    this.setState({ is404: true });
  };

  render = () => {
    const {
      itemData,
      push,
      itemLink,
      processImage,
      outLink,
      messages,
      service,
      isGame,
      isBook,
      linkOutIcon,
      linkOutFile,
      ratingMax,
      processImageSrc,
      preordered,
      wishlist_priority,
      type,
    } = this.props;

    const {
      is404,
      loader,
    } = this.state;

    return (<div className={`${style.thumb} ${style[`${service}Mode`]}`}>
      <div onClick={() => push(path.join(itemLink, String(itemData.id)))} className={loader ? `${style.thumbContainer} ${style.loader}` : style.thumbContainer}>
        <Image onError={this.show404} onLoad={() => this.setState({ loader: false })} className={style.image}  src={processImageSrc || processImage(itemData.id)} />
        <Text title={itemData.title} type="normal" className={`${style.title} ${is404 ? style.noImage : ''}`}>{itemData.title}</Text>
        {outLink && <a onClick={this.preventPropogation} className={style.serviceIcon} title={messages.openPage} target="_blank" href={outLink} rel="noreferrer">{OutIcon(linkOutIcon, linkOutFile)}</a>}
        {itemData.rating > 0 && <Text title={messages.rating} className={style.rating} type="h1" style={{ color: calculateRed(itemData.rating, ratingMax) }}>{itemData.rating}</Text>}
        {isBook && <div className={style[`isRead-${itemData.isRead}`]} title={messages.readStatus[itemData.isRead]}><FontAwesomeIcon icon={faBookReader} /></div>}
        {isBook && itemData.isOwned === 1 && <div className={style.isOwned} title={messages.owned}><FontAwesomeIcon icon={faBook} /></div>}
        {isGame && itemData.plays > 0 && <Text title={messages.playCount}  className={style.playCount} type="h2">{itemData.plays}</Text>}
        {isGame && itemData.type === 2 && itemData.preordered &&
            <div className={style.preordered} title={messages.preordered}><FontAwesomeIcon icon={faClock}/></div>}
        {isGame && itemData.type === 2 && itemData.wishlistPriority > 0 &&
            <div className={style.wishlistPriority} title={messages.wishlistPriority[itemData.wishlistPriority]}>
              <FontAwesomeIcon className={style[`wishlistPriorityIcon-${itemData.wishlistPriority}`]} icon={faCircle}/>
            </div>
        }
        {isGame && itemData.lastPlay?.date && <div className={style.lastPlay} title={messages.lastPlay.replace('#DATE#', dayjs(itemData.lastPlay.date).format('YYYY.MM.DD'))}><FontAwesomeIcon icon={faCalendarAlt} /></div>}
      </div>
    </div>);
  }
}

export default Item;
