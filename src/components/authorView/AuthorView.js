// @flow

import React from 'react';

import path from 'path';

import { config } from '../../config';
import Button from '../../elements/button/Button';
import Icon, { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { getAuthorImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { AUTHORS_LIST } from '../../init/paths';
import { AUTHOR_BOOKS_WRAP_COUNT } from '../../views/authors/constants';
import BooksSmall from '../booksSmall/BooksSmall';
import Image from '../image/Image';

import style from './authorView.css';
import SeriesSmall from "../seriesSmall/SeriesSmall";


type Props = {
  authorData: Object,
  search: string,
  push: Function,
  isOpenAuthor: number,
}

@connectActions({
  push: ['router', 'push'],
})
@connectState({
  search: ['authors', 'search'],
  openAuthor: ['authors', 'openAuthor'],
})
@connectQuery({
  query: 'viewAuthors.author',
  name: 'authorData',
  vars: {
    authorId: (props) => Number(props.openAuthor)
  },
  loading: (props) => !props.openAuthor,
})
class AuthorView extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  sortBooks = (book1, book2) => (book1['title'] < book2['title']) ? -1 : (book1['title'] === book2['title'] ? 0 : 1);

  render = () => {
    const {
      authorData,
      search,
      push,
      isOpenAuthor,
    } = this.props;

    let author;

    if (!authorData.loading && authorData?.data?.author) {
      author = authorData.data.author;
      setTitle(`Author - ${author.name}`);
    }

    if (!authorData.loading && !authorData.data) {
      push(AUTHORS_LIST);
      return <></>;
    }

    const isFullscreen = !isOpenAuthor || (!authorData.loading && authorData?.data?.author?.books && authorData.data.author.books.length >= AUTHOR_BOOKS_WRAP_COUNT);

    return (!authorData.loading && <div className={isFullscreen ? style.coverAllContainer : style.container}>
      <Button asLink to={path.join(AUTHORS_LIST, search || '')}
              className={style.backButton}>{message('common.back')}</Button>
      {authorData.data.author && <div className={style.body}>
        <div className={style.imageContainer}>
          <Image className={style.image} src={getAuthorImageLink(author.id)}/>
          {author.goodreadsId !== 0 &&
              <a title={message('common.open').replace('#SOURCE#', message('specs.gr'))} className={style.bggLink}
                 target="_blank" href={`${config.authorGoodreadsLink}/${author.goodreadsId}/`}
                 rel="noreferrer"><Icon><Goodreads/></Icon></a>}
        </div>

        <div className={style.infoContainer}>
          <Text type="h1" className={style.title}>{author.name}</Text>
        </div>

        {author.books && author.books.length > 0 && <div className={style.booksContainer}>
          <Text type="h1" className={style.title}>{message('components.authorView.texts.books')}</Text>
          <div className={style.booksList}>
            {author.books.sort(this.sortBooks).map((book, key) => <BooksSmall isFullscreen={isFullscreen} key={key}
                                                                              book={book}/>)}
          </div>
        </div>}

        {author.series && author.series.length > 0 && <div className={style.booksContainer}>
          <Text type="h1" className={style.title}>{message('components.authorView.texts.series')}</Text>
          <div className={style.booksList}>
            {author.series.sort(this.sortBooks).map((series, key) => <SeriesSmall isFullscreen={isFullscreen} key={key}
                                                                                  series={series}/>)}
          </div>
        </div>}

        <Button asLink to={path.join(AUTHORS_LIST, search || '')}
                className={style.closeButton}>{message('common.close')}</Button>
      </div>}
    </div>);
  }
}

// authorData?.lastPlay?.date && dayjs(authorData.lastPlay.date).format('YYYY.MM.DD')

export default AuthorView;
