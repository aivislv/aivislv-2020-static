// @flow

import React from 'react';

import "react-responsive-carousel/lib/styles/carousel.min.css";

import Text from '../../elements/text/Text';
import { connectState } from '../../init/actions';

import style from './pannerImage.css';
import ReactFBLike from 'react-fb-like';


type Props = {
  src: string,
  id: number,
  image: Object,
}

@connectState({
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
class PannerImage extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    window.FB?.XFBML?.parse();
  }

  render = () => {
    const {
      src,
      image,
    } = this.props;

    let photoAlt = '';

    if (!image.loading) {
      photoAlt = ((image.title && image.persons && `${image.title}; ${image.persons}`) || (image.title && image.place && `${image.title}; ${image.place}`) || image.title || image.persons || image.place).substr(0, 100);
    }

    return (<article className={style.imageBody}>
      <img loading="lazy" className={style.image} src={src} alt={photoAlt} title={photoAlt} />
      <div className={style.descriptionBlock}>
        {image.title && <h1><Text asSpan className={style.title}>{image.title}</Text></h1>}
        {image.place && <Text className={style.subtitle}>Place: {image.place}</Text>}
        {image.persons && <Text className={style.subtitle}>Person: {image.persons}</Text>}
        <time dateTime={image.created}></time>
        <div className={style.like}>
          <ReactFBLike language="en_US" appId="483133628493176" version="v12.0" colorscheme="dark" layout="button" lazy reference={`photo-mobile-${image.id}`} />
        </div>
      </div>
    </article>);
  }
}

export default PannerImage;
