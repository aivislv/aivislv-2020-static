// @flow

import React from 'react';

import path from 'path';
import Fullscreen from "react-full-screen";
import { Carousel } from 'react-responsive-carousel';

import { SIZE_SPECIAL_ALBUM_WRAP } from '../../elements/constants/size';
import Icon, { Cross } from '../../elements/icon'
import { getImageLink, getImageSlug } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { PHOTOS, PHOTOS_SESSIONS } from '../../init/paths';
import { PHOTO_MODE_ALBUM, PHOTO_MODE_SESSION } from '../../views/photos/constants';

import PannerImage from './PannerImage';
import style from './photoPanner.css';


type Props = {
  photo: number,
  album: number,
  albumData: Object,
  sessionData: Object,
  gqImage: Object,
  push: Function,
  openMode: boolean,
  openImageId: number,
  sizeCurrent: number,
  setOpenImageId: Function,
  section: string,
  session: number,
}

type State = {
  snapElement: Object | null,
  swipePosition: number,
  originalSwipePosition: number,
}

@connectState({
  openMode: ['photos', 'openMode'],
  openImageId: ['photos', 'openImageId'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectActions({
  push: ['router', 'push'],
  setOpenImageId: ['photos', 'setOpenImageId'],
})
@connectQuery({
  query: 'viewPhotos.album',
  name: 'albumData',
  vars: {
    albumId: (props) => props.album
  },
  loading: (props) => props.section !== PHOTO_MODE_ALBUM
})
@connectQuery({
  query: 'viewPhotos.session',
  name: 'sessionData',
  vars: {
    sessionId: (props) => Number(props.session)
  },
  loading: (props) => props.section !== PHOTO_MODE_SESSION
})
class PhotoPanner extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
  };

  componentDidUpdate() {
    window.FB?.XFBML?.parse();
  }

  close = () => {
    const {
      sizeCurrent,
      push,
      album,
      section,
      session,
    } = this.props;

    if (section === PHOTO_MODE_ALBUM) {
      if (sizeCurrent > SIZE_SPECIAL_ALBUM_WRAP) {
        push(path.join(PHOTOS, String(album)));
      } else {
        push(path.join(PHOTOS));
      }
    } else {
      if (sizeCurrent > SIZE_SPECIAL_ALBUM_WRAP) {
        push(path.join(PHOTOS_SESSIONS, String(session)));
      } else {
        push(path.join(PHOTOS_SESSIONS));
      }
    }
  };

  renderImage = (image, key) => {
    const {
      sizeCurrent,
    } = this.props;

    const imageSource = getImageLink(image, sizeCurrent);

    return (<PannerImage image={image} src={imageSource} key={key} id={key} />);
  };

  getOpenImageId = () => {
    const {
      albumData,
      section,
      sessionData,
      photo,
    } = this.props;

    if (!albumData.loading && section === PHOTO_MODE_ALBUM) {
      return albumData.data.album.images.reduce((validKey, photoData, key) => (photoData.id === Number(photo) ? key : validKey), null);
    }

    if (!sessionData.loading && section === PHOTO_MODE_SESSION) {
      return sessionData.data.session.images.reduce((validKey, photoData, key) => (photoData.id === Number(photo) ? key : validKey), null);
    }

    return null;
  };

  goTo = (data, listId) => {
    const {
      push,
      album,
      session,
      section,
      setOpenImageId,
    } = this.props;

    const slug = getImageSlug(data);
    setOpenImageId(listId);
    if (section === PHOTO_MODE_ALBUM) {
      if (slug) {
        push(path.join(PHOTOS, String(album), String(data.id), slug));
      } else {
        push(`${path.join(PHOTOS, String(album), String(data.id))}/`);
      }
    } else {
      if (slug) {
        push(path.join(PHOTOS_SESSIONS, String(session), String(data.id), slug));
      } else {
        push(`${path.join(PHOTOS_SESSIONS, String(session), String(data.id))}/`);
      }
    }
  }

  onChange = (id) => {
    const {
      albumData,
      sessionData,
      section
    } = this.props;

    if (!albumData.loading && section === PHOTO_MODE_ALBUM) {
      setTitle(albumData.data.album.images[id]['title']);
      this.goTo(albumData.data.album.images[id], id);
    }

    if (!sessionData.loading && section === PHOTO_MODE_SESSION) {
      setTitle(sessionData.data.session.images[id]['title']);
      this.goTo(sessionData.data.session.images[id], id);
    }
  }

  render = () => {
    const {
      section,
      openImageId,
      sessionData,
      albumData,
    } = this.props;

    const {
      swipePosition,
    } = this.state;

    return (
      <div>
        {(this.getOpenImageId() !== null) ?
          (<Fullscreen enabled>
            <div className={style.body}>
              <div className={style.loupe}></div>
              <div className={style.gallery}></div>
              <div className={style.close} onClick={this.close}><Icon theme="white"><Cross /></Icon></div>
              <div className={style.imageContainer} ref={this.imageContainer} style={{ transform: `translateX(${swipePosition}px)` }}>
                <Carousel selectedItem={openImageId || this.getOpenImageId() || 0} onChange={this.onChange} useKeyboardArrows showIndicators={false} showStatus={false}  showThumbs={false}>
                  {section === PHOTO_MODE_ALBUM ? (!albumData.loading && albumData.data.album.images.map(this.renderImage)) : (!sessionData.loading && sessionData.data.session.images.map(this.renderImage))}
                </Carousel>
              </div>
            </div></Fullscreen>) : <></>
        }
      </div>
        );
  }
}

export default PhotoPanner;
