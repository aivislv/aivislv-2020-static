// @flow

import React from 'react';


import Button from '../../elements/button/Button';
import Text from '../../elements/text/Text';
import { calculateRed } from '../../helpers/HexDec';
import setTitle from '../../helpers/SetTitle';
import { connectActions } from '../../init/actions';
import { OutIcon } from '../outIcon/OutIcon';

import style from './view.css';


type Props = {
  listData: Object,
  openItem?: number,
  module: string,
  title: string,
  messages: Object,
  serviceIdTitle: string,
  linkOutFile?: string,
  linkOutIcon?: Object,
  outLink: string,
  backLink: string,
  getImageLink: Function,
}

@connectActions({
  push: ['router', 'push'],
})
class View extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      openItem,
      backLink,
      listData,
      module,
      title,
      messages,
      linkOutFile,
      linkOutIcon,
      serviceIdTitle,
      outLink,
      getImageLink,
    } = this.props;

    const openItemInt = Number(openItem);

    const item = !listData.loading && listData?.data?.[module].find((parseItem) => parseItem.id === openItemInt);

    item && setTitle(`${title} - ${item.title}`);

    return (<div className={style.container}>
      <Button asLink to={backLink} className={style.backButton}>{messages.back}</Button>
      {item && <div className={style.body}>
        <div className={style.imageContainer}>
          <img className={style.image} src={getImageLink(item.id)} />
          <a title={messages.openResource} className={`${style.serviceLink} ${style[`${module}Link`]}`} target="_blank" href={`${outLink}${item[serviceIdTitle]}`} rel="noreferrer">{OutIcon(linkOutIcon, linkOutFile)}</a>
          {item.rating > 0 && <Text title={messages.rating} className={style.rating} type="h1" style={{ color: calculateRed(item.rating) }}>{item.rating}</Text>}
        </div>
        <div className={style.infoContainer}>
          <Text type="h1" className={style.title}>{item.title}</Text>
        </div>
        <Button asLink to={backLink} className={style.closeButton}>{messages.close}</Button>
      </div>}
    </div>);
  }
}

export default View;
