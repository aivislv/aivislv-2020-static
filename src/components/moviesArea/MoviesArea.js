// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import Text from '../../elements/text/Text';
import { getMovieImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { connectQuery, connectState } from '../../init/actions';
import { MOVIES_VIEW } from '../../init/paths';
import * as Filters from '../../views/movies/constants';
import Item from '../item/Item';

import style from './moviesArea.css';
import {Loader} from "../loader/loader";


type Props = {
  movies: Object,
  openFilter: string,
  openSort: string,
  openType: string,
  search: string,
  openMovie: number,
  sizeCurrent: number,
  scrollPosition: number,
}

type State = {
}

@connectState({
  openSort: ['movies', 'openSort'],
  openType: ['movies', 'openType'],
  openMovie: ['movies', 'openMovie'],
  search: ['movies', 'search'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewMovies.movies',
  name: 'movies',
})
class MoviesArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
  };

  componentDidUpdate(prevProps) {
    const {
      movies,
      openFilter,
      openSort,
      openType,
      search,
      openMovie,
      sizeCurrent,
      scrollPosition,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.openFilter !== openFilter || prevProps.openSort !== openSort || prevProps.openType !== openType || prevProps.search !== search) {
      force = true;
    }

    if ((!filteredData || force) && !movies.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE;

    if (!openMovie || !skipStillUpdate) {
      if ((!prevProps.openMovie || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      movies,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !movies.loading) {
      this.filterData();
    }
  }

  applyMovieType() {
    const {
      openType,
    } = this.props;

    switch (openType) {
      case Filters.MOVIE_TYPE_WISHLIST: return 0;
      case Filters.MOVIE_TYPE_WATCHED: return 1;
      default: return 2;
    }
  }

  applySortProps() {
    const {
      openSort,
    } = this.props;

    if (openSort && openSort !== Filters.MOVIE_SORT_NO) {
      switch (openSort) {
        case Filters.MOVIE_SORT_ALPHABET:
          return { key: 'title', direction: 'ASC' };
        case Filters.MOVIE_SORT_RATING:
          return { key: 'rating', direction: 'DESC' };
        case Filters.MOVIE_SORT_SCORE:
          return { key: 'score', direction: 'DESC' };
        case Filters.MOVIE_SORT_LAST_PLAY:
          return { key: 'seenUnix', direction: 'DESC' };
        default:
          return false;
      }
    }

    return false;
  }

  filterData = () => {
    const {
      movies,
      search,
    } = this.props;

    const gameType = this.applyMovieType();

    let filteredData = movies.data.movies.filter((item) => item.status === gameType);

    if (search && search.length > 2) {
      const useSearch = new RegExp(search, "gi");
      filteredData = filteredData.filter((item) => item.title.search(useSearch) > -1);
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
        filteredData = filteredData.sort((movie1, movie2) => {
          if (sortProps.direction === 'ASC') {
            return (movie1[sortProps.key] < movie2[sortProps.key]) ? -1 : (movie1[sortProps.key] === movie2[sortProps.key] ? 0 : 1);
          } else {
            return (movie1[sortProps.key] < movie2[sortProps.key]) ? 1 : (movie1[sortProps.key] === movie2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({ filteredData });
  };

  render = () => {
    const {
      movies,
      openMovie,
    } = this.props;

    const {
      filteredData
    } = this.state;

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.imdb')),
      rating: message('common.rating'),
    }

    return (<div className={openMovie ? `${style.body} ${style.hasOpenMovie}` : style.body}>
      {!movies.loading && filteredData && <Text type="h2" className={style.totalSelected}>{message('common.total').replace('#ITEMS#', filteredData.length)}</Text>}
      <div className={style.album}>
        {!movies.loading && filteredData && filteredData.map((movie) => (
          <Item
              itemData={movie}
              itemLink={MOVIES_VIEW}
              processImage={getMovieImageLink}
              linkOutFile="imdbLogo.png"
              outLink={`${config.movieImdbLink}${movie.imdbId}`}
              messages={renderedMessages}
              key={movie.id}
              service="imdb"
          />
        ))}
          {movies.loading && <Loader/>}
      </div>
    </div>);
  }
}

export default MoviesArea;
