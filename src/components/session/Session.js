// @flow

import React from 'react';

import path from 'path';

import Text from '../../elements/text/Text';
import { getImageLink } from '../../helpers/ProcessImages';
import { connectActions } from '../../init/actions';
import { PHOTOS_SESSIONS } from '../../init/paths';

import style from './session.css';


type Props = {
  data: Object,
  push: Function,
}

@connectActions({
  push: ['router', 'push'],
})
class Session extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const { data, push } = this.props;

    return (<div onClick={() => { window.scroll(0, 0); push(path.join(PHOTOS_SESSIONS, String(data.id)))}} className={style.thumb}>
      <img className={style.image} src={getImageLink(data.image, 500)} />
      <div className={style.titleContainer}><Text className={style.title} type="normal">{data.title}</Text></div>
    </div>);
  }
}

export default Session;
