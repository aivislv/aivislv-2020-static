import React from 'react';

import path from 'path';

import { config } from '../../config';
import Icon, { Goodreads } from '../../elements/icon';
import Link from '../../elements/link/Link';
import Text from '../../elements/text/Text';
import { getAuthorImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { AUTHORS_VIEW } from '../../init/paths';
import Image from '../image/Image';

import style from './authorSmall.css';


export default ({ author }) => {
  const goTo = path.join(AUTHORS_VIEW, String(author.id));

  return (<div key={author.id} className={style.author} title={author.name}>
    <div className={style.body}>
      <Link to={goTo}>
        <Image className={style.image} src={getAuthorImageLink(author.id)} />
      </Link>
      {author.goodreadsId !== 0 && <a className={style.bggLink} target="_blank" title={message('common.open').replace('#SOURCE#', message('specs.gr'))}
        href={`${config.authorGoodreadsLink}/${author.goodreadsId}/`} rel="noreferrer"><Icon><Goodreads/></Icon></a>}
    </div>
    <Link to={goTo}>
      <Text type="h2" className={style.title}>{author.name}</Text>
    </Link>
  </div>)
};
