// @flow

import React from 'react';

import path from 'path';

import { config } from '../../config';
import Button from '../../elements/button/Button';
import Icon, { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import {getBookImageLink} from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import {message} from '../../helpers/Translate';
import {connectActions, connectQuery, connectState} from '../../init/actions';
import {SERIES_LIST} from '../../init/paths';
import {SERIES_BOOKS_WRAP_COUNT} from '../../views/series/constants';
import BooksSmall from '../booksSmall/BooksSmall';
import Image from '../image/Image';

import style from './seriesView.css';
import SeriesSmall from "../seriesSmall/SeriesSmall";
import AuthorSmall from "../authorSmall/AuthorSmall";


type
Props = {
  seriesData: Object,
  isOpenSerie: Object,
  push: Function,
  search: string,
}

@connectActions({
  push: ['router', 'push'],
})
@connectState({
  search: ['series', 'search'],
  openSerie: ['series', 'openSerie'],
})
@connectQuery({
  query: 'viewSeries.serie',
  name: 'seriesData',
  vars: {
    seriesId: (props) => Number(props.openSerie)
  },
  loading: (props) => !props.openSerie,
})
class SeriesView extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  sortBooks = (book1, book2) => {
    if (book1['ordered'] === 99999) {
      return 1;
    }

    if (book2['ordered'] === 99999) {
      return -1;
    }

    return (book1['ordered'] < book2['ordered']) ? -1 : (book1['ordered'] === book2['ordered'] ? 0 : 1);
  }

  sortAuthors = (book1, book2) => (book1['name'] < book2['name']) ? -1 : (book1['name'] === book2['name'] ? 0 : 1);

  render = () => {
    const {
      seriesData,
      search,
      isOpenSerie,
      push,
    } = this.props;

    let serie;

    if (!seriesData.loading && seriesData?.data?.serie) {
      serie = seriesData.data.serie;
      setTitle(`Serie - ${serie.title}`);
    }

    if (!seriesData.loading && !seriesData.data) {
      push(SERIES_LIST);
      return <></>;
    }

    const isFullscreen = !isOpenSerie || (!seriesData.loading && seriesData?.data?.serie?.books && seriesData.data.serie.books.length >= SERIES_BOOKS_WRAP_COUNT);

    return (!seriesData.loading && <div className={isFullscreen ? style.coverAllContainer : style.container}>
      <Button asLink to={path.join(SERIES_LIST, search || '')} className={style.backButton}>{message('common.back')}</Button>
      {seriesData.data.serie && <div className={style.body}>
        <div className={style.imageContainer}>
          <Image className={style.image} src={getBookImageLink(serie.firstBookId)} />
          {serie.goodreadsId !== 0 && <a title={message('common.open').replace('#SOURCE#', message('specs.gr'))} className={style.bggLink} target="_blank" href={`${config.seriesGoodreadsLink}${serie.goodreadsId}/`} rel="noreferrer"><Icon><Goodreads /></Icon></a>}
        </div>

        <div className={style.infoContainer}>
          <Text type="h1" className={style.title}>{serie.title}</Text>
        </div>

        {serie.books && serie.books.length > 0 && <div className={style.booksContainer}>
          <Text type="h1" className={style.title}>{message('components.seriesView.texts.books')}</Text>
          <div className={style.booksList}>
            {serie.books.sort(this.sortBooks).map((book, key) => <BooksSmall isFullscreen={isFullscreen} key={key}
                                                                             book={book.book} inSeries={book}
                                                                             showAuthors/>)}
          </div>
        </div>}

        {serie.authors && serie.authors.length > 0 && <div className={style.booksContainer}>
          <Text type="h1" className={style.title}>{message('components.seriesView.texts.authors')}</Text>
          <div className={style.booksList}>
            {serie.authors.sort(this.sortAuthors).map((authors, key) => <AuthorSmall isFullscreen={isFullscreen}
                                                                                     key={key} author={authors}/>)}
          </div>
        </div>}

        <Button asLink to={path.join(SERIES_LIST, search || '')}
                className={style.closeButton}>{message('common.close')}</Button>
      </div>}
    </div>);
  }
}

export default SeriesView;
