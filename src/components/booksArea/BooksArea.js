// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import {getBookImageLink} from '../../helpers/ProcessImages';
import {message} from '../../helpers/Translate';
import {connectQuery, connectState} from '../../init/actions';
import {BOOKS_VIEW} from '../../init/paths';
import * as Filters from '../../views/books/constants';
import Item from '../item/Item';

import style from './booksArea.css';
import {Loader} from "../loader/loader";
import BookFilters from "../bookFilters/BookFilters";
import {Button} from "../../elements";


type
Props = {
  books: Object,
  openType: string,
  search: string,
  openBook: number,
  sizeCurrent: number,
  scrollPosition: number,
  openOwned: string,
  openSort: string,
}

type State = {
}

@connectState({
  openType: ['books', 'openType'],
  openBook: ['books', 'openBook'],
  openOwned: ['books', 'openOwned'],
  openTags: ['books', 'openTags'],
  openSort: ['books', 'openSort'],
  search: ['books', 'search'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewBooks.books',
  name: 'books',
})
class BooksArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
    openedFilters: false
  };

  componentDidUpdate(prevProps) {
    const {
      books,
      openType,
      openSort,
      search,
      openBook,
      openOwned,
      sizeCurrent,
      scrollPosition,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.openOwned !== openOwned || prevProps.openType !== openType || prevProps.search !== search || prevProps.openSort !== openSort) {
      force = true;
    }

    if ((!filteredData || force) && !books.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE;

    if (!openBook || !skipStillUpdate) {
      if ((!prevProps.openBook || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      books,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !books.loading) {
      this.filterData();
    }
  }

  applyBookType() {
    const {
      openType,
    } = this.props;

    switch (openType) {
      case Filters.BOOK_TYPE_HAD_READ:
        return {attribute: 'isRead', value: 2};
      case Filters.BOOK_TYPE_READING:
        return {attribute: 'isRead', value: 1};
      case Filters.BOOK_TYPE_WISHLIST:
        return {attribute: 'isRead', value: 0};
      default:
        return false;
    }
  }

  applyBookOwned() {
    const {
      openOwned,
    } = this.props;

    switch (openOwned) {
      case Filters.BOOK_OWNERSHIP_YES:
        return {attribute: 'isOwned', value: 1};
      case Filters.BOOK_OWNERSHIP_NO:
        return {attribute: 'isOwned', value: 0};
      default:
        return false;
    }
  }

  applySortProps() {
    const {
      openSort
    } = this.props;

    switch (openSort) {
      case Filters.BOOK_SORT_ALPHABET:
        return {key: 'title', direction: 'ASC'};
      case Filters.BOOK_SORT_WISH_PRIORITY:
        return {key: 'wishWeight', direction: 'DESC'};
      default:
        return false;
    }
  }

  searchABook(list, needle) {
    const useSearch = new RegExp(needle, "gi");

    return list.filter((item) => {
      if (item.title.search(useSearch) > -1) {
         return true;
       }

       const isAuthor = item.authors.reduce((result, author) => {
         if (result || author.name.search(useSearch) > -1) {
           return true;
         }
         return false;
       }, false);

       if (isAuthor) {
         return true;
       }

      const isSeries = item.series.reduce((result, serie) => {
        if (result || serie.series.title.search(useSearch) > -1) {
          return true;
        }
      }, false);

      if (isSeries) {
        return true;
      }

      return false;
    })
  }

  filterData = () => {
    const {
      books,
      search,
    } = this.props;

    const gameType = this.applyBookType();
    const gameOwned = this.applyBookOwned();

    let filteredData;

    filteredData = gameType ? books.data.books.filter((item) => item[gameType.attribute] === gameType.value) : books.data.books

    filteredData = gameOwned ? filteredData.filter((item) => item[gameOwned.attribute] === gameOwned.value) : filteredData

    if (search && search.length > 2) {
      filteredData = this.searchABook(filteredData, search);
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
      filteredData = filteredData.sort((book1, book2) => {
        if (sortProps.direction === 'ASC') {
            return (book1[sortProps.key] < book2[sortProps.key]) ? -1 : (book1[sortProps.key] === book2[sortProps.key] ? 0 : 1);
          } else {
            return (book1[sortProps.key] < book2[sortProps.key]) ? 1 : (book1[sortProps.key] === book2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({filteredData});
  };

  filterTag = (book) => {
    const {
      openTags
    } = this.props;

    let isInList = true;

    if (openTags.length > 0) {
      isInList = false;
      let countTags = 0;
      book.tags.forEach((tag) => {
        if (openTags.includes(tag.tag.id)) {
          countTags++
        }
      })

      if (countTags == openTags.length) {
        isInList = true
      }
    }

    return isInList;
  };

  openFilters = () => {
    this.setState({"openedFilters": !this.state.openedFilters})
  }

  render = () => {
    const {
      openBook,
      books,
    } = this.props;

    const {
      filteredData
    } = this.state;

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.gr')),
      rating: message('common.rating'),
      owned: message('components.booksArea.titles.owned'),
      readStatus: [
        message('components.booksArea.titles.wishlist'),
        message('components.booksArea.titles.reading'),
        message('components.booksArea.titles.complete'),
      ],
    };

    const tagFilter = filteredData ? filteredData.filter(this.filterTag) : null;

    return (<div className={style.body}>
      <div className={style.filterAndTotal}>
        <Button onClick={this.openFilters}>Filters</Button>
        {!books.loading && tagFilter && <Text className={style.totalSelected}
                                              type="h2">{message('common.total').replace('#ITEMS#', tagFilter.length)}</Text>}
      </div>

      <div className={style.album}>
        {!books.loading && tagFilter && tagFilter.map((book) => (
            <Item
                itemData={book}
                isBook
                itemLink={BOOKS_VIEW}
                processImage={getBookImageLink}
                linkOutIcon={Goodreads}
                outLink={book.goodreadsId !== 0 && `${config.bookGoodreadsLink}${book.goodreadsId}`}
                messages={renderedMessages}
                key={book.id}
                service="goodreads"
              ratingMax={5}
          />
        ))}
        {books.loading && <Loader/>}
      </div>
      {this.state.openedFilters && <BookFilters closeFilters={this.openFilters}/>}
    </div>);
  }
}

export default BooksArea;
