import React, { useState } from 'react';

import { config } from '../../config';

import style from './image.css';


export default ({ src, className, onError = null, onLoad = null, autoFail = false }) => {
  const [is404, setIs404] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  return <img
    onError={() => {onError && onError(); setIs404(true)}}
    onLoad={() => {onLoad && onLoad(); setIsLoaded(true)}}
    className={`${className} ${!isLoaded && style.hide}`}
    src={(is404 || autoFail) ? `${config.staticFiles}/noimage.jpg` : src}
  />
};
