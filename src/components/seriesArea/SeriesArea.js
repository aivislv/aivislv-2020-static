// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { getBookImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { connectQuery, connectState } from '../../init/actions';
import { SERIES_VIEW } from '../../init/paths';
import { SERIES_BOOKS_WRAP_COUNT } from '../../views/series/constants';
import Item from '../item/Item';

import style from './seriesArea.css';
import {Loader} from "../loader/loader";


type Props = {
  series: Object,
  search: string,
  openSerie: number,
  sizeCurrent: number,
  scrollPosition: number,
  isOpenSerie: Object,
}

@connectState({
  openSerie: ['series', 'openSerie'],
  search: ['series', 'search'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewSeries.series',
  name: 'series',
})
class SeriesArea extends React.PureComponent<Props> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
  };

  componentDidUpdate(prevProps) {
    const {
      series,
      search,
      openSerie,
      sizeCurrent,
      scrollPosition,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.search !== search) {
      force = true;
    }

    if ((!filteredData || force) && !series.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE || prevProps.isOpenSerie?.booksLinked > SERIES_BOOKS_WRAP_COUNT;

    if (!openSerie || !skipStillUpdate) {
      if ((!prevProps.openSerie || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      series,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !series.loading) {
      this.filterData();
    }
  }

  applySortProps() {
    return { key: 'title', direction: 'ASC' };
  }

  filterData = () => {
    const {
      series,
      search,
    } = this.props;

    let filteredData = series.data.series;

    if (search && search.length > 2) {
      const useSearch = new RegExp(search, "gi");
      filteredData = filteredData.filter((item) => item.title.search(useSearch) > -1);
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
        filteredData = filteredData.sort((serie1, serie2) => {
          if (sortProps.direction === 'ASC') {
            return (serie1[sortProps.key] < serie2[sortProps.key]) ? -1 : (serie1[sortProps.key] === serie2[sortProps.key] ? 0 : 1);
          } else {
            return (serie1[sortProps.key] < serie2[sortProps.key]) ? 1 : (serie1[sortProps.key] === serie2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({ filteredData });
  };

  render = () => {
    const {
      series,
      isOpenSerie,
      openSerie,
    } = this.props;

    const {
      filteredData
    } = this.state;

    let renderClass;

    if (openSerie !== null && !isOpenSerie) {
      renderClass = `${style.body} ${style.hasOpenSerieBooks}`
    } else {
      renderClass = isOpenSerie
        ? (
          isOpenSerie.booksLinked >= SERIES_BOOKS_WRAP_COUNT
            ? `${style.body} ${style.hasOpenSerieBooks}`
            : `${style.body} ${style.hasOpenSerie}`
        ) : style.body;
    }

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.gr'))
    };

    return (<div className={renderClass}>
      {!series.loading && filteredData && <Text type="h2"
                                                className={style.totalSelected}>{message('common.total').replace('#ITEMS#', filteredData.length)}</Text>}
      {series.loading && <Loader/>}
      <div className={style.album}>
        {!series.loading && filteredData && filteredData.map((serie) => {
           return <Item
               itemData={serie}
               itemLink={SERIES_VIEW}
               processImageSrc={getBookImageLink(serie.firstBookId)}
               linkOutIcon={Goodreads}
               outLink={serie.goodreadsId !== 0 && `${config.seriesGoodreadsLink}${serie.goodreadsId}`}
               messages={renderedMessages}
               key={serie.id}
               service="goodreads"
               ratingMax={5}
           />
            }
        )}
        {series.loading && <Loader/>}
      </div>
    </div>);
  }
}

export default SeriesArea;
