// @flow

import React from 'react';
import {connectActions, connectQuery, connectState} from "../../init/actions";

import style from './gameFilters.css';
import {Section} from "./Section";
import {createGamePath} from "../../helpers/DynamicPaths";
import {GAMES_SHOW} from "../../init/paths";
import * as Filters from "../../views/games/constants";
import {Button} from "../../elements";
import {TagSection} from "./TagSection";
import {GAME_SORT_WISH_WEIGHT} from "../../views/games/constants";


const filterList = [
    {value: Filters.GAME_FILTER_PLAYED, title: "Played"},
    {value: Filters.GAME_FILTER_UNPLAYED, title: "Unplayed"},
    {value: Filters.GAME_FILTER_1P, title: "Solo"},
    {value: Filters.GAME_FILTER_2P, title: "2 Player"},
    {value: Filters.GAME_FILTER_2P_ONLY, title: "2 Player only"},
    {value: Filters.GAME_FILTER_3P, title: "3 Player"},
    {value: Filters.GAME_FILTER_4P, title: "4 Player"},
    {value: Filters.GAME_FILTER_5P, title: "5 Player"},
    {value: Filters.GAME_FILTER_6P, title: "6 Player"},
    {value: Filters.GAME_FILTER_7P, title: "7 Player"},
    {value: Filters.GAME_FILTER_8P, title: "8 Player"},
    {value: Filters.GAME_FILTER_9P, title: "9 Player"},
    {value: Filters.GAME_FILTER_SHORT, title: "Short"},
    {value: Filters.GAME_FILTER_MEDIUM, title: "Medium"},
    {value: Filters.GAME_FILTER_LONG, title: "Long"},
]

const sortList = [
    {value: Filters.GAME_SORT_ALPHABET, title: "A-Z"},
    {value: Filters.GAME_SORT_RATING, title: "My Rating"},
    {value: Filters.GAME_SORT_PLAYS, title: "Play count"},
    {value: Filters.GAME_SORT_LAST_PLAY, title: "Last Play"},
    {value: Filters.GAME_SORT_SCORE, title: "Average Score"},
    {value: Filters.GAME_SORT_WISHLIST, title: "Wishlist priority"},
    {value: Filters.GAME_SORT_WISH_WEIGHT, title: "Play wish weight"},
]

@connectActions({
    push: ['router', 'push'],
})
@connectState({
    openFilter: ['games', 'openFilter'],
    openSort: ['games', 'openSort'],
    openType: ['games', 'openType'],
    openTags: ['games', 'openTags'],
    search: ['games', 'search'],
})
@connectQuery({
    query: 'viewGames.tags',
    name: 'tagsList',
})
class GameFilters extends React.PureComponent {
    static defaultProps = {};
    state = {
        search: ""
    };

    navigateTo = (type, value) => {
        const {
            openType,
            openSort,
            openTags,
            openFilter,
            search,
            push
        } = this.props;

        const options = {
            type: openType,
            sort: openSort,
            tags: openTags,
            filter: openFilter,
            search
        }

        options[type] = value

        push(createGamePath(GAMES_SHOW, options))
    }

    navigateFilterTo = (value) => {
        const {openTags} = this.props

        if (value === null) {
            this.navigateTo("tags", [])
            return;
        }

        const itemId = openTags.indexOf(value);

        let newArray = [];

        if (itemId > -1) {
            newArray = [...openTags.splice(0, itemId), ...openTags.splice(itemId + 1)];
        } else {
            newArray = [value, ...openTags]
        }

        this.navigateTo("tags", newArray)
    }

    prepTagList = () => {
        const {tagsList, openTags} = this.props
        const {search} = this.state

        if (!tagsList.loading && tagsList.data) {
            let orderByCats = []

            tagsList.data.tags
                .filter(tag => tag.promote)
                .filter(tag => search.length < 1 || tag.tag.toLowerCase().includes(search.toLowerCase()))
                .sort((tagA, tagB) => tagA.tag > tagB.tag ? 1 : -1)
                .forEach((tag) => {
                    let catId = orderByCats.findIndex((cats) => cats.id === tag.category.id);
                    if (catId == -1) {
                        catId = orderByCats.push({
                            id: tag.category.id,
                            name: tag.category.name,
                            color: tag.category.color,
                            tags: []
                        }) - 1
                    }

                    orderByCats[catId].tags.push({
                        title: tag.tag,
                        value: tag.id,
                        isSelected: openTags.includes(tag.id)
                    })
                })


            return orderByCats
        }

        return []
    }

    render = () => {
        const {
            openSort,
            openFilter,
            closeFilters,
        } = this.props;

        const {search} = this.state

        const handleSearch = (e) => {
            this.setState({"search": e.target.value})
        }

        const handleEscape = (e) => {
            if (e.keyCode === 27) {
                this.setState({"search": ""})
            }
        }

        return (<div className={style.overlay}>
            <div className={style.closeContainer}><Button className={style.close} onClick={closeFilters}>X</Button>
            </div>
            <div className={style.body}>
                <Section title="Filter" reset={Filters.GAME_FILTER_NO} list={filterList} current={openFilter}
                         type="filter" link={this.navigateTo}/>
                <Section title="Sort" reset={Filters.GAME_SORT_NO} list={sortList} current={openSort} type="sort"
                         link={this.navigateTo}/>
                <TagSection handleSearch={handleSearch} handleEscape={handleEscape} search={search} title="Tags"
                            list={this.prepTagList()} link={this.navigateFilterTo}/>
            </div>
        </div>)
    }
}

export default GameFilters;
