// @flow

import React from 'react';
import {Button} from "../../elements";
import style from "./gameFilters.css";
import Text from "../../elements/text/Text";

export const TagSection = ({title, list, link, handleSearch, handleEscape, search}) => {
    return <div className={style.section}>
        <Text type="h2" className={style.title}>{title}</Text>
        <div className={style.tagSectionBody}>
            <div key="reset" className={style.top}>
                <Button onClick={() => link(null)}>Reset</Button>
                <input placeholder="Search..." value={search} onKeyUp={handleEscape} onChange={handleSearch}/>
            </div>
            {list.map((cat) => {
                return (<div key={cat.id} className={style.category} style={{borderColor: `#${cat.color}`}}>
                    <Text type="h3" className={style.catTitle}>{cat.name}</Text>
                    {
                        cat.tags.map((listItem) => {
                            return (<div key={listItem.value} className={style.button}><Button
                                style={{backgroundColor: listItem.isSelected ? `#aaaaaa` : `#eeeeee`}}
                                onClick={() => link(listItem.value)}>{listItem.title}</Button></div>)
                        })
                    }</div>)
            })}
        </div>
    </div>
}