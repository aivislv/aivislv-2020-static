// @flow

import React from 'react';

import Button from '../../elements/button/Button';
import Text from '../../elements/text/Text';
import { message } from '../../helpers/Translate';
import { ABOUT } from '../../init/paths';

import style from './footer.css';


type Props = {
}

type State = {
}

class Footer extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    dropDown: false,
  };

  render = () => {
    // const { dropDown } = this.state;

    // const displayMenu = dropDown ? style.menuDisplay : '';
    // const displayToogle = !dropDown ? style.toogleDisplay : '';

    const year = (new Date()).getFullYear();

    return (<div className={style.body}>
      <div className={style.about}>
        <Button asLink to={ABOUT} className={style.logoButton} theme="only-black"><Text type="normal" className={style.copyright}>{`${message('components.footer.texts.copyright')} , 2014 - ${year}`}</Text></Button>
      </div>
    </div>);
  }
}

export default Footer;
