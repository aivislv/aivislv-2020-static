// @flow

import React from 'react';

import path from 'path';

import Text from '../../elements/text/Text';
import { getImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { connectActions, connectState } from '../../init/actions';
import { PHOTOS } from '../../init/paths';

import style from './album.css';


type Props = {
  data: Object,
  openGalley: number,
  openAlbum: number,
  push: Function,
}

@connectState({
  openAlbum: ['photos', 'openAlbum'],
})
@connectActions({
  push: ['router', 'push'],
})
class Album extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const { data, openAlbum, push } = this.props;

    if (openAlbum === data.id) {
      setTitle(data.title);
    }

    return (<div onClick={() => { window.scroll(0, 0); push(path.join(PHOTOS, String(data.id)))}} className={`${style.thumb} ${openAlbum === data.id ? style.open : ''}`}>
      {data.image && <img className={style.image} src={getImageLink(data.image, 250)} />}
      <div className={style.titleContainer}><Text className={style.title} type="normal">{data.title}</Text></div>
    </div>);
  }
}

export default Album;
