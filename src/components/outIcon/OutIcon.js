import React from 'react';

import { config } from '../../config';
import Icon from '../../elements/icon';


export const OutIcon = (linkOutIcon, linkOutFile) => {
  if (linkOutIcon) {
    const ServiceIcon = linkOutIcon;
    return <Icon><ServiceIcon/></Icon>
  } else {
    return <img src={`${config.staticFiles}${linkOutFile}`} />
  }
};