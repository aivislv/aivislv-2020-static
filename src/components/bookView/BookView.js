// @flow

import React from 'react';


import { faBook, faBookReader } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import path from 'path';

import { config } from '../../config';
import Button from '../../elements/button/Button';
import Icon, { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { calculateRed } from '../../helpers/HexDec';
import { getBookImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { BOOKS_LIST } from '../../init/paths';
import AuthorSmall from '../authorSmall/AuthorSmall';
import SeriesSmall from '../seriesSmall/SeriesSmall';

import style from './bookView.css';
import {createBookPath} from "../../helpers/DynamicPaths";


type Props = {
  bookData: Object,
  openType: string,
  search: string,
  push: Function,
}

@connectActions({
  push: ['router', 'push'],
})
@connectState({
  openType: ['books', 'openType'],
  search: ['books', 'search'],
  openBook: ['books', 'openBook'],
  openTags: ['books', 'openTags'],
})
@connectQuery({
  query: 'viewBooks.book',
  name: 'bookData',
  vars: {
    bookId: (props) => Number(props.openBook)
  },
  loading: (props) => !props.openBook,
})
class BookView extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      bookData,
      openType,
      openTags,
      search,
      push,
    } = this.props;

    let book;

    if (!bookData.loading && bookData?.data?.book) {
      book = bookData.data.book;
      setTitle(`Book - ${book.title}`);
    }

    if (!bookData.loading && !bookData.data) {
      push(BOOKS_LIST);
      return <></>;
    }

    const readStatus = [
      message('components.bookView.titles.wishlist'),
      message('components.bookView.titles.reading'),
      message('components.bookView.titles.complete'),
    ];

    return (!bookData.loading && <div className={style.container}>
      <Button asLink to={path.join(BOOKS_LIST, openType, search || '')} className={style.backButton}>{message('common.back')}</Button>
      {bookData.data.book && <div className={style.body}>
        <div className={style.imageContainer}>
          <img className={style.image} src={getBookImageLink(book.id)} />
          {book.goodreadsId !== 0 &&
              <a title={message('common.open').replace('#SOURCE#', message('specs.gr'))} className={style.bggLink}
                 target="_blank" href={`${config.bookGoodreadsLink}${book.goodreadsId}/`}
                 rel="noreferrer"><Icon><Goodreads/></Icon></a>}
          {book.rating > 0 && <Text title={message('common.rating')} className={style.rating} type="h1"
                                    style={{color: calculateRed(book.rating, 5)}}>{book.rating}</Text>}
          <div className={style[`isRead-${book.isRead}`]} title={readStatus[book.isRead]}><FontAwesomeIcon
              icon={faBookReader}/></div>
          {book.isOwned === 1 &&
              <div className={style.isOwned} title={message('components.bookView.titles.owned')}><FontAwesomeIcon
                  icon={faBook}/></div>}
        </div>
        <div className={style.infoContainer}>
          <Text type="h1" className={style.title}>{book.title}</Text>
          <Text type="h1"
                className={style.title}>{message(book.isOwned === 1 ? 'components.bookView.texts.isOwnedY' : 'components.bookView.texts.isOwnedN')}</Text>
          <Text type="h1"
                className={style.title}>{`${message('components.bookView.texts.readStatus')} ${readStatus[book.isRead]}`}</Text>
        </div>

        <div className={style.tagContainer}>
          <Text type="h1" className={style.tagTitle}>Tags:</Text>
          {book.tags.map((tag) => (
              <Button
                  className={style.tag}
                  key={tag.tag.id}
                  to={createBookPath(BOOKS_LIST, {tags: [tag.tag.id]})}
                  asLink
              >
                {tag.tag.title}
              </Button>
          ))}
        </div>

        {book.authors.length > 0 && <div className={style.authorContainer}>
          <Text type="h1"
                className={style.title}>{book.authors.length > 1 ? message('components.bookView.texts.authors') : message('components.bookView.texts.author')}</Text>
          <div className={style.authorList}>
            {book.authors.map((author, key) => <AuthorSmall key={key} author={author}/>)}
          </div>
        </div>}

        {book.series.length > 0 && <div className={style.seriesContainer}>
          <Text type="h1" className={style.title}>{message('components.bookView.texts.series')}</Text>
          <div className={style.seriesList}>
            {book.series.map((series, key) => <SeriesSmall key={key} series={series.series} />)}
          </div>
        </div>}

        <Button asLink to={createBookPath(BOOKS_LIST, {type: openType, search, tags: openTags})}
                className={style.closeButton}>{message('common.close')}</Button>
      </div>}
    </div>);
  }
}

// bookData?.lastPlay?.date && dayjs(bookData.lastPlay.date).format('YYYY.MM.DD')

export default BookView;
