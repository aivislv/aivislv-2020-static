// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import { Bgg } from '../../elements/icon';
import Text from '../../elements/text/Text';
import {getGameImageLink} from '../../helpers/ProcessImages';
import {message} from '../../helpers/Translate';
import {connectQuery, connectState} from '../../init/actions';
import {GAMES_GAME} from '../../init/paths';
import * as Filters from '../../views/games/constants';
import {GAME_EXPANSIONS_MAX, GAME_SORT_WISHLIST} from '../../views/games/constants';
import Item from '../item/Item';

import style from './gameArea.css';
import {Loader} from "../loader/loader";
import GameFilters from "../gameFilters/GameFilters";
import {Button} from "../../elements";


type
Props = {
  games: Object,
  openFilter: string,
  openSort: string,
  openType: string,
  search: string,
  openGame: number,
  sizeCurrent: number,
  scrollPosition: number,
  isOpenGame: Object,
}

type State = {
}

@connectState({
  openFilter: ['games', 'openFilter'],
  openSort: ['games', 'openSort'],
  openType: ['games', 'openType'],
  openGame: ['games', 'openGame'],
  search: ['games', 'search'],
  selectId: ['games', 'selectId'],
  openTags: ['games', 'openTags'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewGames.games',
  name: 'games',
})
class GameArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
    openedFilters: false
  };

  componentDidUpdate(prevProps) {
    const {
      games,
      openFilter,
      openSort,
      openType,
      search,
      scrollPosition,
      openGame,
      sizeCurrent,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.openFilter !== openFilter || prevProps.openSort !== openSort || prevProps.openType !== openType || prevProps.search !== search) {
      force = true;
    }

    if ((!filteredData || force) && !games.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE || prevProps.isOpenGame?.expansionCount > GAME_EXPANSIONS_MAX;

    if (!openGame || !skipStillUpdate) {
      if ((!prevProps.openGame || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      games,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !games.loading) {
      this.filterData();
    }
  }

  applyGameType() {
    const {
      openType,
    } = this.props;

    switch (openType) {
      case Filters.GAME_TYPE_WISHLIST:
        return 2;
      case Filters.GAME_TYPE_PLAYED:
        return 3;
      default:
        return 1
    }
  }

  applySortProps() {
    const {
      openSort,
    } = this.props;

    if (openSort && openSort !== Filters.GAME_SORT_NO) {
      switch (openSort) {
        case Filters.GAME_SORT_ALPHABET:
          return { key: 'title', direction: 'ASC' };
        case Filters.GAME_SORT_LAST_PLAY:
          return { key: 'lastPlayUnix', direction: 'DESC' };
        case Filters.GAME_SORT_RATING:
          return { key: 'rating', direction: 'DESC' };
        case Filters.GAME_SORT_PLAYS:
          return { key: 'plays', direction: 'DESC' };
        case Filters.GAME_SORT_SCORE:
          return { key: 'avgScore', direction: 'DESC' };
        case Filters.GAME_SORT_WISH_WEIGHT:
          return {key: 'wishWeight', direction: 'DESC'};
        case Filters.GAME_SORT_WISHLIST:
          return {skip0: true, key: 'wishlistPriority', direction: 'ASC'};
        default:
          return false;
      }
    }

    return false;
  }

  applyFilterProps() {
    const {
      openFilter,
    } = this.props;

    if (openFilter && openFilter !== Filters.GAME_FILTER_NO) {
      switch (openFilter) {
        case Filters.GAME_FILTER_1P: {
          return [{ key: 'minPlayers', value: 1, rule: '>=' }];
        }
        case Filters.GAME_FILTER_2P: {
          return [{ key: 'minPlayers', value: 2, rule: '<=' }, { key: 'maxPlayers', value: 2, rule: '>=' }];
        }
        case Filters.GAME_FILTER_2P_ONLY: {
          return [{ key: 'minPlayers', value: 2, rule: '=' }, { key: 'maxPlayers', value: 2, rule: '=' }];
        }
        case Filters.GAME_FILTER_3P: {
          return [{ key: 'minPlayers', value: 3, rule: '<=' }, { key: 'maxPlayers', value: 3, rule: '>=' }];
        }
        case Filters.GAME_FILTER_4P: {
          return [{ key: 'minPlayers', value: 4, rule: '<=' }, { key: 'maxPlayers', value: 4, rule: '>=' }];
        }
        case Filters.GAME_FILTER_5P: {
          return [{ key: 'minPlayers', value: 5, rule: '<=' }, { key: 'maxPlayers', value: 5, rule: '>=' }];
        }
        case Filters.GAME_FILTER_6P: {
          return [{ key: 'minPlayers', value: 6, rule: '<=' }, { key: 'maxPlayers', value: 6, rule: '>=' }];
        }
        case Filters.GAME_FILTER_7P: {
          return [{ key: 'minPlayers', value: 7, rule: '<=' }, { key: 'maxPlayers', value: 7, rule: '>=' }];
        }
        case Filters.GAME_FILTER_8P: {
          return [{ key: 'minPlayers', value: 8, rule: '<=' }, { key: 'maxPlayers', value: 8, rule: '>=' }];
        }
        case Filters.GAME_FILTER_9P: {
          return [{ key: 'minPlayers', value: 9, rule: '<=' }, { key: 'maxPlayers', value: 9, rule: '>=' }];
        }
        case Filters.GAME_FILTER_UNPLAYED: {
          return [{ key: 'plays', value: 0, rule: '=' }];
        }
        case Filters.GAME_FILTER_PLAYED: {
          return [{ key: 'plays', value: 0, rule: '>' }];
        }
        case Filters.GAME_FILTER_SHORT: {
          return [{ key: 'maxPlaytime', value: 31, rule: '<' }];
        }
        case Filters.GAME_FILTER_MEDIUM: {
          return [{ key: 'maxPlaytime', value: 30, rule: '>' }, { key: 'maxPlaytime', value: 91, rule: '<' }];
        }
        case Filters.GAME_FILTER_LONG: {
          return [ { key: 'maxPlaytime', value: 90, rule: '>' }];
        }
        default:
          return false;
      }
    }

    return false;
  }

  filterData = () => {
    const {
      games,
      search,
    } = this.props;

    const gameType = this.applyGameType();

    let filteredData = games.data.games.filter((item) => item.type === gameType);

    if (search && search.length > 2) {
      const useSearch = new RegExp(search, "gi");
      filteredData = filteredData.filter((item) => item.title.search(useSearch) > -1);
    }

    const filterProps = this.applyFilterProps();

    if (filterProps) {
      filteredData = filteredData.filter((game) => {
        return filterProps.reduce((canUse, prop) => {
          if (canUse) {
            const compare = game[prop.key];
            switch (prop.rule) {
              case '>':
                return compare > prop.value;
              case '<':
                return compare < prop.value;
              case '>=':
                return compare >= prop.value;
              case '<=':
                return compare <= prop.value;
              case '=':
                return compare === prop.value;
            }
          }

          return canUse
        }, true)
      })
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
        filteredData = filteredData.sort((game1, game2) => {
            if (sortProps.skip0 !== undefined) {
                if (game1.wishlistPriority === 0) {
                    return 1
                }

                if (game2.wishlistPriority === 0) {
                    return -1
                }
            }

          if (sortProps.direction === 'ASC') {
            return (game1[sortProps.key] < game2[sortProps.key]) ? -1 : (game1[sortProps.key] === game2[sortProps.key] ? 0 : 1);
          } else {
            return (game1[sortProps.key] < game2[sortProps.key]) ? 1 : (game1[sortProps.key] === game2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({ filteredData });
  };

  filterTag = (game) => {
    const {
      openTags
    } = this.props;

    let isInList = true;

    if (openTags.length > 0) {
      isInList = false;
      let countTags = 0;
      game.tags.forEach((tag) => {
        if (openTags.includes(tag.id)) {
          countTags++
        }
      })

      if (countTags == openTags.length) {
        isInList = true
      }
    }

    return isInList;
  };

  openFilters = () => {
    this.setState({"openedFilters": !this.state.openedFilters})
  }

  render = () => {
    const {
      games,
      isOpenGame,
    } = this.props;

    const {
      filteredData
    } = this.state;

    const renderClass = isOpenGame
      ? (
        isOpenGame.expansionCount > GAME_EXPANSIONS_MAX
        ? `${style.body} ${style.hasOpenGameExps}`
          : `${style.body} ${style.hasOpenGame}`
      ) : style.body;

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.bgg')),
      rating: message('common.rating'),
      playCount: message('components.gameArea.titles.playCount'),
      lastPlay: message('components.gameArea.titles.lastPlay'),
      preordered: message('components.gameArea.titles.preordered'),
      wishlistPriority: message('components.gameArea.titles.wishlistPriority'),
    };

    const tagFilter = filteredData ? filteredData.filter(this.filterTag) : null;

    return (<div className={renderClass}>
      <div className={style.filterAndTotal}>
        <Button onClick={this.openFilters}>Filters</Button>
        {!games.loading && tagFilter && <Text className={style.totalSelected}
                                              type="h2">{message('common.total').replace('#ITEMS#', tagFilter.length)}</Text>}
      </div>
      <div className={style.album}>
        {!games.loading && tagFilter && tagFilter.map((game) => (<Item
            itemData={game}
            isGame
            itemLink={GAMES_GAME}
            processImage={getGameImageLink}
            linkOutIcon={Bgg}
            outLink={`${config.gameBggLink}${game.bggId}`}
            messages={renderedMessages}
            key={game.id}
            service="bgg"
        />))}
        {games.loading && <Loader/>}
      </div>
      {this.state.openedFilters && <GameFilters closeFilters={this.openFilters}/>}
    </div>);
  }
}

export default GameArea;
