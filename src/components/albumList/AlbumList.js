// @flow

import React from 'react';

import { Button } from '../../elements';
import Text from '../../elements/text/Text';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { PHOTOS_SESSIONS } from '../../init/paths';
import { PHOTO_MODE_ALBUM } from '../../views/photos/constants';
import Album from '../album/Album';

import style from './albumList.css';


type Props = {
  gallery: Object,
  push: Function,
  isPhoto: boolean,
  loadAlbum?: number,
  openAlbum?: number,
  setOpenAlbum?: Function,
  section?: string,
}

type State = {
}

@connectState({
  openAlbum: ['photos', 'openAlbum'],
})
@connectActions({
  setOpenAlbum: ['photos', 'setOpenAlbum'],
  push: ['router', 'push'],
})
@connectQuery({
  query: 'viewPhotos.gallery',
  name: 'gallery',
})
class AlbumList extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.checkAndSetAlbum();
  }

  componentDidUpdate() {
    this.checkAndSetAlbum();
  }

  checkAndSetAlbum() {
    const {
      openAlbum,
      gallery,
      setOpenAlbum,
      loadAlbum,
      section,
    } = this.props;

    if (loadAlbum && section === PHOTO_MODE_ALBUM) {
      if (loadAlbum !== openAlbum) {
        setOpenAlbum(Number(loadAlbum));
      }
    } else if (!gallery.loading && !openAlbum && section === PHOTO_MODE_ALBUM) {
      // push(path.join(PHOTOS, String(gallery.data.gallery[0].id)));
    }
  }

  render = () => {
    const {
      gallery,
      isPhoto,
    } = this.props;

    return (!isPhoto ? <div className={style.body}>
      <div className={style.album}>
        <Button className={style.sessions} asLink to={PHOTOS_SESSIONS}><Text type="h2">Sessions</Text></Button>
        {!gallery.loading && gallery.data.gallery.map((gallery, id) => <Album key={id} data={gallery} />)}
      </div>
    </div> : <div></div>);
  }
}

export default AlbumList;
