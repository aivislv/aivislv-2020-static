// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import { Goodreads } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { getAuthorImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { connectQuery, connectState } from '../../init/actions';
import { AUTHORS_VIEW } from '../../init/paths';
import { AUTHOR_BOOKS_WRAP_COUNT } from '../../views/authors/constants';
import Item from '../item/Item';

import style from './authorsArea.css';


type Props = {
  authors: Object,
  search: string,
  openAuthor: number,
  sizeCurrent: number,
  scrollPosition: number,
  isOpenAuthor: Object,
}

type State = {
}

@connectState({
  openAuthor: ['authors', 'openAuthor'],
  search: ['authors', 'search'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewAuthors.authors',
  name: 'authors',
})
class AuthorsArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
  };

  componentDidUpdate(prevProps) {
    const {
      authors,
      search,
      openAuthor,
      sizeCurrent,
      scrollPosition,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.search !== search) {
      force = true;
    }

    if ((!filteredData || force) && !authors.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE || prevProps.isOpenAuthor?.booksLinked > AUTHOR_BOOKS_WRAP_COUNT;

    if (!openAuthor || !skipStillUpdate) {
      if ((!prevProps.openAuthor || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      authors,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !authors.loading) {
      this.filterData();
    }
  }

  applySortProps() {
    return { key: 'name', direction: 'ASC' };
  }

  filterData = () => {
    const {
      authors,
      search,
    } = this.props;

    let filteredData = authors.data.authors;

    if (search && search.length > 2) {
      const useSearch = new RegExp(search, "gi");
      filteredData = filteredData.filter((item) => item.name.search(useSearch) > -1);
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
        filteredData = filteredData.sort((author1, author2) => {
          if (sortProps.direction === 'ASC') {
            return (author1[sortProps.key] < author2[sortProps.key]) ? -1 : (author1[sortProps.key] === author2[sortProps.key] ? 0 : 1);
          } else {
            return (author1[sortProps.key] < author2[sortProps.key]) ? 1 : (author1[sortProps.key] === author2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({ filteredData });
  };

  render = () => {
    const {
      authors,
      isOpenAuthor,
      openAuthor,
    } = this.props;

    const {
      filteredData
    } = this.state;

    let renderClass;

    if (openAuthor !== null && !isOpenAuthor) {
      renderClass = `${style.body} ${style.hasOpenAuthorBooks}`
    } else {
      renderClass = isOpenAuthor
        ? (
          isOpenAuthor.booksLinked >= AUTHOR_BOOKS_WRAP_COUNT
            ? `${style.body} ${style.hasOpenAuthorBooks}`
            : `${style.body} ${style.hasOpenAuthor}`
        ) : style.body;
    }

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.gr'))
    };

    return (<div className={renderClass}>
      {!authors.loading && filteredData && <Text type="h2" className={style.totalSelected}>{message('common.total').replace('#ITEMS#', filteredData.length)}</Text>}
      <div className={style.album}>
        {!authors.loading && filteredData && filteredData.map((author) => {
          author.title = author.name;

           return <Item
             itemData={author}
             itemLink={AUTHORS_VIEW}
             processImage={getAuthorImageLink}
             linkOutIcon={Goodreads}
             outLink={author.goodreadsId !== 0 && `${config.authorGoodreadsLink}${author.goodreadsId}`}
             messages={renderedMessages}
             key={author.id}
             service="goodreads"
             ratingMax={5}
            />
          }
        )}
      </div>
    </div>);
  }
}

export default AuthorsArea;
