// @flow

import React from 'react';

import { connectQuery, connectState } from '../../init/actions';
import { PHOTO_MODE_SESSION } from '../../views/photos/constants';
import Photo from '../photo/Photo';

import style from './photoSessionArea.css';
import {Loader} from "../loader/loader";


type Props = {
  session: Object,
  openSession: number,
}

@connectState({
  openSession: ['photos', 'openSession'],
})
@connectQuery({
  query: 'viewPhotos.session',
  name: 'session',
  vars: {
    sessionId: (props) => Number(props.openSession)
  }
})
class PhotoSessionArea extends React.PureComponent<Props> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      session,
      openSession,
    } = this.props;

    return (<div className={style.body}>
      <div className={style.album}>
          {!session.loading && session?.data?.session?.images.map((image, id) => (
              <Photo album={openSession} section={PHOTO_MODE_SESSION} listId={id} key={`${openSession}-${id}`}
                     data={image}/>))}
          {session.loading && <Loader/>}
      </div>
    </div>);
  }
}

export default PhotoSessionArea;
