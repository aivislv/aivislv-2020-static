// @flow

import React from 'react';

import { config } from '../../config';
import { SIZE_LARGE } from '../../elements/constants/size';
import Text from '../../elements/text/Text';
import { getAnimeImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { connectQuery, connectState } from '../../init/actions';
import { ANIME_VIEW } from '../../init/paths';
import * as Filters from '../../views/anime/constants';
import Item from '../item/Item';

import style from './animeArea.css';
import {Loader} from "../loader/loader";
import {ANIME_SORT_WISH_WEIGHT} from "../../views/anime/constants";


type Props = {
  anime: Object,
  openFilter: string,
  openSort: string,
  openType: string,
  search: string,
  openAnime: number,
  sizeCurrent: number,
  scrollPosition: number,
}

type State = {
}

@connectState({
  openSort: ['anime', 'openSort'],
  openType: ['anime', 'openType'],
  openAnime: ['anime', 'openAnime'],
  search: ['anime', 'search'],
  scrollPosition: ['layouts', 'scrollPosition'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectQuery({
  query: 'viewAnime.anime',
  name: 'anime',
})
class AnimeArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {
    filteredData: null,
    currentScrollStatus: 0,
  };

  componentDidUpdate(prevProps) {
    const {
      anime,
      openFilter,
      openSort,
      openType,
      search,
      openAnime,
      sizeCurrent,
      scrollPosition,
    } = this.props;

    const {
      filteredData,
      currentScrollStatus,
    } = this.state;

    let force = false;

    if (prevProps.openFilter !== openFilter || prevProps.openSort !== openSort || prevProps.openType !== openType || prevProps.search !== search) {
      force = true;
    }

    if ((!filteredData || force) && !anime.loading) {
      this.filterData();
    }

    const skipStillUpdate = sizeCurrent < SIZE_LARGE;

    if (!openAnime || !skipStillUpdate) {
      if ((!prevProps.openAnime || !skipStillUpdate) || currentScrollStatus === 0) {
        this.setState({ currentScrollStatus: scrollPosition });
      } else {
        if (skipStillUpdate) {
          window.scroll(0, currentScrollStatus);
        }
      }
    }
  }

  componentDidMount() {
    const {
      anime,
    } = this.props;

    const {
      filteredData,
    } = this.state;

    if ((!filteredData) && !anime.loading) {
      this.filterData();
    }
  }

  applyAnimeType() {
    const {
      openType,
    } = this.props;

    switch (openType) {
      case Filters.ANIME_TYPE_WISHLIST: return 6;
      case Filters.ANIME_TYPE_WATCHING: return 1;
      case Filters.ANIME_TYPE_WATCHED: return 2;
      default: return 2;
    }
  }

  applySortProps() {
    const {
      openSort,
    } = this.props;

    if (openSort && openSort !== Filters.ANIME_SORT_NO) {
      switch (openSort) {
        case Filters.ANIME_SORT_ALPHABET:
          return { key: 'title', direction: 'ASC' };
        case Filters.ANIME_SORT_RATING:
          return { key: 'rating', direction: 'DESC' };
        case Filters.ANIME_SORT_SCORE:
          return { key: 'score', direction: 'DESC' };
        case Filters.ANIME_SORT_LAST_PLAY:
          return { key: 'updated', direction: 'DESC' };
        case Filters.ANIME_SORT_WISH_WEIGHT:
          return {key: 'wishWeight', direction: 'DESC'};
        default:
          return false;
      }
    }

    return false;
  }

  filterData = () => {
    const {
      anime,
      search,
    } = this.props;

    const gameType = this.applyAnimeType();

    let filteredData = anime.data.animes.filter((item) => item.status === gameType);

    if (search && search.length > 2) {
      const useSearch = new RegExp(search, "gi");
      filteredData = filteredData.filter((item) => item.title.search(useSearch) > -1);
    }

    const sortProps = this.applySortProps();

    if (sortProps) {
        filteredData = filteredData.sort((anime1, anime2) => {
          if (sortProps.direction === 'ASC') {
            return (anime1[sortProps.key] < anime2[sortProps.key]) ? -1 : (anime1[sortProps.key] === anime2[sortProps.key] ? 0 : 1);
          } else {
            return (anime1[sortProps.key] < anime2[sortProps.key]) ? 1 : (anime1[sortProps.key] === anime2[sortProps.key] ? 0 : -1);
          }
        })
    }

    this.setState({ filteredData });
  };

  render = () => {
    const {
      anime,
      openAnime,
    } = this.props;

    const {
      filteredData
    } = this.state;

    const renderedMessages = {
      openPage: message('common.open').replace('#SOURCE#', message('specs.mal')),
      rating: message('common.rating'),
    }

    return (<div className={openAnime ? `${style.body} ${style.hasOpenAnime}` : style.body}>
      {!anime.loading && filteredData && <Text type="h2" className={style.totalSelected}>{message('common.total').replace('#ITEMS#', filteredData.length)}</Text>}
      <div className={style.album}>
        {!anime.loading && filteredData && filteredData.map((anime) => (
          <Item
              itemData={anime}
              itemLink={ANIME_VIEW}
              processImage={getAnimeImageLink}
              linkOutFile="/malLogo.png"
              outLink={`${config.animeMalLink}${anime.malId}`}
              messages={renderedMessages}
              key={anime.id}
              service="mal"
          />
        ))}
        {anime.loading && <Loader/>}
      </div>
    </div>);
  }
}

export default AnimeArea;
