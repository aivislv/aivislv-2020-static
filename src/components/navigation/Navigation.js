// @flow

import React from 'react';

import { config } from '../../config';
import Button from '../../elements/button/Button';
import Icon, { Menu } from '../../elements/icon';
import Text from '../../elements/text/Text';
import { message } from '../../helpers/Translate';
import { ABOUT, ANIME, BOOKS, GAMES, MOVIES, PHOTOS, ROOT } from '../../init/paths';

import style from './navigation.css';


const buttons = [
  // { to: STREAM, text: message('components.navigation.texts.goTo.stream') },
  { to: PHOTOS, text: message('components.navigation.texts.goTo.photos') },
  { to: GAMES, text: message('components.navigation.texts.goTo.games') },
  { to: ANIME, text: message('components.navigation.texts.goTo.anime') },
  { to: BOOKS, text: message('components.navigation.texts.goTo.books') },
  { to: MOVIES, text: message('components.navigation.texts.goTo.movies') },
  { to: ABOUT, text: message('components.navigation.texts.goTo.about') },
];

type Props = {
  part?: string,
}

type State = {
}

class Navigation extends React.PureComponent<Props, State> {
  static defaultProps = {
    part: 'stream',
  };
  state = {
    dropDown: false,
  };

  render = () => {
    const {
      part
    } = this.props;

    const {dropDown} = this.state;

    const displayMenu = dropDown ? style.menuDisplay : '';
    const displayToogle = !dropDown ? style.toogleDisplay : '';

    return (<>
          <div className={style.body}>
            <div className={style.left}>
              <div className={`${style.dropIcon} ${displayToogle}`}
                   onClick={() => this.setState({dropDown: !dropDown})}><Icon className={style.dropElement}
                                                                              theme={!dropDown ? 'white' : 'gray'}><Menu/></Icon>
              </div>
              <div className={`${style.menu} ${displayMenu}`} onClick={() => this.setState({dropDown: !dropDown})}>
                {buttons.map((element, key) => {
                  return <Button key={key} className={part === element.to ? style.selctedButton : style.button}
                                 theme={part === element.to ? 'only-white' : 'black'} asLink to={element.to}><Text
                      type="h2">{element.text}</Text></Button>
                })}
              </div>
            </div>
            <div className={style.logo}>
              <Button asLink to={ROOT} className={style.logoButton} theme="only-black"><img className={style.logoImage}
                                                                                            src={`${config.staticFiles}images/logo.png`}/></Button>
            </div>
          </div>
          <div className={style.padding}/>
        </>
    );
  }
}

export default Navigation;
