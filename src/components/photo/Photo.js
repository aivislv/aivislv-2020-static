// @flow

import React from 'react';

import Text from '../../elements/text/Text';
import { getImageLink, getImageSlug } from '../../helpers/ProcessImages';
import { connectActions } from '../../init/actions';
import { PHOTO_MODE_ALBUM } from '../../views/photos/constants';

import style from './photo.css';


type Props = {
  data: Object,
  album: number,
  listId: number,
  section: string,
  setOpenImageId: number,
  push: Function,
}

@connectActions({
  push: ['router', 'push'],
  setOpenImageId: ['photos', 'setOpenImageId'],
})
class Photo extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  openImage = () => {
    const {
      setOpenImageId,
      album,
      push,
      listId,
      data,
      section,
    } = this.props;

    const slug = getImageSlug(data);

    setOpenImageId(listId);
    if (section === PHOTO_MODE_ALBUM) {
      push(`/photos/${album}/${data.id}/${slug}`);
    } else {
      push(`/sessions/${album}/${data.id}/${slug}`);
    }
  };

  render = () => {
    const {
      data,

    } = this.props;

    return (<div className={style.thumb}>
      <div onClick={this.openImage} className={style.thumbContainer}>
        <img loading="lazy" width={500} height={500}  className={style.image} src={getImageLink(data, 500)} />
        <Text type="normal" className={style.title}>{data.title}</Text>
      </div>
    </div>);
  }
}

export default Photo;
