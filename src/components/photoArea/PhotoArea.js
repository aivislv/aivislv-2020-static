// @flow

import React from 'react';

import { connectQuery, connectState } from '../../init/actions';
import { PHOTO_MODE_ALBUM } from '../../views/photos/constants';
import Photo from '../photo/Photo';

import style from './photoArea.css';
import {Loader} from "../loader/loader";


type Props = {
  album: Object,
  openAlbum: number,
}

type State = {
}

@connectState({
  openAlbum: ['photos', 'openAlbum'],
})
@connectQuery({
  query: 'viewPhotos.album',
  name: 'album',
  vars: {
    albumId: (props) => props.openAlbum
  }
})
class PhotoArea extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      album,
      openAlbum,
    } = this.props;

    return (<div className={style.body}>
      <div className={style.album}>
        {!album.loading && album?.data?.album?.images.map((image, id) => (
            <Photo section={PHOTO_MODE_ALBUM} album={openAlbum} listId={id} key={`${openAlbum}-${id}`} data={image}/>))}
        {album.loading && <Loader/>}
      </div>
    </div>);
  }
}

export default PhotoArea;
