import style from "./loader.css";
import Text from "../../elements/text/Text";
import {message} from "../../helpers/Translate";
import React from "react";

export const Loader = () => (<div className={style.loading}>
    <div className={style.loader}/>
    <Text type="h2">{message('components.gameArea.titles.loading')}</Text>
</div>)
