// @flow

import React from 'react';

import path from 'path';


import Button from '../../elements/button/Button';
import Input from '../../elements/input/Input';
import Text from '../../elements/text/Text';
import { connectActions } from '../../init/actions';

import style from './subNavigation.css';


type Props = {
  buttons: Array < Object >,
  selectedPath? : string,
  search: string,
  push: Function,
  hide? : string,
  messages: Object,
  searchPath? : Function,
}

type State = {
  subMenu: boolean,
}

@connectActions({
  push: ['router', 'push'],
})
class SubNavigation extends React.PureComponent<Props, State> {
  static defaultProps = {
    hide: false,
  };
  state = {
    subMenu: false,
  };

  openSubMenu = (state = null) => {
    const {
      subMenu,
    } = this.state;

    this.setState({ subMenu: state !== null ? state : !subMenu });
  };

  render = () => {
    const {
      push,
      buttons,
      selectedPath,
      messages,
      searchPath,
      hide,
    } = this.props;

    const {
      subMenu
    } = this.state;

    const navClass = `${style.body} ${(subMenu ? style.subMenu : '')} ${(hide ? style[hide] : '')}`;

    let defaultButton = 0;

    buttons.forEach((item, index) => {
      if (item.to === selectedPath) {
        defaultButton = index
      }
    })

    return (<div className={navClass}>
      <div className={style.left}>
        <div className={style.menu}>
          {buttons.map((button, key) => (
              <Button key={key}
                      theme={key === defaultButton ? (button.primary ? 'only-light-gray' : 'only-white') : (button.primary ? 'dark-gray' : 'gray')}
                      className={key === defaultButton ? `${style.button} ${style.buttonSelected}` : style.button}
                      asLink={key !== defaultButton}
                      onClick={() => this.openSubMenu()}
                      to={path.join(button.to)}
              >
                <Text type="h2">{button.text}</Text>
              </Button>
          ))}
          <Input placeholder={messages.search} className={style.search} name="search" delay={0.3}
                 onChange={(element) => push(searchPath ? searchPath(element) : path.join(selectedPath, element))}/>
        </div>
      </div>
    </div>);
  }
}

export default SubNavigation;
