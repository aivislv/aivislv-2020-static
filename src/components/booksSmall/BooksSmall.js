import React from 'react';

import { faBook, faBookReader } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import path from 'path';

import { config } from '../../config';
import Icon, { Goodreads } from '../../elements/icon';
import Link from '../../elements/link/Link';
import Text from '../../elements/text/Text';
import { getBookImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { BOOKS_VIEW } from '../../init/paths';
import Image from '../image/Image';

import style from './booksSmall.css';


export default ({ book, isFullscreen, inSeries = null }) => {
  const goTo = path.join(BOOKS_VIEW, String(book.id));

  const readStatus = [
    message('components.booksSmall.titles.wishlist'),
    message('components.booksSmall.titles.reading'),
    message('components.booksSmall.titles.complete'),
  ];

  let inSeriesString = null;

  if (inSeries !== null) {
    if (inSeries.ordered && inSeries.ordered !== 99999) {
      inSeriesString = inSeries.ordered;

      if (inSeries.lastOrdered) {
        inSeriesString = `${inSeriesString} - ${inSeries.lastOrdered}`;
      }
    }
  }

  return (<div key={book.id} className={`${style.book} ${!isFullscreen ? style.notFullscreen : ''}`} title={book.title}>
    <div className={style.body}>
      <Link to={goTo}>
        <Image className={style.image} src={getBookImageLink(book.id)} />
        <div className={style[`isRead-${book.isRead}`]} title={readStatus[book.isRead]}><FontAwesomeIcon icon={faBookReader} /></div>
        {book.isOwned === 1 && <div className={style.isOwned} title={message('components.booksSmall.titles.openBook')}><FontAwesomeIcon icon={faBook} /></div>}
        {inSeries !== null && inSeriesString !== null && <div className={style.inSeries} title={message('components.booksSmall.titles.numberInSeries')}>{inSeriesString}</div>}
      </Link>
      {book.goodreadsId !== 0 && <a className={style.bggLink} target="_blank"
                                    title={message('common.open').replace('#SOURCE#', message('specs.gr'))}
                                    href={`${config.bookGoodreadsLink}${book.goodreadsId}/`}
                                    rel="noreferrer"><Icon><Goodreads/></Icon></a>}
    </div>
    <Link to={goTo}>
      <Text type="h2" className={style.title}>{book.title}</Text>
    </Link>
  </div>)
};
