import React from 'react';

import path from "path";

import { config } from '../../config';
import Icon, { Goodreads } from '../../elements/icon';
import Link from '../../elements/link/Link';
import Text from '../../elements/text/Text';
import { getBookImageLink } from '../../helpers/ProcessImages';
import { message } from '../../helpers/Translate';
import { SERIES_VIEW } from '../../init/paths';
import Image from '../image/Image';

import style from './seriesSmall.css';


export default ({ series }) => {
  const goTo = path.join(SERIES_VIEW, String(series.id));

  return (<div key={series.id} className={style.series} title={series.title}>
    <div className={style.body}>
      <Link to={goTo}>
        <Image className={style.image} src={getBookImageLink(series.firstBookId)} />
      </Link>
      <a className={style.bggLink} target="_blank" title={message('common.open').replace('#SOURCE#', message('specs.gr'))}
        href={`${config.seriesGoodreadsLink}/${series.goodreadsId}/`} rel="noreferrer"><Icon><Goodreads/></Icon></a>
    </div>
    <Link to={goTo}>
      <Text type="h2" className={style.title}>{series.title}</Text>
    </Link>
  </div>)
};
