// @flow

import React from 'react';

import style from './init.css';


type Props = {
  children: React.Node,
}

class Init extends React.PureComponent<Props> {
  render = () => {
    const {
      children
    } = this.props;

    return <div className={style.body}>
      {children}
    </div>;
  }
}

export default Init;
