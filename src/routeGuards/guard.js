import { appliedRules } from "../init/pathRules";


export const validate = (path, url) => {
    const rules = appliedRules();
    if (rules[path]) {
        return rules[path].reduce(
            (value, run) => {
                if (value === true) {
                    return run(url);
                }

                return value;
            },
            true
        )
    }

    return true;
}
