export const hexdec = function (hexString) {
  hexString = (hexString).replace(/[^a-f0-9]/gi, '');
  return parseInt(hexString, 16);
};

export const dechex = function (number) {
  if (number < 0) {
    number = 0xFFFFFFFF + number + 1;
  }
  return parseInt(number, 10)
    .toString(16);
};

export const calculateRed = (rating, max = 10) => {
  let r = hexdec('ff');
  let g = hexdec('0');

  const change = ((r / (max * 10)) * (rating * 10));

  r -= change;
  g = change;

  r = dechex(r);
  g = dechex(g);

  if (String(r).length < 2) {
    r = `0${r}`;
  }

  if (String(r).length < 2) {
    g = `0${g}`;
  }

  return `#${r}${g}00`;
};
