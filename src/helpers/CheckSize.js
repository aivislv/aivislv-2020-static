export const isSize = (size) => {
  const check = (window.document.compatMode === "CSS1Compat") ?
    document.documentElement :
    document.body;

  const currentSize = Math.max(check.clientWidth, window.innerWidth || 0);

  return currentSize >= size;
};


export const currentSize = () => {
  const check = (window.document.compatMode === "CSS1Compat") ?
    document.documentElement :
    document.body;

  return Math.max(check.clientWidth, window.innerWidth || 0);
};
