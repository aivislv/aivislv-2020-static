const translate = {
  en: {
    common: {
      close: 'Close',
      total: 'Total: #ITEMS#',
      open: 'Open in #SOURCE#',
      rating: 'My rating',
      search: 'Search...',
      back: 'Back',
    },
    specs: {
      bgg: 'BoardGameGeek',
      imdb: 'IMDB',
      mal: 'MyAnimeList',
      gr: 'Goodreads',
    },
    components: {
      footer: {
        texts: {
          copyright: '© Aivis Lisovskis',
        },
      },
      navigation: {
        texts: {
          goTo: {
            stream: 'Stream',
            photos: 'Photos',
            games: 'Tabletop',
            anime: 'Anime',
            movies: 'Movies',
            books: 'Books',
            about: 'About',
          }
        },
      },
      gameArea: {
        titles: {
          playCount: 'Play count',
          lastPlay: 'Last played: #DATE#',
          preordered: 'Preordered',
          loading: 'Loading data...',
          wishlistPriority: ['No', 'Must have', 'Love to have', 'Like to have', 'Thinking about it', 'Dont buy this'],
        }
      },
      gameView: {
        texts: {
          playCount: 'Total plays',
          lastPlay: 'Last play',
          preordered: 'Preordered',
          playerCount: 'Player count',
          playLength: 'Play length',
          rating: 'My rating',
          wishlistPriority: ['No', 'Must have', 'Love to have', 'Like to have', 'Thinking about it', 'Dont buy this'],

        },
      },
      animeView: {
        texts: {
          rating: 'My rating',
        },
      },
      animeNavigation: {
        texts: {
          goTo: {
            watched: 'A - Z',
            wishlist: 'Wishlist',
            rating: 'Rating',
            watching: 'Watching',
            updated: 'Freshest',
          }
        }
      },
      booksNavigation: {
        texts: {
          goTo: {
            owned: 'Owned',
            wishlist: 'Wishlist',
            hadRead: 'Had Read',
            reading: 'Reading',
            all: 'All',
            authors: 'Authors',
            books: 'Books',
            series: 'Series',
          }
        }
      },
      booksArea: {
        titles: {
          owned: 'Is in my library',
          wishlist: 'Planning to read',
          complete: 'Is read',
          reading: 'Reading now',
        },
      },
      booksSmall: {
        titles: {
          wishlist: 'Planning to read',
          complete: 'Is read',
          reading: 'Reading now',
          openBook: 'Open book info',
          numberInSeries: 'Number In This Series',
        },
      },
      bookView: {
        texts: {
          isOwnedY: 'Is owned: Yes',
          isOwnedN: 'Is owned: No',
          readStatus: 'Read status:',
          series: 'Series',
          author: 'Author',
          authors: 'Authors',
        },
        titles: {
          owned: 'I own physical copy of this book',
          wishlist: 'Planning to read',
          complete: 'Is read',
          reading: 'Reading now',
        }
      },
      authorView: {
        texts: {
          books: 'Books',
          series: 'Series',
        },
      },
      seriesView: {
        texts: {
          books: 'Books',
          authors: 'Authors',
        },
      },
      moviesView: {
        texts: {
          rating: 'My rating',
        },
      },
      moviesNavigation: {
        texts: {
          goTo: {
            watched: 'A - Z',
            wishlist: 'Wishlist',
            rating: 'Rating',
            watching: 'Watching',
            updated: 'Freshest',
          },
        }
      },
      gamesNavigation: {
        texts: {
          goTo: {
              all: 'A - Z',
              playcount: 'Play Count',
              wishlist: 'Wishlist',
              unplayed: 'Unplayed',
              rating: 'Rating',
              lastPlay: 'Last Played',
              tags: 'Tags',
              played: 'Played',
          }
        }
      }
    }
  },
};

export default translate;
