import translate from './translation.js';


const locale = "en";

export const message = (path) => {
  const pathParts = path.split(".");

  if (translate[locale]) {
    return pathParts.reduce((accumulator, value) => {
      if (accumulator && accumulator[value]) {
        return accumulator[value];
      }
      return '!#NO TEXT#!';
    }, translate[locale]);
  }

  return false;
};

export const html = () => {

}
