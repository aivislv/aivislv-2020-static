import cookies from 'browser-cookies';


export function set(items, store = 'cookies') {
  if (store === 'cookies') {
    Object.keys(items).forEach((item) => {
      cookies.set(item, JSON.stringify(items[item]), {expires: 365})
    });
  }

  if (store === 'localStorage') {
    Object.keys(items).forEach((item) => {
      window.localStorage.setItem(item, JSON.stringify(items[item]))
    });
  }

  if (store === 'sessionStorage') {
    Object.keys(items).forEach((item) => {
      window.sessionStorage.setItem(item, JSON.stringify(items[item]))
    });
  }
}

export function remove(items, store = 'cookies') {
  if (store === 'cookies') {
    items.forEach((item) => {
      cookies.erase(item);
    });
  }

  if (store === 'localStorage') {
    items.forEach((item) => {
      window.localStorage.removeItem(item)
    });
  }

  if (store === 'sessionStorage') {
    items.forEach((item) => {
      window.sessionStorage.removeItem(item)
    });
  }
}

export async function revive(store) {
  const reStore = {
    'cookies': {},
    'localStorage': {},
    'sessionStorage': {},
  };

  if (store['cookies']) {
    Object.keys(store['cookies']).forEach((item) => {
      const currentItem =  JSON.parse(cookies.get(item));

      if (currentItem !== null) {
        reStore.cookies[item] = currentItem;
      }
    });
  }

  if (store['localStorage']) {
    Object.keys(store['localStorage']).forEach((item) => {
      const currentItem = JSON.parse(window.localStorage.getItem(item));

      if (currentItem !== null) {
        reStore.localStorage[item] = currentItem;
      }
    });
  }

  if (store['sessionStorage']) {
    Object.keys(store['sessionStorage']).forEach((item) => {
      const currentItem = JSON.parse(window.sessionStorage.getItem(item));

      if (currentItem !== null) {
        reStore.sessionStorage[item] = currentItem;
      }
    });
  }

  return reStore;
}
