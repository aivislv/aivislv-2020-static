import path from "path";

export const createGamePath = (basePath, props) => {
    return path.join(basePath, JSON.stringify(props))
}

export const createBookPath = (basePath, props = []) => {
    return path.join(basePath, JSON.stringify(props))
}