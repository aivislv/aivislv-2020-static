import { config } from '../config';

import { slugMachine } from './slugSpecialCharReplace';


export const getImageSlug = (image) => {
  if (image.title) {
    return slugMachine(image.title);
  }

  return '';
};

export const getImageLink = (image, size = 500) => {
  const allowedSizes = [2560, 1920, 1280, 1024];
  let useSize;

  if (size === 500 || size === 250) {
    useSize = size;
  } else {
    useSize = allowedSizes.reduce((curentSize, allowedSize) => (size <= allowedSize ? allowedSize : curentSize), allowedSizes[0]);
  }

  return `${config.images}${useSize}/${image.id}.${image.extension}`
};

export const getGameImageLink = (image) => {
  return `${config.gameImages}${image}.jpg`
};

export const getAnimeImageLink = (image) => {
  return `${config.animeImages}${image}.jpg`
};

export const getMovieImageLink = (image) => {
  return `${config.movieImages}${image}.jpg`
};

export const getBookImageLink = (image) => {
  return `${config.bookImages}${image}.jpg`
};

export const getAuthorImageLink = (image) => {
  return `${config.authorImages}${image}.jpg`
};
