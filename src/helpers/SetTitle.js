import { config } from '../config';


const setTitle = (title) => {
  window.document.title = `${config.titlePrefix}${title}`;
};

export default setTitle;