export const SIZE_SMALL = 321;
export const SIZE_MEDIUM_R = 420;
export const SIZE_MEDIUM = 421;
export const SIZE_LARGE_R = 1134;
export const SIZE_LARGE = 1135;
export const SIZE_X_LARGE_R = 1200;
export const SIZE_X_LARGE = 1201;

export const SIZE_SPECIAL_ALBUM_WRAP = 1134;