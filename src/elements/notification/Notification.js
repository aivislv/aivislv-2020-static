// @flow

import React from 'react';

import style from './notification.css';


type Props = {
  customClass?: string,
  theme?: string,
  onClick?: boolean | Function,
  passThrough?: Object,
  children: React.Node,
}

class Notification extends React.PureComponent<Props> {
  static defaultProps = {
    customClass: '',
    theme: 'gray',
    onClick: false,
    passThrough: {},
  };

  onClick = () => {
    const {
      onClick,
      passThrough,
    } = this.props;

    onClick(passThrough);
  };

  render() {
    const {
      customClass,
      theme,
      children,
    } = this.props;

    return <div onClick={this.onClick} className={`${style[theme]} ${customClass}`}>{children}</div>
  }
}

// tests

export default Notification;
