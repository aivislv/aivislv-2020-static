import React from 'react';

import {Redirect, Route} from 'react-router-dom';

import { validate } from "../../routeGuards/guard";


class RouteAdv extends Route {
    validate = (matchProps) => {
       const {
           component,
           render,
       } = this.props;

       const Component = component;

       const result = validate(matchProps.match.path, matchProps.match.url);

       return (result === true
               ? (component ? <Component {...this.props} /> : render(matchProps))
               : <Redirect to={result} />
           );
    }

    render() {
        return <Route {...this.props} render={this.validate}/>
    }
}

export default RouteAdv;
