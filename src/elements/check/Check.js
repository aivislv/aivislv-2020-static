// @flow

import React from 'react';

import { CheckBox, Unselected } from "../icon";
import { Icon, Text } from '../index';

import style from './check.css';


type Props = {
  name: string,
  customClass?: string,
  title?: string | boolean,
  checked?: boolean,
  onChange?: Function,
  passThrough?: Object,
  order?: string,
}

type State = {
  checked: boolean,
}

class Check extends React.PureComponent<Props, State> {
  static defaultProps = {
    customClass: '',
    title: false,
    checked: false,
    onChange: () => {},
    passThrough: {},
    order: 'left',
  };

  constructor(props) {
    super(props);
    this.state = {checked: props.checked}
  }

  onClick = () => {
    const {
      onChange,
      passThrough
    } = this.props;

    const {
      checked
    } = this.state;

    const newState = !checked;
    this.setState({checked: newState});
    onChange(newState, passThrough)
  };

  render() {
    const {
      customClass,
      name,
      title,
      order,
    } = this.props;

    const {
      checked
    } = this.state;

    const checkStatus = `check-status-${checked}`;
    const checkOrder = `check-order-${order}`;

    return <div onClick={this.onClick} className={`${style.check} ${style[checkOrder]} ${style[checkStatus]} ${customClass}`}>
      {title && <label htmlFor={name} className={style.label}><Text>{title}</Text></label>}
      <Icon customClass={style.unchecked}><Unselected /></Icon><Icon customClass={style.checked}><CheckBox /></Icon>
      <input onChange={() => {}} type="checkbox" checked={checked} name={name} className={style.checkBox} />
    </div>
  }
}

export default Check;
