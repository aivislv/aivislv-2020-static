// @flow

import React from 'react';

import style from './person.css';


type Props = {
  customClass?: string,
  image?: string | boolean,
  name?: string | boolean,
  onClick?: Function,
}

class Person extends React.PureComponent<Props, State> {
  static defaultProps = {
    customClass: '',
    image: false,
    name: false,
    onClick: () => {},
  };

  renderImage = () => {
    const {
      image
    } = this.props;

    return <img className={style.image} src={image} />;
  };

  renderName = () => {
    const {
      name
    } = this.props;

    let char = "";

    if (name) {
      char = name.charAt(0);
    }
    return <div className={style.name}>{char}</div>;
  };

  render() {
    const {
      customClass,
      image,
      onClick,
    } = this.props;

    return <div onClick={onClick} className={`${style.icon} && ${customClass}`}>
      {image ? this.renderImage() : this.renderName()}
    </div>
  }
}

export default Person;
