import Icon from './Icon';


export Cart from '../../assets/icons/cart.svg';
export Checked from '../../assets/icons/check_icon.svg';
export Check from '../../assets/icons/check.svg';
export CheckBox from '../../assets/icons/checkbox_icon.svg';
export Cross from '../../assets/icons/cross_icon.svg';
export Down from '../../assets/icons/down_icon.svg';
export Dropdown from '../../assets/icons/dropdown_icon.svg';
export Fb from '../../assets/icons/fb_icon.svg';
export FbRound from '../../assets/icons/fb_round_icon.svg';
export Left from '../../assets/icons/left_icon.svg';
export Menu from '../../assets/icons/menu_icon.svg';
export Minus from '../../assets/icons/minus_icon.svg';
export Next from '../../assets/icons/nextstep_icon.svg';
export Pin from '../../assets/icons/pin_icon.svg';
export Pluss from '../../assets/icons/pluss_icon.svg';
export Previous from '../../assets/icons/prewstep_icon.svg';
export Right from '../../assets/icons/right_icon.svg';
export Search from '../../assets/icons/search_icon.svg';
export Send from '../../assets/icons/send_icon.svg';
export SendWslt from '../../assets/icons/sendnwslt_icon.svg';
export Twitter from '../../assets/icons/twitter_icon.svg';
export Up from '../../assets/icons/up_icon.svg';
export User from '../../assets/icons/user_icon.svg';
export Unselected from '../../assets/icons/empty_round.svg';
export OptionBox from '../../assets/icons/option_box.svg';

export Bgg from '../../assets/icons/bggLogo.svg';
export Goodreads from '../../assets/icons/goodreads.svg';

export default Icon;
