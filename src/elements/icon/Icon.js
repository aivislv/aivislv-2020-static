// @flow

import React from 'react';

import style from './icon.css';


type Props = {
  className?: string,
  onClick?: Function,
  theme?: string,
  passThrough?: Object,
  children: React.Node,
}

class Icon extends React.PureComponent<Props> {
  static defaultProps = {
    className: '',
    onClick: () => {},
    theme: 'black',
    passThrough: {},
  };

  onClick = () => {
    const {
      onClick,
      passThrough,
    } = this.props;

    onClick(passThrough);
  };

  render() {
    const {
      className,
      theme,
      children,
    } = this.props;

    return <div onClick={this.onClick} className={`${style[theme]} ${className}`}>{children}</div>
  }
}

export default Icon;
