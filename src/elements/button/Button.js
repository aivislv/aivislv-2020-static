// @flow

import React from 'react';

import { Link } from 'react-router-dom';

import styles from './button.css';


type Props = {
  className? : string,
  onClick? : Function,
  theme? : string,
  passThrough? : Object,
  asLink? : boolean,
  to? : string,
  children: React.Node,
  style? : Object,
  disabled? : false,
}

class Button extends React.PureComponent<Props> {
  static defaultProps = {
    className: '',
    onClick: () => {
    },
    theme: 'white',
    passThrough: {},
    asLink: false,
    style: {},
    to: '/',
  };

  onClick = () => {
    const {
      onClick,
      passThrough,
    } = this.props;

    onClick(passThrough);
  };

  render() {
    const {
      className,
      theme,
      children,
      asLink,
      to,
      style,
      disabled,
    } = this.props;

    return asLink ?
      <Link to={to} onClick={this.onClick} style={style} className={`${styles[theme]} ${className}`}>{children}</Link> :
      <button disabled={disabled} style={style} onClick={this.onClick}
              className={`${styles[theme]} ${className}`}>{children}</button>;
  }
}

// tests

export default Button;
