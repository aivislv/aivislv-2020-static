// @flow

import React from 'react';

import style from './input.css';


type Props = {
  name: string,
  className?: string,
  placeholder?: string | boolean,
  onChange?: Function,
  passThrough?: Object,
  value?: string,
  delay?: number | boolean,
  waitForUpdate?: boolean,
  type?: string,
  status?: string,
  validator?: Object | boolean,
  autoValidate?: boolean,
  validateWith?: Function | boolean,
  maxlength?: number,
  required?: boolean,
}

type State = {
  value: string,
  waiting: boolean,
  autoTrigger: boolean | number,
}

class Input extends React.PureComponent<Props, State> {
  static defaultProps = {
    className: '',
    placeholder: '',
    onChange: () => {},
    passThrough: {},
    value: '',
    delay: false,
    waitForUpdate: false,
    type: 'text', // text, password, email...
    status: 'normal', // error, success, normal
    validator: false,
    validateWith: false,
    autoValidate: false,
    maxlength: 524288,
    required: false,
  };

  constructor(props) {
    super(props);

    this.myInput = React.createRef();

    this.state = {
      value: props.value,
      waiting: false,
      autoTrigger: false,
      innerStatus: false,
    }
  }

  validate = async () => {
    const {
      validator,
      validateWith
    } = this.props;

    const {
      value
    } = this.state;

    let result = true;

    if (validateWith) {
      result = await validateWith(value);
    } else {
      if (validator) {
        result = validator.element(this.myRef.current);
      }
    }

    if (!result) {
      this.setState({innerStatus: 'error'})
    } else {
      this.setState({innerStatus: false})
    }

    return result;
  };

  pushUpdate = async (newValue, pass = false) => {
    const {
      onChange,
      passThrough,
      waitForUpdate,
      delay,
    } = this.props;

    const {
      autoTrigger,
    } = this.state;

    if (delay === 0 || pass) {
      if (waitForUpdate) {
        this.setState({ waiting: true });
      }
      const acceptUpdate = await onChange(newValue, passThrough);

      if (typeof(acceptUpdate) === 'string') {
        this.setState({ value: acceptUpdate });
      }

      if (waitForUpdate) {
        this.setState({ waiting: false });
      }
    } else {
      if (autoTrigger) {
        clearTimeout(autoTrigger);
      }
      this.setState({ autoTrigger: setTimeout(() => this.pushUpdate(newValue, true), delay * 1000) });
    }
  }

  onEscape = async (e) => {
    if (e.keyCode === 27) {
      this.setState({ value: '' });
      this.pushUpdate('');
    }
  };

  onChange = async (e) => {
    const {
      autoValidate,
    } = this.props;
    const {
      waiting,
    } = this.state;

    if (!waiting) {
        let proceed = true;

        if (autoValidate) {
          proceed = this.validate();
        }

        if (proceed) {
          this.setState({ value: e.target.value });
          this.pushUpdate(e.target.value);
        }
    }
  };

  render() {
    const {
      className,
      name,
      type,
      status,
      placeholder,
      maxlength,
      required
    } = this.props;

    const {
      value,
      innerStatus,
    } = this.state;

    let inputStatus = `input-state-${status}`;

    if (innerStatus) {
      inputStatus = `input-state-${innerStatus}`
    }

    return <div onClick={this.onClick} className={`${style[inputStatus]} ${className}`}>
      <input maxLength={maxlength} required={required} ref={this.myInput} onKeyUp={this.onEscape}
             onChange={this.onChange} value={value} type={type} name={name} placeholder={placeholder}
             className={style.inputElement}/>
    </div>
  }
}

export default Input;
