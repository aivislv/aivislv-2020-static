// @flow

import React from 'react';

import { Link as RouterLink } from 'react-router-dom';

import style from './link.css';


type Props = {
  className?: string,
  to: string,
  theme?: string,
  onClick?: Function,
  children: React.Node,
}

class Link extends React.PureComponent<Props> {
  static defaultProps = {
    className: '',
    theme: 'gray-black',
    to: false,
    onClick: () => {},
  };

  onClick = () => {
    const {
      onClick,
    } = this.props;

    onClick();
  };

  render() {
    const {
      className,
      theme,
      to,
      children,
    } = this.props;

    return <RouterLink onClick={this.onClick} className={`${style[theme]} ${className}`} to={to}>{children}</RouterLink>
  }
}

// tests

export default Link;
