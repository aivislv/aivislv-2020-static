// @flow

import React from 'react';

import style from './image.css';


type Props = {
  customClass?: string,
  src: string,
  size?: string,
}

class Image extends React.PureComponent<Props> {
  static defaultProps = {
    customClass: '',
    size: 'thumb-large'
  };

  render() {
    const {
      customClass,
      size,
      src
    } = this.props;

    return <img src={src} className={`${style[size]} ${customClass}`} />
  }
}

export default Image;
