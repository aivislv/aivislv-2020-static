// @flow

import React from 'react';

import style from './container.css';


type Props = {
  customClass?: string,
  theme?: string,
  children: React.Node,
}

class Container extends React.PureComponent<Props> {
  static defaultProps = {
    customClass: '',
    theme: 'white',
  };

  render() {
    const {
      customClass,
      theme,
      children,
    } = this.props;

    return <div className={`${style[theme]} ${customClass}`}>{children}</div>
  }
}

export default Container;
