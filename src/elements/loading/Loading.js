// @flow

import React from 'react';

import { Container, Notification } from '../index';


type Props = {
  passThrough?: Object,
  customClass?: string,
  onError?: Function,
  onLoading?: Function,
  onLoadingDone?: Function,
  onSuccess?: Function,
  onFinished?: Function,
  onClick?: Function,
  isError?: boolean | string,
  isLoading?: boolean,
  className: string,
  children: React.Node,
}

class Loading extends React.PureComponent<Props> {
  static defaultProps = {
    customClass: '',
    onError: () => {},
    onLoading: () => {},
    onLoadingDone: () => {},
    onFinished: () => {},
    onSuccess: () => {},
    onClick: () => {},
    passThrough: {},
    isError: false,
    isLoading: false,
  };

  error = () => {
    const {
      className,
      children,
      isError,
    } = this.props;

    return <Container className={className}>
      <Notification theme="error" className="isError">{isError}</Notification>
      <>{children}</>
    </Container>
  };

  loading = () => {
    const {
      className,
      children,
    } = this.props;

    return <Container className={className}>
      <Notification theme="gray" className="isLoading">Loading data... Please wait...</Notification>
      <>{children}</>
    </Container>
  };

  render() {
    const {
      isLoading,
      isError,
    } = this.props;

    if (isLoading) {
      return this.loading();
    }

    if (isError) {
      return this.error('There was an error in request!');
    }

    return <></>;
  }
}

// tests

export default Loading;
