// @flow

import React from 'react';

import useStyle from './text.css';


type Props = {
  className?: string,
  type?: string,
  html?: boolean,
  value?: string,
  style?: Object,
  title?: string,
  asSpan?: boolean,
  children?: React.Node,
}

class Text extends React.PureComponent<Props> {
  static defaultProps = {
    type: 'normal',
    html: false,
    style: {},
    title: null,
    asSpan: false,
  };

  render() {
    const {
      type,
      className,
      children,
      html,
      value,
      style,
      title,
      asSpan,
    } = this.props;

    if (asSpan) {
      return html ?
        <span style={style} title={title} dangerouslySetInnerHTML={{__html: value}} className={`${useStyle[type]} ${className}`} />
        :
        <span style={style} title={title} className={`${useStyle[type]} ${className}`}>{children}</span>
    }

    return html ?
      <div style={style} title={title} dangerouslySetInnerHTML={{__html: value}} className={`${useStyle[type]} ${className}`} />
      :
      <div style={style} title={title} className={`${useStyle[type]} ${className}`}>{children}</div>
  }
}

// tests

export default Text;
