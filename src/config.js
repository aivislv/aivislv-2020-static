export const config = {
  api: global.window.setEnvironment === 'production' ? 'https://api.aivis.lv' : 'http://lapi.aivis.lv',
  images: 'https://cdn.aivis.lv/gallery/',
  gameImages: 'https://cdn.aivis.lv/gallery/games/',
  animeImages: 'https://cdn.aivis.lv/gallery/anime/',
  movieImages: 'https://cdn.aivis.lv/gallery/movies/',
  bookImages: 'https://cdn.aivis.lv/gallery/books/covers/',
  authorImages: 'https://cdn.aivis.lv/gallery/books/authors/',
  development: true,
  titlePrefix: 'aivis.lv - ',
  gameBggLink: 'https://www.boardgamegeek.com/boardgame/',
  animeMalLink: 'https://myanimelist.net/anime/',
  movieImdbLink: 'https://www.imdb.com/title/',
  bookGoodreadsLink: 'https://www.goodreads.com/book/show/',
  authorGoodreadsLink: 'https://www.goodreads.com/author/show/',
  seriesGoodreadsLink: 'https://www.goodreads.com/series/',
  staticFiles: global.window.setEnvironment === 'production' ? 'https://static.aivis.lv/' : 'http://lstatic.aivis.lv/',
  // google analytics ID
};
