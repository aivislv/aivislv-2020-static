import { connectRouter, routerMiddleware } from "connected-react-router";
import {createBrowserHistory} from "history";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
// import log from "redux-logger";
import thunk from "redux-thunk";

import reducers from "./Components.reducers";


//EXECUTIONS
export const history = createBrowserHistory();

const historyWrapper = (history) => combineReducers({
    router: connectRouter(history),
    ...reducers,
});

const dev = process.env.NODE_ENV !== "production";

const composeEnhancers =
    dev && typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;


const enhancer = composeEnhancers(
    applyMiddleware(
      thunk,
      // log,
      routerMiddleware(history)
    )
);

export const store = createStore(
    historyWrapper(history),
    enhancer,
);
