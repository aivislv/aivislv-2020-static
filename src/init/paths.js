// -------------- DYNAMIC PARTS
export const ROOT = '/';
export const ROOT_SUB = '/:sub';

export const GAMES = '/games';
export const GAMES_GAME = '/games/view';
export const GAMES_FILTERS = '/games/list/:props?';
export const GAMES_SHOW = '/games/list';
// export const GAMES_SELECT = '/games/tags';

export const GAMES_MODE_VIEW = 'view';
export const GAMES_MODE_LIST = 'list';
// export const GAMES_MODE_SELECT = 'tags';

export const GAMES_FULL = '/games/:mode?/:props?';

export const STREAM = '/stream';
export const STREAM_PAGE = '/stream/:page';

export const PHOTOS = '/photos';
export const PHOTOS_PHOTO_SLUG_WO_SLASH = '/photos/:album/:photo';
export const PHOTOS_PHOTO_SLUG_Q_QS = '/photos/:album/:photo';
export const PHOTOS_PHOTO_SLUG = '/photos/:album?/:photo?/:slug?';

export const PHOTOS_SESSIONS = '/sessions';
export const PHOTOS_SESSIONS_PHOTO_SLUG_WO_SLASH = '/sessions/:session/:photo';
export const PHOTOS_SESSIONS_PHOTO_SLUG_Q_QS = '/sessions/:session/:photo';
export const PHOTOS_SESSIONS_PHOTO_SLUG = '/sessions/:session?/:photo?/:slug?';

export const ANIME = '/anime';
export const ANIME_SELECTED = '/anime/view/:anime';
export const ANIME_FULL = '/anime/list/:type?/:sort?/:search?';
export const ANIME_VIEW = '/anime/view';
export const ANIME_LIST = '/anime/list';

export const MOVIES = '/movies';
export const MOVIES_SELECTED = '/movies/view/:movie';
export const MOVIES_FULL = '/movies/list/:type?/:sort?/:search?';
export const MOVIES_VIEW = '/movies/view';
export const MOVIES_LIST = '/movies/list';

export const MUSIC = '/music';

export const BOOKS = '/books';

export const BOOKS_MODE_VIEW = 'view';
export const BOOKS_MODE_LIST = 'list';
export const BOOKS_FULL = '/books/:mode?/:props?';
// export const BOOKS_FULL = '/books/:mode?/:type?/:search?';
export const BOOKS_VIEW = '/books/view';
export const BOOKS_LIST = '/books/list';

export const AUTHORS_MODE_VIEW = 'view';
export const AUTHORS_MODE_LIST = 'list';
export const BOOKS_AUTHORS_FULL = '/authors/:mode?/:search?';
export const AUTHORS_VIEW = '/authors/view';
export const AUTHORS_LIST = '/authors/list';

export const SERIES_MODE_VIEW = 'view';
export const SERIES_MODE_LIST = 'list';
export const BOOKS_SERIES_FULL = '/series/:mode?/:search?';
export const SERIES_VIEW = '/series/view';
export const SERIES_LIST = '/series/list';

export const CODING = '/coding';

export const ABOUT = '/about';

// -------------- FIXED PATHS
