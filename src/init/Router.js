import React from 'react';

import { ConnectedRouter } from 'connected-react-router';
import { Switch } from 'react-router-dom';

import Main from '../layouts/Main';
import { reviveItemsCall } from '../redux/persistent/actions';
import PersistenceState from '../redux/persistent/state';
import About from '../views/about/About';
import Anime from '../views/anime/Anime';
import BooksOverview from '../views/booksOverview/BooksOverview';
import { SECTION_AUTHORS, SECTION_BOOKS, SECTION_SERIES } from '../views/booksOverview/constants';
import Games from '../views/games/Games';
import Movies from '../views/movies/Movies';
import { PHOTO_MODE_ALBUM, PHOTO_MODE_SESSION } from '../views/photos/constants';
import Photos from '../views/photos/Photos';
import Stream from '../views/stream/Stream';

import { connectPrefetch } from './actions';
import * as PATHS from './paths';


@connectPrefetch([
  () => reviveItemsCall(PersistenceState),
])
class AppRouter extends React.PureComponent {
  render () {
    const {
      history
    } = this.props;

    return (<ConnectedRouter history={history}>
      <div>
        <Switch>
          <Main exact path={PATHS.PHOTOS_PHOTO_SLUG_Q_QS} part={PATHS.PHOTOS} section={PHOTO_MODE_ALBUM} component={Photos}/>
          <Main exact path={PATHS.PHOTOS_PHOTO_SLUG_WO_SLASH} part={PATHS.PHOTOS} section={PHOTO_MODE_ALBUM} component={Photos}/>
          <Main exact path={PATHS.PHOTOS_PHOTO_SLUG} part={PATHS.PHOTOS} section={PHOTO_MODE_ALBUM} component={Photos}/>

          <Main exact path={PATHS.PHOTOS_SESSIONS} section={PHOTO_MODE_SESSION} part={PATHS.PHOTOS} component={Photos}/>

          <Main exact path={PATHS.PHOTOS_SESSIONS_PHOTO_SLUG_Q_QS} section={PHOTO_MODE_SESSION} part={PATHS.PHOTOS} component={Photos}/>
          <Main exact path={PATHS.PHOTOS_SESSIONS_PHOTO_SLUG_WO_SLASH} section={PHOTO_MODE_SESSION} part={PATHS.PHOTOS} component={Photos}/>
          <Main exact path={PATHS.PHOTOS_SESSIONS_PHOTO_SLUG} section={PHOTO_MODE_SESSION} part={PATHS.PHOTOS} component={Photos}/>

          <Main path={PATHS.GAMES_FULL} part={PATHS.GAMES} component={Games}/>

          <Main exact path={PATHS.ANIME} part={PATHS.ANIME} component={Anime}/>
          <Main exact path={PATHS.ANIME_SELECTED} part={PATHS.ANIME} component={Anime}/>
          <Main exact path={PATHS.ANIME_FULL} part={PATHS.ANIME} component={Anime}/>
          
          <Main exact path={PATHS.MOVIES} part={PATHS.MOVIES} component={Movies}/>
          <Main exact path={PATHS.MOVIES_SELECTED} part={PATHS.MOVIES} component={Movies}/>
          <Main exact path={PATHS.MOVIES_FULL} part={PATHS.MOVIES} component={Movies}/>

          <Main exact path={PATHS.ABOUT} part={PATHS.ABOUT} component={About}/>

          <Main exact path={PATHS.BOOKS_FULL} section={SECTION_BOOKS} part={PATHS.BOOKS} component={BooksOverview}/>
          <Main exact path={PATHS.BOOKS_AUTHORS_FULL} section={SECTION_AUTHORS} part={PATHS.BOOKS} component={BooksOverview}/>
          <Main exact path={PATHS.BOOKS_SERIES_FULL} section={SECTION_SERIES} part={PATHS.BOOKS} component={BooksOverview}/>

          <Main exact path={PATHS.MUSIC} part={PATHS.MUSIC} component={Stream}/>
          <Main exact path={PATHS.STREAM} part={PATHS.PHOTOS} component={Photos}/>
          <Main exact={false} path={PATHS.ROOT} part={PATHS.PHOTOS} section={PHOTO_MODE_SESSION} component={Photos}/>
        </Switch>
      </div>
    </ConnectedRouter>);
  }
}

export default AppRouter;
