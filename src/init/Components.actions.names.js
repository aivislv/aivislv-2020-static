import * as LAYOUTS from '../layouts/actions.names';
import * as PERSISTENT from '../redux/persistent/actions.names';
import * as ANIME from '../views/anime/actions.names';
import * as AUTHORS from '../views/authors/actions.names';
import * as BOOKS from '../views/books/actions.names';
import * as BOOKSOVERVIEW from '../views/booksOverview/actions.names';
import * as GAMES from '../views/games/actions.names';
import * as MOVIES from '../views/movies/actions.names';
import * as PHOTOS from '../views/photos/actions.names';
import * as SERIES from '../views/series/actions.names';


const actionsNames = {
  PERSISTENT,
  PHOTOS,
  GAMES,
  ANIME,
  LAYOUTS,
  MOVIES,
  BOOKSOVERVIEW,
  BOOKS,
  AUTHORS,
  SERIES,
};

export default actionsNames;
