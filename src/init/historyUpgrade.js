import { setItems } from '../redux/persistent/actions';

import {history, store} from './store';


store.dispatch(
    setItems({
        previousPath: '/',
        currentPath: document.location.pathname,
    }, 'localStorage')
);

export const listen = history.listen((location) => {
    const storage = store.getState().persistent.localStorage;
    store.dispatch(
        setItems({
            previousPath: storage.currentPath,
            currentPath: location.pathname,
        }, 'localStorage')
    );
})