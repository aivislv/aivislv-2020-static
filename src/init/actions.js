import createConnectWrappers from 'aivis-connect';

import componentsActions from './Components.actions';
import componentsActionsNames from './Components.actions.names' ;
import graphQLMutations from './graphQLMutations';
import graphQLQueries from './graphQLQueries';


export const actions = {
    ...componentsActions
};

const queries = {
    ...graphQLQueries
};

const mutations = {
    ...graphQLMutations
};

export const C = {
    ...componentsActionsNames
};

const created = createConnectWrappers({ mutations, queries, actions });

export const connectActions = created.connectActions;
export const connectState = created.connectState;
export const connectPrefetch = created.connectPrefetch;
export const connectQuery = created.connectQuery;
export const connectMutation = created.connectMutation;

export default queries;
