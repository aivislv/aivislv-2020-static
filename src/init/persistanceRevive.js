import { reviveItemsCall } from '../redux/persistent/actions';
import PersistenceState from '../redux/persistent/state';

import { store } from './store';


export const persistanceReviver =  async () => {
    await store.dispatch(reviveItemsCall(PersistenceState));
};