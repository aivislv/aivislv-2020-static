import * as viewAbout from '../views/about/queries';
import * as viewAnime from '../views/anime/queries';
import * as viewAuthors from '../views/authors/queries';
import * as viewBooks from '../views/books/queries';
import * as viewGames from '../views/games/queries';
import * as viewMovies from '../views/movies/queries';
import * as viewPhotos from '../views/photos/queries';
import * as viewSeries from '../views/series/queries';
import * as viewStream from '../views/stream/queries';


const graphQLQueries = {
  viewStream,
  viewPhotos,
  viewGames,
  viewAnime,
  viewMovies,
  viewAbout,
  viewBooks,
  viewAuthors,
  viewSeries,
};

export default graphQLQueries;
