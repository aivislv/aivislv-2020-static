import { go, goBack, goForward, push, replace } from 'connected-react-router';

import * as layouts from '../layouts/actions';
import * as persistent from '../redux/persistent/actions';
import * as anime from '../views/anime/actions';
import * as authors from '../views/authors/actions';
import * as books from '../views/books/actions';
import * as booksOverview from '../views/booksOverview/actions';
import * as games from '../views/games/actions';
import * as movies from '../views/movies/actions';
import * as photos from '../views/photos/actions';
import * as series from '../views/series/actions';


// Adding router actions
const router = { push, replace, go, goBack, goForward };

const actions = {
   router,
   persistent,
   photos,
   games,
   layouts,
   anime,
   movies,
   booksOverview,
   books,
   authors,
   series,
};

export default actions;
