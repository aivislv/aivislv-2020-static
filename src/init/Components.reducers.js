import {default as layouts} from '../layouts/reducers';
import {default as persistent} from '../redux/persistent/reducers';
import {default as anime} from '../views/anime/reducers';
import {default as authors} from '../views/authors/reducers';
import {default as books} from '../views/books/reducers';
import {default as booksOverview} from '../views/booksOverview/reducers';
import {default as games} from '../views/games/reducers';
import {default as movies} from '../views/movies/reducers';
import {default as photos} from '../views/photos/reducers';
import {default as series} from '../views/series/reducers';


const reducers = {
  persistent,
  photos,
  layouts,
  games,
  anime,
  movies,
  booksOverview,
  books,
  authors,
  series,
};

export default reducers;
