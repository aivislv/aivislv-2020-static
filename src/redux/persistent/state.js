const State = {
  cookies: {},
  localStorage: {
    authToken: false,
    auth: {
      graphql_auth0_access_token: false,
      graphql_auth0_id_token: false,
      graphql_auth0_expires_at: 0,
    },
    previousPath: '',
    currentPath: '/',
  },
  sessionStorage: {
  },
  isRevived: false,
};

export default State;
