import * as Persistence from '../../helpers/Persistence';

import * as actions from  './actions.names';


export const setItems = (items, store = 'cookies') => {
  Persistence.set(items, store);

  return {
    type: actions.SET_ITEMS,
    items,
    store,
  };
};

export const removeItems = (items, store = 'cookies') => {
  Persistence.remove(items, store);

  return {
    type: actions.REMOVE_ITEMS,
    items,
    store,
  };
};

export const requireItems = () =>({
    type: actions.REQUIRED_ITEMS,
});

export const reviveItems = (newState) =>({
  type: actions.REVIVE_ITEMS,
  newState,
});

export const reviveItemsCall = (state = {}) => {
  return async (dispatch) => {
    dispatch(requireItems());
    return new Promise((resolve) => resolve(Persistence.revive(state)))
      .then((newState) => {
        dispatch(reviveItems(newState));
    });
  }
};
