import _ from 'lodash';

import * as Actions from './actions.names';
import State from './state';


const reducer = (state = State, action) => {
  switch (action.type) {
    case Actions.SET_ITEMS: {
      const newState = {
        ...state
      };

      newState[action.store] = {
        ...state[action.store],
        ...action.items
      };

      return newState;
    }
    case Actions.REMOVE_ITEMS: {
      const myNewState = {
        ...state
      };

      myNewState[action.store] = _.omit(state[action.store], action.items);

      return myNewState;
    }
    case Actions.REVIVE_ITEMS: {
      return {
        ...state,
        cookies: {...state.cookies, ...action.newState.cookies},
        localStorage: {...state.localStorage, ...action.newState.localStorage},
        sessionStorage: {...state.sessionStorage, ...action.newState.sessionStorage},
        isRevived: true,
      }
    }

    default:
      return state;
  }
};

export default reducer;
