import gql from 'graphql-tag';


export const all = gql`
query getStream {
  stream {
    id,
    title,
    created,
    resourceId,
    type,
  }
}
`;

