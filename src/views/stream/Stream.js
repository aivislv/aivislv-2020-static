// @flow

import React from 'react';

import setTitle from '../../helpers/SetTitle';
import { connectQuery } from '../../init/actions';

import style from './stream.css';


@connectQuery({
    query: 'viewStream.all',
    name: 'stream',
})
class Stream extends React.PureComponent {
    static defaultProps = {};
    state = {};

    render = () => {
        setTitle('Stream');

        return (<div className={style.body}>
        </div>);
    }
}

export default Stream;
