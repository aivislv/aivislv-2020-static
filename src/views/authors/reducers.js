import * as Actions from './actions.names';


const reducer = (state = {
  openAuthor: null,
  search: null,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_AUTHOR: {
      return {
        ...state,
        openAuthor: action.payload.authorId,
      }
    }
    case Actions.SET_SEARCH: {
      return {
        ...state,
        search: action.payload.search,
      }
    }
    default:
      return state;
  }

};

export default reducer;
