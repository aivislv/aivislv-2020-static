// @flow

import React from 'react';


import path from "path";

import AuthorsArea from '../../components/authorsArea/AuthorsArea';
import AuthorView from '../../components/authorView/AuthorView';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import {
  AUTHORS_LIST,
  AUTHORS_MODE_LIST,
  AUTHORS_MODE_VIEW,
  BOOKS_LIST, SERIES_LIST,
} from '../../init/paths';

import style from './authors.css';


@connectActions({
  setOpenAuthor: ['authors', 'setOpenAuthor'],
  setSearch: ['authors', 'setSearch'],
})
@connectState({
  openAuthor: ['authors', 'openAuthor'],
  search: ['authors', 'search'],
})
@connectQuery({
  query: 'viewAuthors.authors',
  name: 'authors',
})
class Authors extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setSearch,
      setOpenAuthor,
      match,
    } = this.props;

    if (match?.params?.mode === AUTHORS_MODE_LIST) {
      if (match?.params?.search) {
        setSearch(match?.params?.search);
      } else {
        setSearch('');
      }

      setOpenAuthor(null);
    }

    if (match?.params?.mode === AUTHORS_MODE_VIEW) {
      setOpenAuthor(match?.params?.search);
      setSearch('');
    }
  }

  render = () => {
    const {
      search,
      openAuthor,
      authors,
    } = this.props;

    let isOpenAuthor;

    if (!authors.loading) {
      isOpenAuthor = authors.data?.authors?.find((author) => {
        return author.id === Number(openAuthor);
      });
    }

    return (<div className={style.body}>
      {!authors.loading && <SubNavigation
        buttons={[
          { to: path.join(BOOKS_LIST), text: message('components.booksNavigation.texts.goTo.books'), primary: true},
          { to: path.join(AUTHORS_LIST), text: message('components.booksNavigation.texts.goTo.authors'), primary: true },
          { to: path.join(SERIES_LIST), text: message('components.booksNavigation.texts.goTo.series'), primary: true},
        ]}
        selectedPath={AUTHORS_LIST}
        search={search}
        messages={{search: message('common.search')}}
      />}
      {!authors.loading && <AuthorsArea isOpenAuthor={isOpenAuthor} />}
      {openAuthor && <AuthorView isOpenAuthor={isOpenAuthor} />}
    </div>);
  }
}

export default Authors;
