import gql from 'graphql-tag';


export const authors = gql`
query getAuthors {
  authors {
    goodreadsId
    id
    name
    booksLinked
  }
}
`;

export const author = gql`
query getAuthor($authorId: Int!) {
  author(authorId: $authorId) {
    booksLinked
    goodreadsId
    id
    name
    books {
      goodreadsId
      id
      isOwned
      isRead
      rating
      title
    }
    series {
      title
      id
      visible,
      firstBookId,
      goodreadsId
    }
  }
}
`;