import * as C from './actions.names';


export const setOpenAuthor = (authorId) => ({
  type: C.SET_OPEN_AUTHOR,
  payload: {
    authorId,
  }
});

export const setSearch = (search) => ({
  type: C.SET_SEARCH,
  payload: {
    search,
  }
});
