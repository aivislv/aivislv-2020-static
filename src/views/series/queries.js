import gql from 'graphql-tag';


export const series = gql`
query getSeries {
  series {
    booksLinked
    firstBookId
    goodreadsId
    id
    title
  }
}
`;

export const serie = gql`
query getSerie($seriesId: Int!) {
  serie(seriesId: $seriesId) {
    booksLinked
    firstBookId
    goodreadsId
    id
    title
    books {
      ordered
      lastOrdered
      book {
        authors {
          name
          id
        }
        goodreadsId
        id
        isOwned
        isRead
        title
      }
    }
    authors {
      name
      id
      visible,
      goodreadsId
    }
  }
}
`;