// @flow

import React from 'react';


import path from "path";

import SeriesArea from '../../components/seriesArea/SeriesArea';
import SeriesView from '../../components/seriesView/SeriesView';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import {
  AUTHORS_LIST,
  BOOKS_LIST,
  SERIES_LIST,
  SERIES_MODE_LIST, SERIES_MODE_VIEW,
} from '../../init/paths';

import style from './series.css';


@connectActions({
  setOpenSerie: ['series', 'setOpenSerie'],
  setSearch: ['series', 'setSearch'],
})
@connectState({
  openSerie: ['series', 'openSerie'],
  search: ['series', 'search'],
})
@connectQuery({
  query: 'viewSeries.series',
  name: 'series',
})
class Series extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setSearch,
      setOpenSerie,
      match,
    } = this.props;

    if (match?.params?.mode === SERIES_MODE_LIST) {
      if (match?.params?.search) {
        setSearch(match?.params?.search);
      } else {
        setSearch('');
      }

      setOpenSerie(null);
    }

    if (match?.params?.mode === SERIES_MODE_VIEW) {
      setOpenSerie(match?.params?.search);
      setSearch('');
    }
  }

  render = () => {
    const {
      search,
      openSerie,
      series,
    } = this.props;

    let isOpenSerie;

    if (!series.loading) {
      isOpenSerie = series.data?.series?.find((serie) => {
        return serie.id === Number(openSerie);
      });
    }

    return (<div className={style.body}>
      {!series.loading && <SubNavigation
        buttons={[
          { to: path.join(BOOKS_LIST), text: message('components.booksNavigation.texts.goTo.books'), primary: true},
          { to: path.join(AUTHORS_LIST), text: message('components.booksNavigation.texts.goTo.authors'), primary: true },
          { to: path.join(SERIES_LIST), text: message('components.booksNavigation.texts.goTo.series'), primary: true},
        ]}
        selectedPath={SERIES_LIST}
        search={search}
        messages={{search: message('common.search')}}
      />}
      {!series.loading && <SeriesArea isOpenSerie={isOpenSerie} />}
      {openSerie && <SeriesView isOpenSerie={isOpenSerie} />}
    </div>);
  }
}

export default Series;
