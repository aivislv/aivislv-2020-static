import * as C from './actions.names';


export const setOpenSerie = (serieId) => ({
  type: C.SET_OPEN_SERIE,
  payload: {
    serieId,
  }
});

export const setSearch = (search) => ({
  type: C.SET_SEARCH,
  payload: {
    search,
  }
});
