import * as Actions from './actions.names';


const reducer = (state = {
  openSerie: null,
  search: null,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_SERIE: {
      return {
        ...state,
        openSerie: action.payload.serieId,
      }
    }
    case Actions.SET_SEARCH: {
      return {
        ...state,
        search: action.payload.search,
      }
    }
    default:
      return state;
  }

};

export default reducer;
