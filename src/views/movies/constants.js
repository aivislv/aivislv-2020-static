export const MOVIE_SORT_NO = 'all';
export const MOVIE_SORT_ALPHABET = 'alphabet';
export const MOVIE_SORT_LAST_PLAY = 'added';
// rating is my rating
export const MOVIE_SORT_RATING = 'rating';
// score is user given score
export const MOVIE_SORT_SCORE = 'score';

export const MOVIE_TYPE_WATCHED = 'watched';
export const MOVIE_TYPE_WISHLIST = 'wishlist';

