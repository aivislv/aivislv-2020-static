import * as C from './actions.names';


export const setOpenMovie = (movieId) => ({
  type: C.SET_OPEN_MOVIE,
  payload: {
    movieId,
  }
});

export const setOpenSort = (sortId) => ({
  type: C.SET_OPEN_SORT,
  payload: {
    sortId,
  }
});

export const setSearch = (search) => ({
  type: C.SET_SEARCH,
  payload: {
    search,
  }
});

export const setOpenType = (typeId) => ({
  type: C.SET_OPEN_TYPE,
  payload: {
    typeId,
  }
});
