export const SET_OPEN_MOVIE = 'SET_OPEN_MOVIE';
export const SET_OPEN_SORT = 'SET_OPEN_SORT';
export const SET_SEARCH = 'SET_SEARCH';
export const SET_OPEN_TYPE = 'SET_OPEN_TYPE';