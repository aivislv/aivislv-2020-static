// @flow

import React from 'react';


import path from "path";

import MoviesArea from '../../components/moviesArea/MoviesArea';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import View from '../../components/view/View';
import { config } from '../../config';
import { getMovieImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { MOVIES_LIST } from '../../init/paths';

import * as constants from "./constants";
import style from './movies.css';


@connectActions({
  setOpenSort: ['movies', 'setOpenSort'],
  setOpenType: ['movies', 'setOpenType'],
  setOpenMovie: ['movies', 'setOpenMovie'],
  setSearch: ['movies', 'setSearch'],
})
@connectState({
  openMovie: ['movies', 'openMovie'],
  openSort: ['movies', 'openSort'],
  openType: ['movies', 'openType'],
  search: ['movies', 'search'],
})
@connectQuery({
  query: 'viewMovies.movies',
  name: 'movies',
})
class Movies extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setOpenSort,
      setOpenType,
      setSearch,
      setOpenMovie,
      match,
    } = this.props;

    if (match?.params?.sort) {
      setOpenSort(match.params.sort);
    }

    if (match?.params?.type) {
      setOpenType(match.params.type);
    }

    if (match?.params?.movie) {
      setOpenMovie(match.params.movie);
    } else {
      setOpenMovie(null);
    }

    if (match?.params?.search) {
      setSearch(match?.params?.search);
    } else if (!match?.params?.movie) {
      setSearch('');
    }
  }

  render = () => {
    const {
      openMovie,
      movies,
      openType,
      openSort,
      search,
    } = this.props;

    setTitle('Movie');

    return (<div className={style.body}>
      <SubNavigation
        buttons={[
          { to: path.join(MOVIES_LIST, constants.MOVIE_TYPE_WATCHED, constants.MOVIE_SORT_ALPHABET), text: message('components.moviesNavigation.texts.goTo.watched')},
          { to: path.join(MOVIES_LIST, constants.MOVIE_TYPE_WATCHED, constants.MOVIE_SORT_RATING), text: message('components.moviesNavigation.texts.goTo.rating')},
          { to: path.join(MOVIES_LIST, constants.MOVIE_TYPE_WATCHED, constants.MOVIE_SORT_LAST_PLAY), text: message('components.moviesNavigation.texts.goTo.updated')},
          { to: path.join(MOVIES_LIST, constants.MOVIE_TYPE_WISHLIST, constants.MOVIE_SORT_ALPHABET), text: message('components.moviesNavigation.texts.goTo.wishlist')},
        ]}
        selectedPath={path.join(MOVIES_LIST, openType, openSort)}
        search={search}
        messages={{search: message('common.search')}}
      />
      <MoviesArea />
      {openMovie && <View
        listData={movies}
        openItem={openMovie}
        messages={{
          openResource: message('common.open').replace('#SOURCE#', message('specs.imdb')),
          rating: message('common.rating'),
          close: message('common.close'),
          back: message('common.back'),
        }}
        backLink={path.join(MOVIES_LIST, openType, openSort, search || '')}
        module="movies"
        title="Movie"
        linkOutFile="imdbLogo.png"
        outLink={config.movieImdbLink}
        serviceIdTitle="imdbId"
        getImageLink={getMovieImageLink}
      />}
    </div>);
  }
}

export default Movies;
