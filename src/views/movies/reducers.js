import * as Actions from './actions.names';
import * as Constants from './constants';


const reducer = (state = {
  openSort: Constants.MOVIE_SORT_ALPHABET,
  search: null,
  openType: Constants.MOVIE_TYPE_WATCHED,
  openMovie: null,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_MOVIE: {
      return {
        ...state,
        openMovie: action.payload.movieId,
      }
    }
    case Actions.SET_SEARCH: {
      return {
        ...state,
        search: action.payload.search,
      }
    }
    case Actions.SET_OPEN_SORT: {
      if (Object.values(Constants).includes(action.payload.sortId)) {
        return {
          ...state,
          openSort: action.payload.sortId,
        }
      }

      return state;
    }
    case Actions.SET_OPEN_TYPE: {
      if (Object.values(Constants).includes(action.payload.typeId)) {
        return {
          ...state,
          openType: action.payload.typeId
        }
      }

      return state;
    }
    default:
      return state;
  }

};

export default reducer;
