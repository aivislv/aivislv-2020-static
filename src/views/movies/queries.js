import gql from 'graphql-tag';


export const movies = gql`
query getMovies {
  movies {
    id,
    title,
    rating,
    status,
    imdbId,
    seenUnix,
    score,
    year,
  }
}
`;