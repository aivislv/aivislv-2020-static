// @flow

import React from 'react';

import { connectActions, connectState } from '../../init/actions';
import Authors from '../authors/Authors';
import Books from '../books/Books';
import Series from '../series/Series';

import style from './booksOverview.css';
import { SECTION_AUTHORS, SECTION_BOOKS, SECTION_SERIES } from './constants';


type Props = {
  section: string,
  setOpenSection: Function,
  openSection: string,
}

@connectActions({
  setOpenSection: ['booksOverview', 'setOpenSection'],
})
@connectState({
  openSection: ['booksOverview', 'openSection'],
})
class BooksOverview extends React.PureComponent<Props> {
  static defaultProps = {};
  state = {};

  componentDidMount(): void {
    this.setSection();
  }

  componentDidUpdate(): void {
    this.setSection();
  }

  setSection = () => {
    const {
      setOpenSection,
      section,
    } = this.props;

    setOpenSection(section);
  }

  render = () => {
    const {
      openSection,
    } = this.props;

    return (<div className={style.body}>
      {openSection === SECTION_BOOKS && <Books {...this.props} />}
      {openSection === SECTION_AUTHORS && <Authors {...this.props} />}
      {openSection === SECTION_SERIES && <Series {...this.props} />}
    </div>);
  }
}

export default BooksOverview;
