import * as C from './actions.names';


export const setOpenSection = (sectionId) => ({
  type: C.SET_OPEN_SECTION,
  payload: {
    sectionId,
  }
});
