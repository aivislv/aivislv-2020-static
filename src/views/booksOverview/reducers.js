import * as Actions from './actions.names';
import * as Constants from './constants';


const reducer = (state = {
  openSection: Constants.SECTION_BOOKS,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_SECTION: {
      if (Object.values(Constants).includes(action.payload.sectionId)) {
        return {
          ...state,
          openSection: action.payload.sectionId,
        }
      }

      return state;
    }
    default:
      return state;
  }

};

export default reducer;
