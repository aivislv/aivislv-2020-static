// @flow

import React from 'react';

import path from "path";

import BooksArea from '../../components/booksArea/BooksArea';
import BookView from '../../components/bookView/BookView';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import { message } from '../../helpers/Translate';
import {connectActions, connectQuery, connectState} from '../../init/actions';
import {AUTHORS_LIST, BOOKS_LIST, BOOKS_MODE_LIST, BOOKS_MODE_VIEW, GAMES_SHOW, SERIES_LIST} from '../../init/paths';

import style from './books.css';
import * as constants from './constants';
import {createBookPath, createGamePath} from "../../helpers/DynamicPaths";
import {setOpenOwnership} from "./actions";


@connectActions({
  setOpenType: ['books', 'setOpenType'],
  setOpenOwnership: ['books', 'setOpenOwnership'],
  setOpenBook: ['books', 'setOpenBook'],
  setOpenTags: ['books', 'setOpenTags'],
  setOpenSort: ['books', 'setOpenSort'],
  setSearch: ['books', 'setSearch'],
})
@connectState({
  openBook: ['books', 'openBook'],
  openType: ['books', 'openType'],
  openSort: ['books', 'openSort'],
  openTags: ['books', 'openTags'],
  openOwned: ['books', 'openOwned'],
  search: ['books', 'search'],
})
@connectQuery({
  query: 'viewBooks.books',
  name: 'books',
})
class Books extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setOpenType,
      setSearch,
      setOpenBook,
      setOpenTags,
      setOpenOwnership,
      setOpenSort,
      match,
    } = this.props;

  if (match?.params?.mode === BOOKS_MODE_LIST) {

    if (match?.params?.props) {
      const allProps = JSON.parse(match.params.props)

      if (allProps.type) {
        setOpenType(allProps.type);
      }

      if (allProps.tags) {
        setOpenTags(allProps.tags);
      }

      if (allProps.owned) {
        setOpenOwnership(allProps.owned);
      }

      if (allProps.sort) {
        setOpenSort(allProps.sort);
      }

      if (allProps.search) {
        setSearch(allProps.search);
      } else {
        setSearch('');
      }
    }

    setOpenBook(null);
  }

    if (match?.params?.mode === BOOKS_MODE_VIEW) {
      setOpenBook(match?.params?.props);
    }
  }

  render = () => {
    const {
      openType,
      openTags,
      openOwned,
      search,
      openBook,
      openSort,
    } = this.props;

    return (<div className={style.body}>
      <SubNavigation
          buttons={[
            {
              to: createBookPath(BOOKS_LIST),
              text: message('components.booksNavigation.texts.goTo.books'),
              primary: true
            },
            {
              to: path.join(AUTHORS_LIST),
              text: message('components.booksNavigation.texts.goTo.authors'),
              primary: true
            },
            {
              to: path.join(SERIES_LIST),
              text: message('components.booksNavigation.texts.goTo.series'),
              primary: true
            },
            {
              to: createBookPath(BOOKS_LIST, {
                type: constants.BOOK_TYPE_NO,
                owned: openOwned,
                tags: openTags,
                search: search,
                sort: constants.BOOK_SORT_ALPHABET
              }),
              text: message('components.booksNavigation.texts.goTo.all'),
            },
            {
              to: createBookPath(BOOKS_LIST, {
                type: constants.BOOK_TYPE_HAD_READ,
                owned: openOwned,
                tags: openTags,
                search: search,
                sort: openSort
              }),
              text: message('components.booksNavigation.texts.goTo.hadRead')
            },
            {
              to: createBookPath(BOOKS_LIST, {
                type: constants.BOOK_TYPE_WISHLIST,
                owned: openOwned,
                tags: openTags,
                search: search,
                sort: openSort
              }),
              text: message('components.booksNavigation.texts.goTo.wishlist')
            },
            {
              to: createBookPath(BOOKS_LIST, {
                type: constants.BOOK_TYPE_READING,
                owned: openOwned,
                tags: openTags,
                search: search,
                sort: openSort
              }),
              text: message('components.booksNavigation.texts.goTo.reading')
            },
          ]}
          selectedPath={createBookPath(BOOKS_LIST, {
            type: openType,
            owned: openOwned,
            tags: openTags,
            search: search,
            sort: openSort
          })}
          search={search}
          searchPath={(searchProp) => createBookPath(BOOKS_LIST, {
            type: openType,
            tags: openTags,
            search: searchProp,
            sort: openSort
          })}
          messages={{search: message('common.search')}}
      />
      <BooksArea/>
      {openBook && <BookView />}
    </div>);
  }
}

export default Books;
