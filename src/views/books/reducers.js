import * as Actions from './actions.names';
import * as Constants from './constants';


const reducer = (state = {
  openBook: null,
  search: null,
  openTags: [],
  openOwned: Constants.BOOK_OWNERSHIP_ALL,
  openType: Constants.BOOK_TYPE_NO,
  openSort: Constants.BOOK_SORT_ALPHABET,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_BOOK: {
      return {
        ...state,
        openBook: action.payload.bookId,
      }
    }
    case Actions.SET_SEARCH: {
      return {
        ...state,
        search: action.payload.search,
      }
    }
    case Actions.SET_OPEN_TAGS: {
      let needsUpdate = false;

      action.payload.tags.forEach((tag) => {
        if (!state.openTags.includes(tag)) {
          needsUpdate = true
        }
      })

      if (action.payload.tags.length !== state.openTags.length) {
        needsUpdate = true
      }

      if (needsUpdate) {
        return {
          ...state,
          openTags: [...action.payload.tags]
        };
      } else {
        return state
      }
    }
    case Actions.SET_OPEN_TYPE: {
      if (Object.values(Constants).includes(action.payload.typeId)) {
        return {
          ...state,
          openType: action.payload.typeId
        }
      }

      return state;
    }
    case Actions.SET_SORT: {
      if (Object.values(Constants).includes(action.payload.typeId)) {
        return {
          ...state,
          openSort: action.payload.typeId
        }
      }

      return state;
    }
    case Actions.SET_OPEN_OWNERSHIP: {
      if (Object.values(Constants).includes(action.payload.ownershipId)) {
        return {
          ...state,
          openOwned: action.payload.ownershipId
        }
      }

      return state;
    }
    default:
      return state;
  }

};

export default reducer;
