import * as C from './actions.names';


export const setOpenBook = (bookId) => ({
  type: C.SET_OPEN_BOOK,
  payload: {
    bookId,
  }
});

export const setSearch = (search) => ({
  type: C.SET_SEARCH,
  payload: {
    search,
  }
});

export const setOpenType = (typeId) => ({
  type: C.SET_OPEN_TYPE,
  payload: {
    typeId,
  }
});

export const setOpenSort = (typeId) => ({
  type: C.SET_SORT,
  payload: {
    typeId,
  }
});

export const setOpenOwnership = (ownershipId) => ({
  type: C.SET_OPEN_OWNERSHIP,
  payload: {
    ownershipId,
  }
});

export const setOpenTags = (tags) => ({
  type: C.SET_OPEN_TAGS,
  payload: {
    tags,
  }
});
