export const BOOK_TYPE_NO = 'no';
export const BOOK_TYPE_WISHLIST = 'wishlist';
export const BOOK_TYPE_HAD_READ = 'hadRead';
export const BOOK_TYPE_READING = 'reading';

export const BOOK_OWNERSHIP_ALL = "all";
export const BOOK_OWNERSHIP_YES = "yes";
export const BOOK_OWNERSHIP_NO = "no";

export const BOOK_SORT_ALPHABET = "alphabet"

export const BOOK_SORT_UPDATED = "updated"

export const BOOK_SORT_WISH_PRIORITY = "wishPriority"