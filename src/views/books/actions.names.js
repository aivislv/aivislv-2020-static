export const SET_OPEN_BOOK = 'SET_OPEN_BOOK';
export const SET_SEARCH = 'SET_SEARCH';
export const SET_OPEN_TYPE = 'SET_OPEN_TYPE';
export const SET_OPEN_TAGS = 'SET_OPEN_TAGS';
export const SET_OPEN_OWNERSHIP = 'SET_OPEN_OWNERSHIP';

export const SET_SORT = 'SET_SORT';