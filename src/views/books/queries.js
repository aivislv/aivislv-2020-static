import gql from 'graphql-tag';


export const books = gql`
query getBooks {
  books {
    goodreadsId
    id
    isOwned
    isRead
    title
    authors {
      name
    }
    series {
      series {
        title
      }
    }
    tags {
      tag {
        id
        title
      }
    }
    rating,
    wishWeight
  }
}
`;

export const book = gql`
query getBook($bookId: Int!) {
  book(bookId: $bookId) {
    created
    goodreadsId
    id
    isOwned
    isRead
    isbn10
    title
    rating
    averageRating
    authors {
      name
      goodreadsId
      id
    }
    series {
      ordered
      series {
        id
        firstBookId
        goodreadsId
        title
      }
    }
    tags {
      tag {
        id
        title
      }
    }
  }
}
`

export const tags = gql`
query getTags {
  bookTags {
    title
    id
  }
}`
;