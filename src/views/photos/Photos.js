// @flow

import React from 'react';

import path from 'path';

import AlbumList from '../../components/albumList/AlbumList';
import PhotoArea from '../../components/photoArea/PhotoArea';
import PhotoPanner from '../../components/photoPanner/photoPanner';
import PhotoSessionArea from '../../components/photoSessionArea/PhotoSessionArea';
import PhotoSessionList from '../../components/photoSessionList/PhotoSessionList';
import PhotoViewer from '../../components/photoViewer/photoViewer';
import { SIZE_SPECIAL_ALBUM_WRAP } from '../../elements/constants/size';
import { getImageSlug } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { PHOTOS, PHOTOS_SESSIONS } from '../../init/paths';

import { PHOTO_MODE_ALBUM, PHOTO_MODE_SESSION } from './constants';
import style from './photos.css';


@connectState({
  openAlbum: ['photos', 'openAlbum'],
  openSession: ['photos', 'openSession'],
  openImage: ['photos', 'openImage'],
  sizeCurrent: ['layouts', 'sizeCurrent'],
})
@connectActions({
  push: ['router', 'push'],
  setOpenAlbum: ['photos', 'setOpenAlbum'],
  setOpenSession: ['photos', 'setOpenSession'],
  setOpenImageId: ['photos', 'setOpenImageId'],
})
@connectQuery({
  query: 'viewPhotos.album',
  name: 'album',
  vars: {
    albumId: (props) => Number(props.match?.params?.album) || null
  },
  loading: (props) => props.section !== PHOTO_MODE_ALBUM || !Number(props.match?.params?.album),
})
@connectQuery({
  query: 'viewPhotos.session',
  name: 'session',
  vars: {
    sessionId: (props) => Number(props.match?.params?.session) || null
  },
  loading: (props) => props.section !== PHOTO_MODE_SESSION || !Number(props.match?.params?.session),
})
@connectQuery({
  query: 'viewPhotos.gallery',
  name: 'gallery',
})
class Photos extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.checkSetOpenSession();
  }

  componentDidUpdate() {
    const {
      match,
      sizeCurrent,
      push,
      album,
      openAlbum,
      setOpenAlbum,
      openImage,
      setOpenImageId,
      session,
      section,
    } = this.props;

    // Mobile view switch for album
    if (match?.params?.album && SIZE_SPECIAL_ALBUM_WRAP >= sizeCurrent && !album.loading && !match?.params?.photo) {
      push(path.join(PHOTOS, match.params.album, String(album.data.album.images[0].id), getImageSlug(album.data.album.images[0])));
    }

    // Mobile view switch for session
    if (match?.params?.session && SIZE_SPECIAL_ALBUM_WRAP >= sizeCurrent && !session.loading && !match?.params?.photo) {
      push(path.join(PHOTOS_SESSIONS, match.params.session, String(session.data.session.images[0].id), getImageSlug(session.data.session.images[0])));
    }

    if (!match?.params?.photo && openImage) {
      setOpenImageId(null);
    }

    if (!match?.params?.album && section === PHOTO_MODE_ALBUM && openAlbum && SIZE_SPECIAL_ALBUM_WRAP >= sizeCurrent) {
      setOpenAlbum(null);
    } else if (!match?.params?.album && section === PHOTO_MODE_ALBUM && openAlbum) {
      push(path.join(PHOTOS, String(openAlbum)));
    }

    this.checkSetOpenSession();
  }

  checkSetOpenSession() {
    const {
      openSession,
      setOpenAlbum,
      setOpenSession,
      match,
      section,
      push,
    } = this.props;

    if (!match?.params?.session && section === PHOTO_MODE_SESSION && openSession) {
      setOpenSession(null);
      setOpenAlbum(null);
      push(path.join(PHOTOS_SESSIONS));
    }

    if (match?.params?.session && openSession !== match?.params?.session) {
      setOpenSession(match?.params?.session);
      setOpenAlbum(null);
    }
  }

  ifDisplayAlbumList() {
    const {
      section,
      sizeCurrent,
    } = this.props;

    const isSmall = SIZE_SPECIAL_ALBUM_WRAP >= sizeCurrent;

    return !isSmall || section === PHOTO_MODE_ALBUM;
  }

  render = () => {
    const {
      openAlbum,
      openSession,
      section,
      match,
      sizeCurrent,
    } = this.props;

    setTitle('Photos');

    return (<div className={style.body}>
      <div className={style.mainBody}>
        {this.ifDisplayAlbumList()  && <AlbumList loadAlbum={match?.params?.album} section={section} isPhoto={!!match?.params?.photo}/>}
        {!match?.params?.photo && section === PHOTO_MODE_ALBUM && openAlbum && <PhotoArea />}
        {!match?.params?.photo && section === PHOTO_MODE_SESSION && openSession && <PhotoSessionArea sessionId={match?.params?.session}/>}
        {!match?.params?.photo && section === PHOTO_MODE_SESSION && !openSession && <PhotoSessionList session={match?.params?.session} />}
        {match?.params?.photo && SIZE_SPECIAL_ALBUM_WRAP < sizeCurrent && <PhotoViewer album={openAlbum} session={openSession} section={section} photo={match?.params?.photo} />}
        {match?.params?.photo && SIZE_SPECIAL_ALBUM_WRAP >= sizeCurrent && <PhotoPanner key={`${openAlbum}-${openSession}`} album={openAlbum} session={openSession} section={section} photo={match?.params?.photo} />}
      </div>
    </div>);
  }
}

export default Photos;
