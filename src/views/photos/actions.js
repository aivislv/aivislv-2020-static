import * as C from './actions.names';


export const setOpenAlbum = (albumId) => ({
  type: C.SET_OPEN_ALBUM,
  payload: {
    albumId,
  }
});

export const setOpenSession = (sessionId) => ({
  type: C.SET_OPEN_SESSION,
  payload: {
    sessionId,
  }
});

export const setOpenImageId = (imageId) => ({
  type: C.SET_OPEN_IMAGE_ID,
  payload: {
    imageId,
  }
});

