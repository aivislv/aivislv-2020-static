import gql from 'graphql-tag';


export const gallery = gql`
query getGallery {
  gallery {
    id,
    title,
    order,
    image {
      id,
      title,
      extension,
      persons,
      place,
    }
  }
}
`;

export const sessions = gql`
query getSessions {
  sessions {
    id,
    title,
    image {
      id,
      title,
      extension,
      persons,
      place,
    }
  }
}
`;

export const album = gql`
query getAlbum($albumId: Int) {
  album(albumId: $albumId) {
    title,
    id,
    images {
        id,
        title,
        order,
        extension,
        persons,
        place,
        created,
        song {
          title,
          artist,
          spotifyId,
          youtubeLink,
        }
    }
  }
}
`;

export const session = gql`
query getSession($sessionId: Int!) {
  session(sessionId: $sessionId) {
    title,
    id,
    images {
        id,
        title,
        order,
        extension,
        persons,
        place,
        created,
        song {
          title,
          artist,
          spotifyId,
          youtubeLink,
        }
    }
  }
}
`;

export const image = gql`
query getImage($imageId: Int!) {
  image(imageId: $imageId) {
    id,
    title,
    extension,
    persons,
    place,
    created,
    song {
      title,
      artist,
      spotifyId,
      youtubeLink,
    },
    session {
      id,
      title,
    }
  }
}
`;
