import * as Actions from './actions.names';


const reducer = (state = {
  openAlbum: null,
  openMode: null,
  openImageId: null,
  openSession: null,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_ALBUM: {
      return {
        ...state,
        openAlbum: action.payload.albumId,
      }
    }
    case Actions.SET_OPEN_IMAGE_ID: {
      return {
        ...state,
        openImageId: action.payload.imageId,
      }
    }
    case Actions.SET_OPEN_SESSION: {
      return {
        ...state,
        openSession: action.payload.sessionId,
      }
    }
    default:
      return state;
  }

};

export default reducer;
