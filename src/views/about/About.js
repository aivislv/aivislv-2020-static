// @flow

import React from 'react';

// eslint-disable-next-line import/no-webpack-loader-syntax
import "!css-loader!./aboutBase.css";
import Text from '../../elements/text/Text';
import setTitle from '../../helpers/SetTitle';
import { connectQuery } from '../../init/actions';

import style from './about.css';


type Props = {
  content: Object,
}

@connectQuery({
  query: 'viewAbout.content',
  name: 'content',
  vars: {
    contentId: 1,
  },
})
class About extends React.PureComponent<Props> {
  static defaultProps = {};
  state = {};

  render = () => {
    const {
      content,
    } = this.props;

    setTitle('About');

    return (<div className={style.body}>
      {!content.loading && content.data.content && <div>
        <Text type="h1" className={style.header}>{content.data.content.title}</Text>
        <img src={content.data.content.titlePhoto} />
        <Text type="normal" html value={content.data.content.content} />
      </div>}
    </div>);
  }
}

export default About;
