import gql from 'graphql-tag';


export const content = gql`
query getContent($contentId: Int!) {
  content(contentId: $contentId) {
    title,
    titlePhoto,
    content,
  }
}
`;