// @flow

import React from 'react';

import { Notification } from '../../elements';

import style from './404.css';


type Props = {
}

type State = {
}

class Root extends React.PureComponent<Props, State> {
  static defaultProps = {};
  state = {};

  render = () => {
    return (<div className={style.body}>
      <Notification>Page not found!</Notification>
    </div>);
  }
}

export default Root;
