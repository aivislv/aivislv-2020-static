import gql from 'graphql-tag';


export const anime = gql`
query getAnimes {
  animes {
    id,
    title,
    rating,
    status,
    malId,
    score,
    endDate,
    updated,
    wishWeight
  }
}
`;