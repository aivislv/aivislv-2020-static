export const ANIME_SORT_NO = 'all';
export const ANIME_SORT_ALPHABET = 'alphabet';
export const ANIME_SORT_LAST_PLAY = 'lastWatch';
export const ANIME_SORT_WISH_WEIGHT = 'wishWeight';

// rating is my rating
export const ANIME_SORT_RATING = 'rating';
// score is user given score
export const ANIME_SORT_SCORE = 'score';

export const ANIME_TYPE_WATCHED = 'watched';
export const ANIME_TYPE_WISHLIST = 'wishlist';
export const ANIME_TYPE_WATCHING= 'watching';

