export const SET_OPEN_ANIME = 'SET_OPEN_ANIME';
export const SET_OPEN_SORT = 'SET_OPEN_SORT';
export const SET_SEARCH = 'SET_SEARCH';
export const SET_OPEN_TYPE = 'SET_OPEN_TYPE';