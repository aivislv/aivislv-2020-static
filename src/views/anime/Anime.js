// @flow

import React from 'react';

import path from "path";

import AnimeArea from '../../components/animeArea/AnimeArea';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import View from '../../components/view/View';
import { config } from '../../config';
import { getAnimeImageLink } from '../../helpers/ProcessImages';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import { connectActions, connectQuery, connectState } from '../../init/actions';
import { ANIME_LIST } from '../../init/paths';

import style from './anime.css';
import * as constants from './constants';


@connectActions({
  setOpenSort: ['anime', 'setOpenSort'],
  setOpenType: ['anime', 'setOpenType'],
  setOpenAnime: ['anime', 'setOpenAnime'],
  setSearch: ['anime', 'setSearch'],
})
@connectState({
  openAnime: ['anime', 'openAnime'],
  openSort: ['anime', 'openSort'],
  openType: ['anime', 'openType'],
  search: ['anime', 'search'],
})
@connectQuery({
  query: 'viewAnime.anime',
  name: 'anime',
})
class Anime extends React.PureComponent{
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setOpenSort,
      setOpenType,
      setSearch,
      setOpenAnime,
      match,
    } = this.props;

    if (match?.params?.sort) {
      setOpenSort(match.params.sort);
    }

    if (match?.params?.type) {
      setOpenType(match.params.type);
    }

    if (match?.params?.anime) {
      setOpenAnime(match.params.anime);
    } else {
      setOpenAnime(null);
    }

    if (match?.params?.search) {
      setSearch(match?.params?.search);
    } else if (!match?.params?.anime) {
      setSearch('');
    }
  }

  render = () => {
    const {
      openAnime,
      anime,
      openType,
      openSort,
      search,
    } = this.props;

    setTitle('Anime');

    return (<div className={style.body}>
      <SubNavigation
        buttons={[
          { to: path.join(ANIME_LIST, constants.ANIME_TYPE_WATCHED, constants.ANIME_SORT_ALPHABET), text: message('components.animeNavigation.texts.goTo.watched')},
          { to: path.join(ANIME_LIST, constants.ANIME_TYPE_WATCHED, constants.ANIME_SORT_RATING), text: message('components.animeNavigation.texts.goTo.rating')},
          { to: path.join(ANIME_LIST, constants.ANIME_TYPE_WATCHED, constants.ANIME_SORT_LAST_PLAY), text: message('components.animeNavigation.texts.goTo.updated')},
          { to: path.join(ANIME_LIST, constants.ANIME_TYPE_WATCHING, constants.ANIME_SORT_ALPHABET), text: message('components.animeNavigation.texts.goTo.watching')},
          {
            to: path.join(ANIME_LIST, constants.ANIME_TYPE_WISHLIST, constants.ANIME_SORT_WISH_WEIGHT),
            text: message('components.animeNavigation.texts.goTo.wishlist')
          },
        ]}
        selectedPath={path.join(ANIME_LIST, openType, openSort)}
        search={search}
        messages={{search: message('common.search')}}
      />
      <AnimeArea />
      {openAnime && <View
        listData={anime}
        openItem={openAnime}
        messages={{
          openResource: message('common.open').replace('#SOURCE#', message('specs.mal')),
          rating: message('common.rating'),
          close: message('common.close'),
          back: message('common.back'),
        }}
        backLink={path.join(ANIME_LIST, openType, openSort, search || '')}
        module="animes"
        title="Anime"
        linkOutFile="malLogo.png"
        outLink={config.animeMalLink}
        serviceIdTitle="malId"
        getImageLink={getAnimeImageLink}
      />}
    </div>);
  }
}

export default Anime;
