import * as C from './actions.names';


export const setOpenAnime = (animeId) => ({
  type: C.SET_OPEN_ANIME,
  payload: {
    animeId,
  }
});

export const setOpenSort = (sortId) => ({
  type: C.SET_OPEN_SORT,
  payload: {
    sortId,
  }
});

export const setSearch = (search) => ({
  type: C.SET_SEARCH,
  payload: {
    search,
  }
});

export const setOpenType = (typeId) => ({
  type: C.SET_OPEN_TYPE,
  payload: {
    typeId,
  }
});
