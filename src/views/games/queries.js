import gql from 'graphql-tag';


export const games = gql`
query getGames {
  games {
    id,
    title,
    bggId,
    plays,
    rating,
    expansion,
    minPlayers,
    maxPlayers,
    minPlaytime,
    maxPlaytime,
    avgScore,
    rank,
    preordered,
    wishlistPriority,
    type,
    expansionCount,
    lastPlayUnix,
    players,
    wishWeight,
    lastPlay {
      date
    },
    tags {
      id,
      tag,
    }
  }
}
`;

export const tags = gql`
query getTags {
  tags {
    id,
    tag,
    promote,
    category {
      id,
      name,
      mandatory,
      color
    }
  }
}
`;

export const game = gql`
query getGame($gameId: Int!) {
  game(gameId: $gameId) {
    id,
    title,
    expansions {
      id,
      title,
      bggId,
      type,
      preordered,
      wishlistPriority,
      isPromo,
    },
    bggId,
    plays,
    rating,
    expansion,
    minPlayers,
    maxPlayers,
    minPlaytime,
    maxPlaytime,
    preordered,
    wishlistPriority,
    avgScore,
    rank,
    type,
    players,
    lastPlayUnix,
    lastPlay {
      date
    },
    tags {
      id,
      tag,
      category {
        id,
        name,
        color,
      }
    }
  }
}
`;