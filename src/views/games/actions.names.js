export const SET_OPEN_GAME = 'SET_OPEN_GAME';
export const SET_OPEN_FILTER = 'SET_OPEN_FILTER';
export const SET_OPEN_SORT = 'SET_OPEN_SORT';
export const SET_SEARCH = 'SET_SEARCH';
export const SET_OPEN_TYPE = 'SET_OPEN_TYPE';
export const SET_OPEN_SELECT = 'SET_OPEN_SELECT';
export const SET_CLOSE_SELECT = 'SET_CLOSE_SELECT';
export const SET_OPEN_TAGS = 'SET_OPEN_TAGS';