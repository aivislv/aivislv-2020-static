export const GAME_FILTER_NO = 'no';
export const GAME_FILTER_UNPLAYED = 'unplayed';
export const GAME_FILTER_PLAYED = 'played';
export const GAME_FILTER_SHORT = 'short';
export const GAME_FILTER_MEDIUM = 'medium';
export const GAME_FILTER_LONG = 'long';
export const GAME_FILTER_2P = '2p';
export const GAME_FILTER_2P_ONLY = '2ponly';
export const GAME_FILTER_1P = '1p';
export const GAME_FILTER_3P = '3p';
export const GAME_FILTER_4P = '4p';
export const GAME_FILTER_5P = '5p';
export const GAME_FILTER_6P = '6p';
export const GAME_FILTER_7P = '7p';
export const GAME_FILTER_8P = '8p';
export const GAME_FILTER_9P = '9p';

export const GAME_SORT_WISHLIST = 'wishlist';

export const GAME_SORT_WISH_WEIGHT = "wishweight"

export const GAME_SORT_NO = 'all';
export const GAME_SORT_ALPHABET = 'alphabet';
export const GAME_SORT_PLAYS = 'plays';
export const GAME_SORT_LAST_PLAY = 'lastPlay';
// rating is my rating
export const GAME_SORT_RATING = 'rating';
// score is user given score
export const GAME_SORT_SCORE = 'score';

export const GAME_TYPE_OWNED = 'owned';
export const GAME_TYPE_WISHLIST = 'wishlist';
export const GAME_TYPE_PLAYED = 'played'

export const GAME_EXPANSIONS_MAX = 4;
