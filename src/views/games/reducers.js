import * as Actions from './actions.names';
import * as Constants from './constants';


const reducer = (state = {
  openGame: null,
  openFilter: Constants.GAME_FILTER_NO,
  openSort: Constants.GAME_SORT_ALPHABET,
  search: null,
  openType: Constants.GAME_TYPE_OWNED,
  openTags: [],
  selectId: null,
  select: false,
}, action) => {
  switch (action.type) {
    case Actions.SET_OPEN_GAME: {
      return {
        ...state,
        openGame: action.payload.gameId,
      };
    }
    case Actions.SET_OPEN_SELECT: {
      return {
        ...state,
        selectId: action.payload.selectId,
        select: action.payload.select
      };
    }
    case Actions.SET_CLOSE_SELECT: {
      return {
        ...state,
        select: action.payload.select
      };
    }
    case Actions.SET_OPEN_TAGS: {
      let needsUpdate = false;

      action.payload.tags.forEach((tag) => {
        if (!state.openTags.includes(tag)) {
          needsUpdate = true
        }
      })

      if (action.payload.tags.length !== state.openTags.length) {
        needsUpdate = true
      }

      if (needsUpdate) {
        return {
          ...state,
          openTags: [...action.payload.tags]
        };
      } else {
        return state
      }
    }
    case Actions.SET_SEARCH: {
      return {
        ...state,
        search: action.payload.search,
      };
    }
    case Actions.SET_OPEN_SORT: {
      if (Object.values(Constants).includes(action.payload.sortId)) {
        return {
          ...state,
          openSort: action.payload.sortId,
        }
      }

      return state;
    }
    case Actions.SET_OPEN_FILTER: {
      if (Object.values(Constants).includes(action.payload.filterId)) {
        return {
          ...state,
          openFilter: action.payload.filterId,
        }
      }

      return state;
    }
    case Actions.SET_OPEN_TYPE: {
      if (Object.values(Constants).includes(action.payload.typeId)) {
        return {
          ...state,
          openType: action.payload.typeId
        }
      }

      return state;
    }
    default:
      return state;
  }

};

export default reducer;
