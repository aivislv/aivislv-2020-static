import * as C from './actions.names';


export const setOpenGame = (gameId) => ({
  type: C.SET_OPEN_GAME,
  payload: {
    gameId,
  }
});

export const setOpenFilter = (filterId) => ({
  type: C.SET_OPEN_FILTER,
  payload: {
    filterId,
  }
});

export const setOpenSort = (sortId) => ({
    type: C.SET_OPEN_SORT,
    payload: {
        sortId,
    }
});

export const setOpenTags = (tags) => ({
    type: C.SET_OPEN_TAGS,
    payload: {
        tags,
    }
});


export const setSearch = (search) => ({
    type: C.SET_SEARCH,
    payload: {
        search,
    }
});

export const setOpenType = (typeId) => ({
    type: C.SET_OPEN_TYPE,
  payload: {
    typeId,
  }
});

export const setOpenSelect = (selectId) => ({
  type: C.SET_OPEN_SELECT,
  payload: {
    selectId,
    select: true,
  }
});

export const setCloseSelect = (selectId) => ({
  type: C.SET_CLOSE_SELECT,
  payload: {
    selectId,
    select: false,
  }
});