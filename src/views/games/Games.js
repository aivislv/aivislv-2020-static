// @flow

import React from 'react';

import GameArea from '../../components/gameArea/GameArea';
import GameView from '../../components/gameView/GameView';
import SubNavigation from '../../components/subNavigation/SubNavigation';
import setTitle from '../../helpers/SetTitle';
import { message } from '../../helpers/Translate';
import {connectActions, connectQuery, connectState} from '../../init/actions';
import {GAMES_MODE_LIST, GAMES_MODE_VIEW, GAMES_SHOW} from '../../init/paths';

import * as constants from './constants';
import { GAME_EXPANSIONS_MAX } from './constants';
import style from './games.css';
import { setCloseSelect } from './actions';
import {createGamePath} from "../../helpers/DynamicPaths";

@connectActions({
  setOpenFilter: ['games', 'setOpenFilter'],
  setOpenSort: ['games', 'setOpenSort'],
  setOpenType: ['games', 'setOpenType'],
  setOpenGame: ['games', 'setOpenGame'],
  setOpenSelect: ['games', 'setOpenSelect'],
  setCloseSelect: ['games', 'setCloseSelect'],
  setOpenTags: ['games', 'setOpenTags'],
  setSearch: ['games', 'setSearch'],
})
@connectState({
  openGame: ['games', 'openGame'],
  openSort: ['games', 'openSort'],
  openType: ['games', 'openType'],
  openTags: ['games', 'openTags'],
  openFilter: ['games', 'openFilter'],
  search: ['games', 'search'],
  select: ['games', 'select'],
})
@connectQuery({
  query: 'viewGames.games',
  name: 'games',
})
class Games extends React.PureComponent {
  static defaultProps = {};
  state = {};

  componentDidMount() {
    this.applySettings();
  }

  componentDidUpdate() {
    this.applySettings();
  }

  applySettings() {
    const {
      setOpenFilter,
      setOpenSort,
      setOpenType,
      setSearch,
      setOpenGame,
      setCloseSelect,
      setOpenTags,
      match,
    } = this.props;

    if (match?.params?.mode === GAMES_MODE_LIST) {
      setCloseSelect();

      if (match?.params?.props) {
        const allProps = JSON.parse(match.params.props)

        if (allProps.filter) {
          setOpenFilter(allProps.filter);
        }

        if (allProps.sort) {
          setOpenSort(allProps.sort);
        }

        if (allProps.type) {
          setOpenType(allProps.type);
        }

        if (allProps.tags) {
          setOpenTags(allProps.tags);
        }

        if (allProps.search) {
          setSearch(allProps.search);
        } else {
          setSearch('');
        }
      }

      setOpenGame(null);
    }

    if (match?.params?.mode === GAMES_MODE_VIEW) {
      setCloseSelect(null);
      setOpenGame(match?.params?.props);
    }
  }

  render = () => {
    const {
      openGame,
      games,
      openType,
      openSort,
      openFilter,
      openTags,
      search,
      select,
      selectId,
    } = this.props;

    setTitle('Games');

    let isOpenGame;

    if (!games.loading) {
      isOpenGame = games.data?.games?.find((game) => {
        return game.id === Number(openGame);
      });
    }

    const hideMenu = isOpenGame
      ? (
        isOpenGame.expansionCount > GAME_EXPANSIONS_MAX
          ? 'hideAll'
          : 'hideSmall'
      ) : false;

    return (<div className={style.body}>
      {!games.loading && <SubNavigation
        buttons={[
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_OWNED,
              sort: constants.GAME_SORT_ALPHABET,
              filter: constants.GAME_FILTER_NO,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.all')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_OWNED,
              sort: constants.GAME_SORT_PLAYS,
              filter: constants.GAME_FILTER_NO,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.playcount')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_OWNED,
              sort: constants.GAME_SORT_RATING,
              filter: constants.GAME_FILTER_NO,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.rating')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_OWNED,
              sort: constants.GAME_SORT_LAST_PLAY,
              filter: constants.GAME_FILTER_PLAYED,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.lastPlay')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_OWNED,
              sort: constants.GAME_SORT_ALPHABET,
              filter: constants.GAME_FILTER_UNPLAYED,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.unplayed')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_WISHLIST,
              sort: constants.GAME_SORT_ALPHABET,
              filter: constants.GAME_FILTER_NO,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.wishlist')
          },
          {
            to: createGamePath(GAMES_SHOW, {
              type: constants.GAME_TYPE_PLAYED,
              sort: constants.GAME_SORT_ALPHABET,
              filter: constants.GAME_FILTER_NO,
              tags: openTags,
              search
            }),
            text: message('components.gamesNavigation.texts.goTo.played')
          }
        ]}
        selectedPath={createGamePath(GAMES_SHOW, {
          type: openType,
          sort: openSort,
          filter: openFilter,
          tags: openTags,
          search
        })}
        search={search}
        searchPath={(searchProp) => createGamePath(GAMES_SHOW, {
          type: openType,
          sort: openSort,
          filter: openFilter,
          tags: openTags,
          search: searchProp
        })}
        messages={{search: message('common.search')}}
        hide={hideMenu}
      />}
      {(!select || selectId) && !games.loading && <GameArea isOpenGame={isOpenGame}/>}
      {openGame && <GameView/>}
    </div>);
  }
}

export default Games;
