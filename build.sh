#!/bin/bash

if [[ -z "${STAT_VERSION}" ]]; then
  VAR=0
else
  VAR="${STAT_VERSION}"
fi

export STAT_VERSION=$((VAR+1))

npm run build-prod